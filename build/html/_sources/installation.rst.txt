Installation and Setup for CryptoMove on Docker
=================================================

This section provides step-by-step instructions to install CryptoMove on Docker

Step 1: Install Docker
************************
 
#. ``sudo apt-get install linux-image-extra-$(name -r)``
#. ``sudo apt-get install linux-image-extra-virtual``
#. ``sudo apt-get update``
#. ``sudo apt-get install apt-transport-https ca-certificates curl software-properties-common``
#. ``curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -``
#. ``sudo apt-key fingerprint 0EBFCD88``
#. ``sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"``
#. ``sudo apt-get install docker-ce``
#. ``docker build -t cryptomove .``
#. ``docker run -ti --privileged cryptomove /bin/bash``


Step 2: Mount an S3 bucket to an EC2 instance
************************************************

Instructions for mounting a block device in AWS: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html

#. ``wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/s3fs/s3fs-1.74.tar.gz``
#. ``tar -xvzf s3fs-*.tar.gz``
#. ``sudo apt-get update``
#. ``sudo apt-get install build-essential gcc libfuse-dev libcurl4-openssl-dev libxml2-dev mime-support pkg-config libxml++2.6-dev libssl-dev``
#. ``cd s3fs-*``
#. ``./configure --prefix=/usr``
#. ``make``
#. ``make install``


Step 3: Check the directory for which the s3fs was mounted
*************************************************************
 
#. ``which s3fs``


Step 4: Configuring S3 bucket with access key and secret key
***************************************************************

Create a new file in /etc with the name passwd-s3fs and paste the access key & secret key in the below format.
 
#. ``touch /etc/passwd-s3fs``
#. ``vim /etc/passwd-s3fs``
#. ``Your_accesskey:Your_secretkey``


Step 5: Change the permissions of the file
********************************************
 
#. ``sudo chmod 640 /etc/passwd-s3fs``


Step 6: Create a directory and mount S3bucket in it.
*******************************************************
 
#. ``mkdir /mys3bucket``
#. ``s3fs <your_bucketname> -o use_cache=/tmp -o allow_other -o multireq_max=5 /mys3bucket``

.. note:: Replace <your_bucketname> with your S3 bucket name.


Step 7: Change CryptoMove datastore to AWS S3
************************************************
 
#. ``export HELLO_PACKSRC_PATH=/home/Ubuntu/cmove``
#. ``mv /home/Ubuntu/cmove/datstore /mnt/mys3bucket/datastore``
#. ``export CRYPTOMOVE_PATH=/mnt/mys3bucket/datastore``
