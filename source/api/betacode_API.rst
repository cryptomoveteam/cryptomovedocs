Beta Code
==========

Beta code to skip charge
-------------------------

**POST**: /v1/user/server/betacode

Required informations (To be finalized):
------------------------------------------


Beta code
------------


UI Request signature
---------------------

::

	{
		"beta_code": "Pl6cwCjJ"
	}

Successful Response Signature 200
-----------------------------------

::

	{
		"status": "success"
	}

Error Response return 400
--------------------------

::

	{
		"status": "wrong beta code"
	}