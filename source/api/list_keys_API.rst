List keys API
==============

**POST**: /v1/user/key/list [Integrated]

Required informations (To be finalized)
----------------------------------------

* The tokenId of auth0 user

UI Request signature
---------------------

::

	{
		“email” : “adsfasdf@gmail.com”,
	}

Successful Response Signature
-------------------------------

::

	{
		“status”: “success”,
			“keys”: [
			“key01”,
			“key02”,
			“key03”,
			“key04”
		]
	}

Error Response return
------------------------

::

	{
		“status”: “error”,
		“message”: “Failed to list keys.”
	}