Versioning Rest API List
=========================

List all the keys with total version:

**POST**: /v1/user/key/list_no_dup

Required informations (To be finalized)
---------------------------------------

* The tokenId of auth0 user

UI Request signature
----------------------

::

	{
		“email” : “adsfasdf@gmail.com”,
	}

Successful Response Signature
------------------------------

::

	{
		“status”: “success”,
			“keys”: {
			“kai_wang1”(key name): 2(total versions of this key),
			“kai_wang2”: 3
			}
	}

Error Response return
----------------------

::

	{
		“status”: “error”,
		“message”: “Failed to get the key list.”
	}