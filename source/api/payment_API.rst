Payment API
============

**POST**: /vi/stripe/pay (To be finalized) [Integrated]


Stripe structure
-----------------

Post request making to RestAPI with parameters listed below. RestAPI request stripe to charge money then get response with a charge Id.

RestAPI send a request to Auth0 to update the user info with charge Id and whether the user enrolled, get a response from Auth0 whether successfully update the info.(May need one more step to get the user info from Auth0 in order to update the user information)

RestAPI send a response with status code


Required informations
-----------------------

* The tokenId of auth0 user
* The token generated by stripe
* Amount charges
* Currency


UI Request signature
---------------------

::

	{
		“auth0_user_token_id” : “adsfasdf”,
		“charge_amount” : “500” (5 dollars),
		“currency”: “USD”,
		“stripe_token”: “asdfadsf”
	}

Successful Response Signature
------------------------------

::

	{
		“status”: “success”,
		“data”: {}
		“message”: “Congrats! Now you are a cryptomove gold member!”
	}

Error Response return
----------------------

::

	{
		“status”: “error”,
		“message”: “Error in stripe payment process, please contact our service at 222-222-2222 to resolve the issue.”
	}