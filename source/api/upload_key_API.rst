Upload key API
=================

**POST**: /v1/user/key/protect [Integrated]

Required informations (To be finalized)
----------------------------------------

* The tokenId of auth0 user
* Key name
* Key value

UI Request signature
---------------------

::

	{
		“email” : “adsfasdf@gmail.com”,
		“key_name” : “test name”, (spaces not allowed)
		“key_value”: “key value”
	}

Successful Response Signature
-------------------------------

::

	{
		“status”: “success”,
		“key-name”: “test name”
	}

Error Response return
------------------------

::

	{
		“status”: “error”,
		“message”: “Failed to saving the key.”
	}