List specific key’s all versions
==================================

**POST**: /v1/user/key/version_list

Required informations (To be finalized)
-----------------------------------------

* The tokenId of auth0 user
* Key name

UI Request signature
----------------------

::

	{
		“email” : “adsfasdf@gmail.com”,
		“Key_name”: “kai_wang1”
	}

Successful Response Signature
-------------------------------

::

	{
		“status”: “success”,
			“keys”: [
			“Version 1”,
			“Version 2”,
			“Version 4”
			]
	}

Error Response return
-----------------------

::

	{
		“status”: “error”,
		“message”: “Failed to get the key list.”
	}