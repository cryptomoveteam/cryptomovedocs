Preface
=======

CryptoMove distributed data store maintains data across a TCP/IP network
of LINUX computers and public data storage clouds [1]_. Via cloning,
partitioning -- *and a continuous process of encryption, renaming, and
movement around the network* -- it makes locating the data of interest
impossible absent knowledge of the data key. Without the key, brute
force attacks on CryptoMove store are doomed to failure because of the
astronomically high entropy of the CryptoMove data store. The non-stop
concealment process keeps moving and mutating the saved data, thus
precluding internal and external attackers from identifying the stored
data of interest. Therefore, CryptoMove renders futile any attack on the
data store even before the attack begins – the attacker just can never
find the data. In addition, CryptoMove actively watches for interference
with its operations and intrusion in the data store – it moves the data
away after detecting a malicious activity. At the same time, storing
encrypted data in files allows for data store owners to utilize any
file-based store management facilities assuring the availability of
ciphertext for subsequent legitimate retrieval and decryption. Part of
the CryptoMove strategy -- storing and moving around redundant data --
offers protection against hardware and software failures.

CryptoMove is a product by CryptoMove, Inc. written in the distributed
programming language `Hello <http://www.amsdec.com/about/>`__.
CryptoMove is available at
`www.cryptomove.com <http://www.cryptomove.com>`__.

About This Document
-------------------

This document has six parts:

+-----------------------------------+-----------------------------------+
| 6. `Introduction <#_Introduction> | Explains how CryptoMove solves    |
| `__                               | the inherent insecurity of the    |
|                                   | modern data stores by preventing  |
|                                   | attackers from identifying the    |
|                                   | target of the attack.             |
+===================================+===================================+
| 1. `User                          | Shows how to use CryptoMove to    |
|    Guide <#ccli-user-guide>`__    | save and restore data files       |
|                                   | securely. Read it to become       |
|                                   | quickly familiar with the main    |
|                                   | patterns of CryptoMove client     |
|                                   | commands.                         |
+-----------------------------------+-----------------------------------+
| 2. `Architecture <#architecture>` | Presents the inner workings of    |
| __                                | the CryptoMove distributed data   |
|                                   | store and CryptoMove daemons.     |
|                                   | Study it if you are curious about |
|                                   | the CryptoMove internals.         |
+-----------------------------------+-----------------------------------+
| 3. `CCLI                          | Lists all commands from the       |
|    Reference <#ccli-reference>`__ | CryptoMove Command Line Interface |
|                                   | (CCLI) to access and control      |
|                                   | CryptoMove distributed data       |
|                                   | store. Use its information to     |
|                                   | perform effective data store and  |
|                                   | retrieval operations.             |
+-----------------------------------+-----------------------------------+
| 4. `CCAPI                         | Explains CryptoMove C Language    |
|    Reference <#ccapi-reference>`_ | API (CCAPI) to access and control |
| _                                 | CryptoMove distributed data       |
|                                   | store. Use that information to    |
|                                   | develop effective programmatic    |
|                                   | data store and retrieval          |
|                                   | operations from custom written    |
|                                   | programs.                         |
+-----------------------------------+-----------------------------------+
| 5. `Appendix <#appendix-math>`__  | Explains mathematical             |
|                                   | underpinnings of the CryptoMove   |
|                                   | concealment algorithms. Read this |
|                                   | part if you enjoy formulae from   |
|                                   | Combinatorics and Probability     |
|                                   | Theory.                           |
+-----------------------------------+-----------------------------------+

Platforms
---------

The current CryptoMove release v1.0.\* runs on the 64-bit UBUNTU and
REDHAT [2]_ varieties of the LINUX OS [3]_ on Intel x86_64 [4]_
processors.

Typographical Conventions
-------------------------

Different typing and colored fonts throughout the text denote the
following: regular text, *important items,*\ **emphasized items**\ *,*
commands and output, syntax diagrams, comments, prompts.

CryptoMove v1.0.6 Release Notes
-------------------------------

-  A number of code bugs fixed.

-  Runtime performance improved.

-  Documentation corrected and enhanced.

-  New features added:

1.  Edge server mode

2.  Key distribution and duplication

3.  Intrusion detection

4.  Data redundancy groups

5.  Save and restore of the data logs

6.  Modification and commit of the already saved data

7.  Save and restore of the transient data

8.  Storing data on the dedicated host in the dedicated directory

9.  Saved data expiration

10. Cryptomove Data Store Directory Operations

11. Saving and restoring client directories

12. Restricted data keys

13. CryptoMove C language API (CCAPI)

14. Key revocation

15. Cryptomove MySQL and Vertica DB User Defined Functions

16. Cryptomove Client Directory Operations

17. Allowing access to AWS S3 and Box clouds

18. CryptoMove server auditing

19. Maintaining multiple system keys

20. Prohibiting ptrace() system calls and server dumps

21. Allowing for gpg-encrypted system keys

22. Introducing icrv client

23. Porting to REDHAT

24. Better installation with automatic install of dependent packages

Introduction
============

**Preventing Data Identification**

When users encrypt and save their secret information in the computerized
persistent data stores, the stored data inevitably becomes the target of
the cryptographic attacks. During the attack, obtaining an encrypted
secret is the first necessary step in the sequence of deciphering
attempts. The purpose of the attack may vary from decoding the
ciphertext to understanding the encoding method, discovering encryption
keys for use in subsequent attacks, etc. However, *in all cases,
identifying the encrypted text is a mandatory condition without which no
cryptographic attack can start*, much less advance in achieving its
goals.

Without data identification, any cryptographic attack becomes futile. At
the core of the CryptoMove operations is the continuous concealment
process that cryptographically mutates stored data, changes data names
and keeps moving the data around the distributed data store. In
addition, CryptoMove continuously watches for an interference with its
runtime operations and unauthorized access to the data store: when it
detects an intrusion, CryptoMove immediately moves away all data from
the data store and kills the intruding process.

Because of this dynamic approach, no attacker can identify the data of
interest by its name, size, content, access time or use patterns.
Therefore, the attacker cannot attack individual data simply because she
does not know which data to attack. Even if the attacker attempts to get
a hold of the entire data store, her chances are slim in case the store
is spread over the network.

At the same time, a brute force attack on CryptoMove store is infeasible
because of its high cost in both time and computing resources. Just to
locate a single file before mounting a cryptographic attack on that
file, one has to sift through about :math:`2^{476}` combinations of
parts in the CryptoMove store of 10000 files, where each file is split
into 12 parts with 4 copies [5]_.

   *By continuous transformation and movement of the stored data, and by
   watching for the intrusion, CryptoMove makes identification of a
   particular ciphertext practically impossible.*

**Why is Preventing Data Identification Important?**

Just encrypting the file content and its name, without preventing its
identification, still jeopardizes security. In case the attacker gets a
hold on the ciphertext, she might succeed in decrypting the ciphertext.
Alternatively, she might blackmail the data owner despite being unable
to decrypt the data. Also, encryption methods considered safe today
might become unsafe tomorrow because of the progress in the deciphering
techniques. In some cases, the attacker may apply unlimited computing
resources thus succeeding in a brute force attack. Ultimately, the
attacker may possess a secret algorithm that deciphers the seemingly
impregnable encryption scheme. *Therefore, having the attacker being
able to identify the encrypted data is potentially as dangerous as
having the attacker succeeding in stealing cleartext or deciphering
encrypted data*.

If the means of identification are unprotected, then it is easy for the
attacker to choose data just by the file name, size, content, access
time or use pattern. Note that sometimes the attacker is an *outsider*
who procures help from an *insider* or is an insider herself familiar
with the data store keys. The same insider often can identify the data
based on her insider’s knowledge. Also, in many cases it is enough to
identify just a portion of the needed data as it contains references to
the remaining parts.

   *Defeating attack by denying the ability to isolate the ciphertext
   serves as strong protection not only against*\ **external
   attackers**\ *who had violated access controls but also
   against*\ **internal**\ **attackers**\ *with the proper storage
   access, such as malevolent system administrators.*

CCLI User Guide
===============

*CCLI* stands for CryptoMove Command Line Interface. This section
briefly describes how to use CryptoMove to save and restore data in the
CryptoMove distributed data store using CCLI. While providing initial
guidance and basic examples, it concentrates on the simplest use
patterns, leaving details outside of its scope. For a complete reference
of all CryptoMove commands, options, and modes of operations, consult
the Reference in section 4. Section 5 describes *CCAPI* -- CryptoMove C
language API, which you can use to access CryptoMove data store from
programs written in C programming language.

CryptoMove Crash Course – Secret Confession
-------------------------------------------

   The following example is a crash course in basic CryptoMove
   operations. It shows how to:

1. Prepare CryptoMove package for execution

2. Generate system and data keys

3. Start up CryptoMove server

4. Start up CryptoMove client

5. Save a file in CryptoMove

6. Observe local data movement as it is performed by a single server

7. Restore data from the store

8. Quit the client and shut down the server

**Note that the actual keys generated on your computer must differ from
the ones in this document – use the keys created on your computer if you
plan to play this example online. Similarly, the names of the Linux
host, Linux user and file directories on your system may differ from the
ones in the screenshots below.**

1. **Prepare CryptoMove package for execution**

..

   Set up the path to CryptoMove package after its proper install as
   explained in section `CryptoMove
   Installation <#typographical-conventions>`__:

   think@centos-middle:~$ export
   HELLO_PACKSRC_PATH=/home/think/cryptomove.prepare.1.0.6

   think@centos-middle:~$

   Set up the path to the CryptoMove data store, which will be created
   in the next step in the current directory:

   think@centos-middle:~$ export CRYPTOMOVE_PATH=`pwd\`

   think@centos-middle:~$

2. **Generate System Key**

Generate a 12 bytes long system key – CryptoMove manager will use it to
start up CryptoMove server and to encrypt data parts in its store; this
command also initializes CryptoMove data store in the current directory:

   think@centos-middle:~$ crv gs 12

   system key generated -- <322d98a8f4c3777ea653b1d5>

   think@centos-middle:~$

3. **Start up CryptoMove server**

Make sure to start up the standalone server – the one that does not
connect to other servers and does not accept connections from other
servers:

   think@centos-middle:~/cryptomove.prepare.1.0.6$ cat /dev/null >
   .hello_hosts

   think@centos-middle:~/cryptomove.prepare.1.0.6$ cat /dev/null >
   .hello_in

   think@centos-middle:~/cryptomove.prepare.1.0.6$

   Start up CryptoMove server – enter the just generated system key on
   the prompt:

   think@centos-middle:~$ crv ss

   hee: HELLO ENGINE V1.0.6 (alpha) COPYRIGHT (C) CRYPTOMOVE 2011-2016.

   (22058) Enter secret key #1 out of 1: ## enter just generated system
   key here

   think@centos-middle:~$

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 1(min=1/max=1) hosts

   1.
   [ONLINE]<CENTOS-MIDDLE>[monitored]<CENTOS-MIDDLE><12357:192.168.1.20><1B201A633CE149F1B9D1AC2286C0F876>

   #### no parts to restart

   cryptomove server pid=22062 started...

4. **Generate Data Key**

Generate a single 8 bytes long data key – CryptoMove client will use it
for encrypting cleartext before saving it in CryptoMove data store, and
for deciphering the saved data after its restoration from the store.
Note that CryptoMove does not echo the keys upon their entry – you will
not see them as you type:

   think@centos-middle:~$ crv gd 8 1

   (21574) Enter secret key #1 out of 1: ## enter just generated system
   key here

   Begin generating 1 data key...

   Generating data key #1 out of 1 -- <b8b8be8d6efabaaf>

   End generating data keys

   think@centos-middle:~$

5. **Start up CryptoMove client**

Before starting up CryptoMove client, prepare the file with secret
information – you will be saving this file in CryptoMove store:

   think@centos-middle:~$ echo I love you! > secret_confession

   think@centos-middle:~$ cat secret_confession

   I love you!

   think@centos-middle:~$

Now start up CryptoMove client:

   think@centos-middle:~$ export
   HELLO_PACKSRC_PATH=/home/think/cryptomove.prepare.1.0.6

   think@centos-middle:~$ crv cs

   starting cryptomove data user client

   (23064)crv>

You can work interactively with the cryptomove client by typing commands
onto its prompt “crv>”. To avoid entering data key multiple times,
switch to the *trust mode* – the client remembers the entered key in its
un-swappable memory and keeps using it for all subsequent commands
requiring a key:

   (23064)crv>trust on

   enter data key: ## enter just generated data key here

   (23064)crv>

6. **Save a file in CryptoMove**

..

   Save file secret_confession in CryptoMove:

   (23064)crv>put -d 4 -m 28800 -p 28800 secret_confession

   \***\* saving commences (entropy 153)

   1. compressing...

   2. cooking 96 keys.....

   3. making 96 part names....

   4. making 96 hash names....

   5. making 96 trace names.....

   7. permuting....

   8. splitting...

   9. encrypting....

   10. padding & signing...

   11. confusing...

   12. scattering...

   13. flushing...

   14. encrypting traces...

   15. dumping traces...

   16. logging record <#1>...

   17. start moving 96 out of 96...

   \***\* saved file <secret_confession>

   (23064)crv>

CryptoMove client had split the original file into default 12 parts,
created default 4 copies for each part, performed all actions as listed
in the screenshot above, and commanded CryptoMove server to start
concealing all parts. The number 96 of listed parts comes out because
there is a permutation part for every created data part. Thus, the total
part count is 4*12*2=96. Note the non-default parameters used when
saving the file:

+-----------------------------------+-----------------------------------+
| -d 4                              | Move parts no further than 4      |
|                                   | steps away from the base server.  |
+===================================+===================================+
| -m 28800                          | Move each part independently      |
|                                   | 28800 times a day (i.e. every     |
|                                   | three seconds).                   |
+-----------------------------------+-----------------------------------+
| -p 28800                          | Generate pulse message for each   |
|                                   | part with the interval 3 seconds  |
|                                   | apart.                            |
+-----------------------------------+-----------------------------------+

7. **Observe local data movement as it is performed by a single server**

From a different window, set CRYPTOMOVE_PATH and launch another
CryptoMove client as *system owner*. Upon key prompts, enter the system
key. After that, perform the following commands:

+-----------------------------------+-----------------------------------+
| power 100                         | By default, CryptoMove server     |
|                                   | works with power value equal 20.  |
|                                   | With such power, it moves just a  |
|                                   | few parts of any given file, and  |
|                                   | not very frequently. Switching it |
|                                   | to 100 – the maximal power        |
|                                   | possible – forces the server to   |
|                                   | move around every part with the   |
|                                   | frequency specified at the time   |
|                                   | of saving the file (every 3       |
|                                   | seconds in our case).             |
|                                   |                                   |
|                                   | After that, from yet another      |
|                                   | window, run Linux utility         |
|                                   | iotop [7]_ to observe the disk    |
|                                   | I/O statistics:                   |
|                                   |                                   |
|                                   |    think@think:~$ sudo iotop -o   |
|                                   |    -P -b \| grep hee              |
|                                   |                                   |
|                                   | You should be able to see some    |
|                                   | read and write activity caused by |
|                                   | Hello runtime engine which        |
|                                   | executes CryptoMove package.      |
+===================================+===================================+
| power 2                           | This command shall reset          |
|                                   | CryptoMove server to its minimal  |
|                                   | power mode that still allows the  |
|                                   | movement of data parts.           |
|                                   |                                   |
|                                   | After that, observe the           |
|                                   | diminished disk I/O activity in   |
|                                   | the window that runs iotop.       |
+-----------------------------------+-----------------------------------+
| quit                              | Quits CryptoMove client.          |
+-----------------------------------+-----------------------------------+

..

   think@centos-middle:~$ export CRYPTOMOVE_PATH=`cat pwd\`

   think@centos-middle:~$ ./crv os

   (25077) Enter secret key #1 out of 1: ## enter just generated system
   key here

   starting cryptomove system owner client

   (25077)crv>power 100

   (25077)crv>power 2

   (25077)crv>quit

   cryptomove client finished

   think@centos-middle:~$

8. **Restore data from the store**

In this series of commands, you restore the previously saved file from
the store. First, delete it from the current user directory, so that you
know that restoration truly works. You can delete the file using plain
shell command prepended with the exclamation sign, as follows:

   (23064)crv>!rm -f secret_confession

   (23064)crv>!ls -lt secret_confession

   ls: cannot access secret_confession: No such file or directory

   (23064)crv>

Now restore the file from CryptoMove store using get command. Note that
while the client saved 96 parts, it restores only 24 parts because there
are 4 copies of 24 unique parts, of which there are 12 data parts and 12
permutation parts:

   (23064)crv>get secret_confession

   \***\* restoring commences

   1. using log record <#1>...

   2. cooking 96 keys.....

   3. making 96 part names....

   4. making 96 hash names....

   5. fetching 24 parts (in <=20 seconds)....

   completed 24 parts of <secret_confession>

   elapsed 1.005 seconds

   6. assembling cleartext....

   7. flushing result...

   \***\* restored file <secret_confession>

   (23064)crv>

   Finally, check that the restored file has the original content:

   (23064)crv>!ls -lt secret_confession

   -rw-rw-r--. 1 root think 20 Aug 27 20:44 secret_confession

   (23064)crv>!cat secret_confession

   I love you!

   (23064)crv>

9. **Quit the client and shut down the server**

Finally, quit the client and shut down the server. when asked for a key
enter the previously generated system key:

   (23064)crv>quit

   cryptomove client finished

   think@centos-middle:~$ crv st

   (25432) Enter secret key #1 out of 1: ## enter just generated system
   key here

   cryptomove server stopping...

   think@centos-middle:~$

While the server is stopping, it indicates in its window the shutdown
progress as the total number of operations performed so far. It finally
stops after that number (1848824) remains stable for some time:

   cryptomove server is about to stop
   .1848337.1848426.1848623.1848671.1848716.1848764.1848803.1848824.1848824.

   cryptomove server stopped (code=0)

   1:22062:2:NOTICE:G5:5::HELLO ENGINE pid=22062 finished.

   think@centos-middle:~$

You may check the contents of the CryptoMove store to see the file parts
– their names, access time and size -- all indistinguishable from each
other, located in random directories, and uncorrelated to the original
file name secret_confession.

think@centos-middle:~$ sudo su

[root@centos-middle think]# cd .crv_top_dir/

[root@centos-middle .crv_top_dir]# ls -lt \`find . -name "*.crv"\` \|
more

-rw-------. 1 root think 16465 Jan 21 2015
./.crptmv_top_dir/0/0/1/3/3/AwSuO8dxOII0kUH5jVXaUBuSZdZhNTnl72aCOwCm3Og1

uwHq8btCzhfrPxc7nmyuSM2CRpRxCDR1f1pk7uhcjnYFkfXYbrluFM4tJ6OGVZJuOTjfgLJe7DFr3mWyQigSMHJbYsI3WmQYD632UGXP+ALNAlChIoya

xcgT7gj+xkug==.crv

-rw-------. 1 root think 12369 Jan 21 2015
./.crptmv_top_dir/0/0/2/0/C9aRkRvyFshUw8UEx8bT6_rpq6X6T8+K3mbxwkyk53bzAv

OAYmG8OPxehYJloP1u0LHoStq2xDS06nJQbL_39m4005s8eLItj08LEbFrF4ufmg7_mNG4lHgpqCV1SpWjiJVBLjZkNWR1GM9wAuExksRx7bfbeK4t8S

OXG7v+F2oQ==.crv

-rw-------. 1 root think 8273 Jan 21 2015
./.crptmv_top_dir/0/0/2/APabAvjKOBEQ7c+4eIFWP0SZHXlQeRbfWEiqXnoHk+Xz7Eby

RJe6oPgRmNangM2RXiji06XgH2f_ybGE_547IdSacKgJVvmvP4QJ4hku7m4veSTLA0BaWRJ+mkSOFvEeU+ht4CmZB53yWyZvAOZZa8FKFVhrqikAE7Az

aOjCMpJQ==.crv

-rw-------. 1 root think 16465 Jan 21 2015
./.crptmv_top_dir/0/0/DQZQs0M5G+x3QoMj2Yrk5ChCo1ojSx2rumgGsJyupwnl5SDbvW

Ih5MHmFD_5kBghPrLszCEWUQ7I70OMeHMxRS1MwqmjQkyN+gqUkpNC6UQvT+7qJOtcTNa+WaoYZo9pzuziS9PCFf43h7KWwxpDwi+SXiorHNiaIegBIM

KMp5taCstfjho82aLCnmOhOYNHk=.crv

-rw-------. 1 root think 16465 Jan 21 2015
./.crptmv_top_dir/0/1/3/4/5/4/DtQc1dRDZaXIt_CcEjUI6jvQdB1iw2O7oxOp_CCv46

gsHo2cKNlJFLuIhLCC3Yrichtoefszisya0+46Rax52nKLGoDy7bh3QRAqbYOdnZWQGOdI_IIymQMc9e8vwzPgrN+4zVMgX0HrQqBz_UQz1_qDbTwbBG

+kH_mVRpBxeXdvY_LemUUwcB6ZdEMTGtPek=.crv

-rw-------. 1 root think 16465 Jan 21 2015
./.crptmv_top_dir/0/2/1/1/1/2/A_5aynTNTLnkHOgsy9f4ZM1UcKMmIM+VbV30gYzD+t

zRmlLfZNU5Mn2mPFwXIFPfEZDUlhXk79PhEL_h4eRu1pbYodDZQuaEe5QE8NDCU4ERCRTmEWpcacbWebFnPNVcxOTO8+hJ4D7Lzkffaa8GDpndbeCuQa

Rha9eOKkINZCGw==.crv

………………………………………………………………………………………………………………………………………………………………

[root@centos-middle .crptmv_top_dir]# exit

think@centos-middle:~$

CryptoMove Users
----------------

There are two categories of CryptoMove users: *data users* and *system
owners*. As a data user, you run CCLI commands from the CryptoMove
interactive client program that moves files between your file system and
CryptoMove distributed data store. You can also write and execute
programs that access CryptoMove data store via CCAPI – CryptoMove C
language API.

When restoring data from CryptoMove data store into the client data
store, CryptoMove client makes the system root user the owner of the
restored data unless environmental variable CRYPTOMOVE_USER is set in
the client’s parent shell: in this case the user $CRYPTOMOVE_USER
becomes owner of the restored data.

As a system owner, you own a part or the whole of the CryptoMove data
store; you also operate one or more CryptoMove servers that maintain
CryptoMove store. You can also perform either role at different times,
in case you want to exchange your personal data with your own CryptoMove
store.

CryptoMove Directories & Shell Variables
----------------------------------------

   There are three directories to think about when working with
   CryptoMove:

1. **The directory where CryptoMove software is installed.** Its path
   must be assigned to HELLO_PACKSRC_PATH before running CryptoMove
   server or client. As such, it is best to set HELLO_PACKSRC_PATH in
   your shell startup file (e.g. .bashrc).

2. **The directory where CryptoMove system key is generated.** Its path
   must be assigned to CRYPTOMOVE_PATH before running CryptoMove server
   or any CryptoMove command that requires the system key. That same
   directory contains a subdirectory .crptmv_top_dir, which is the top
   directory of the whole CryptoMove data store. Note that directory
   $CRYPTOMOVE_PATH is unrelated to the installation directory
   $HELLO_PAPCKSRC_PATH.

..

   The system owner can create more than one CryptoMove system keys in
   different directories. This way, each directory contains its system
   key and CryptoMove data store. By setting CRYPTOMOVE_PATH to
   different directories, system owner can start CryptoMove server to
   work with any of its CryptoMove data stores: each time CryptoMove
   will ask for the system key related to the target data.

   Currently, only one CryptoMove server can run at any given time.
   Therefore, although the disk may contain several CryptoMove data
   stores, CryptoMove server will be using only one of them from the
   moment of its startup until shutdown.

3. **The directory from where CryptoMove data user starts up CryptoMove
   client.** That can be any directory on disk, in general, unrelated to
   either $HELLO_PACKSRC_PATH or $CRYPTOMOVE_PATH directories.

..

   When a client starts up, it connects to the currently running
   CryptoMove server on the same host [8]_. This way, the client does
   not need to know which cryptomove store is currently in use. It is up
   to the system owner to manage CryptoMove server start up and shutdown
   events, and to choose which CryptoMove data store to use at start up
   time.

4. **The directory from where CryptoMove server temporarily stores some
   of the user data before committing it to the data store**. That
   directory shall be designated by the variable $CRYPTOMOVE_API_PATH.

5. **The directory where CryptoMove server stores log files that record
   client operations**. That directory shall be indicated by the
   variable $CRYPTOMOVE_LOG_PATH.

Sample Cryptomove Environment Preparation
-----------------------------------------

The following example shows how to prepare the required Cryptomove shell
variables:

#

# setup cryptomove datastore paths

#

# directory which holds all things cryptomove

export CRYPTOMOVE_PATH=`pwd`/datastore

# cryptomove user -- owns all restored files

export CRYPTOMOVE_USER=`whoami\`

# directory to hold logs of saved files

export CRYPTOMOVE_LOG_PATH=$CRYPTOMOVE_PATH/.log

# directory to hold pipes for clients to communicate with the server via
cryptomove API

export CRYPTOMOVE_API_PATH=$CRYPTOMOVE_PATH/.api

#

# settings for server startup behavior

#

#this setting keeps server stdout and stderr on the screen that invoked
the server

export CRYPTOMOVE_NOALTER="yes"

#with this setting a true Hello command line is printed upon Hello
engine startup

export CRYPTOMOVE_HELLO_ENGINE_STARTUP_LINE_DUMP=1

Preparing CryptoMove Environment
--------------------------------

As a system owner, you should know that CryptoMove software must be
installed on each computer where either CryptoMove client or CryptoMove
server will be running. You shall have the rights to execute CryptoMove
client and server programs /usr/bin/crv and /usr/bin/hee.

Because CryptoMove software is written in Hello language, its code is
located in shared libraries, which are the translated images of Hello
packages CryptoMove_World and Wide_World. After CryptoMove installation,
you can find these libraries in the directory where CryptoMove software
has been installed. Point your shell environment to the full path of
that directory as follows (or else CryptoMove will not start up):

export HELLO_PACKSRC_PATH=/full_path_to_installation_directory

Initializing CryptoMove Data Store
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The command crv gs generates the system key and also creates the
CryptoMove native data store. Therefore, you can generate a system key
only once in given a directory. However, you can generate a system key
(and corresponding CryptoMove native store) in any UNIX directory, by
resetting CRYPTOMOVE_PATH, thus ending up with different CryptoMove data
stores on the same computer.

You can start up CryptoMove server to use any of those directories as
long as there is only one CryptoMove server running. The current
CryptoMove version does not allow for multiple CryptoMove servers to
operate in parallel on a single LINUX host [9]_.

You must set shell variable CRYPTOMOVE_PATH to the parent directory of
the CryptoMove data store before starting CryptoMove server or executing
any of theCryptoMove manager commands that require the system key,
including crv gs.

Preparing Keys
--------------

CryptoMove generates, stores their hashes, and uses two categories of
symmetric keys for its operations – *data* and *system* *keys*. You
should keep them secret from unauthorized users.

Data Keys
~~~~~~~~~

If you are a data user, you shall ask system owner to allocate for you
one or more *data keys* because the client program will ask you to
provide a data key to authorize save and restore operations. If you own
the data store and, at the same time, want to save your data in that
store, then, as a system owner, you should allocate the data keys to
yourself.

If you are a system owner, then it is your responsibility to handle
creation and assignment of data keys to all users of your CryptoMove
store. You shall keep data keys secret from unauthorized users.

As a data user, you can have any number of data keys. Once you save a
file using a key, that file must be restored with the same key. Also,
data keys are *sticky* – they are valid only on the computer host where
they had been generated. Therefore, you cannot save a file on computer A
and restore it on B – you can restore it only on A. However, you may
distribute a key from one client computer to another, and also save data
with the remote name using flag –r remotename. After that, you may
restore the saved data on another host to which you have already
distributed the key which has been used to save the data.

User Keys
~~~~~~~~~

As a data user, in addition to data keys you can use *user keys*, which
are secret strings of characters. If you choose to use a user key while
saving a file, you must then use the same user key in the restore
operations. You can mix and match any user key with any data key. If you
keep the user key secret, then nobody, even the system owner, can get a
hold on the saved data because CryptoMove does not store user keys or
their hashes persistently.

If you save data using only data key but without the key, then the
system owner may still have a chance to recover your data. If you are a
system owner who saves and restores your own data in the CryptoMove,
then you may choose not to use the keys as long as you keep your data
keys secret.

System Key
~~~~~~~~~~

As a system owner, you shall generate a single *system key* after
installing CryptoMove. That key will allow you to start up and shut down
CryptoMove server, and to perform certain management operations.
CryptoMove also uses the system key to encrypt and decrypt data parts in
its persistent store.

Generating CryptoMove System Key
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To generate the system key run

   crv gs {LENGTH \| KEY}

When LENGTH is specified, this command generates a system key of LENGTH
bytes long, stores its hash in the data store. When KEY is specified, it
must be provided in hexadecimal format -- CryptoMove stores the hash of
that key in the data store. In either case, CryptoMove prints the
generated or supplied key (in hex format) on the screen. You shall keep
system key secret and use it for all CryptoMove operations that require
a system key. In order to generate any key, you must have shell variable
CRYPTOMOVE_PATH set to the directory containing CryptoMove data store.

Generating CryptoMove Data Keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To generate COUNT data keys LENGTH bytes long issue command:

   crv gd LENGTH COUNT

This command generates data keys, stores their hashes in the data store,
and prints them on the screen in hex format. This command requires you
to provide a system key.

You can also supply to CryptoMove a data key in hexadecimal format as
follows:

   crv gd KEY

In this case, CryptoMove stores the key hash in the data store and
prints this key on the screen. This command requires you to provide a
system key. You shall keep each data key secret and use it for all
CryptoMove operations that require a data key.

Generating and Using User Keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can always ask CryptoMove to generate a user key of arbitrary length
using command

   crv gk LENGTH

Either system owner or data user can run this command. CryptoMove
generates a user key of LENGTH bytes long and prints it (in hex format)
on the screen. CryptoMove does not save any hash of the generated user
key persistently. The generated user key is very strong for
cryptographic purposes. Also, the longer it is, the higher its strength.

As a data user, you may choose to use the generated user key, in
conjunction with the data key, in subsequent save/restore operations.
When you save the data with a user key, then even the system owner who
knows your data key(s) cannot recover the saved data. You can use any
user key, not necessarily generated by CryptoMove. However, in this
case, it is up to you to assure the strength of the used user key.

Examples of Server and Data Keys Generation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generate 16-bytes long system key and initialize CryptoMove native
store:

   think@think:~/cryptomove.prepare.1.0.6/bla$ crv gs 16

   system key generated -- <49e32e5f631a294e96cc3cc506c693a1>

   think@think:~/cryptomove.prepare.1.0.6$

Generate three 8-bytes data keys – note that CryptoMove does not echo
the entered system key; then generate a 16-bytes long key:

   think@think:~/cryptomove.prepare.1.0.6/bla$ crv gd 8 3

   (9730) Enter secret key #1 out of 1:

   Begin generating 3 data keys...

   Generating data key #1 out of 3 -- <4566b12a22d5e5b5>

   Generating data key #2 out of 3 -- <6a839117531fa519>

   Generating data key #3 out of 3 -- <37a6ca23a4da4659>

   End generating data keys

   think@think:~/cryptomove.prepare.1.0.6/bla$ crv gk 16

   key generated -- <29ed324c392cf841894a0047d97233a7>

   think@think:~/cryptomove.prepare.1.0.6/bla$

Server Startup and Shutdown
---------------------------

As a system owner, you can start up CryptoMove server with command

   crv ss

This command asks you for a system key.

In this version of CryptoMove software, every client connects to a
server running on the local host. Starting CryptoMove server is
necessary for CryptoMove clients to execute user requests. In addition
to serving requests from local clients, CryptoMove server communicates
and exchanges parts of saved data with the servers running on other
computer hosts. You must have shell variable CRYPTOMOVE_PATH set to the
directory containing CryptoMove data store.

Upon startup, the server detaches from its controlling terminal and
proceeds running as a daemon. Therefore, you cannot stop it as a regular
program with CTRL/C. Instead, you should shut it down with the special
cryptomove command:

   crv st

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store. This command also asks you for the
system key. After the server shuts down, no CryptoMove client on the
local host will be able to perform user requests. Also, the saved data
parts residing on this server cannot be accessed by other servers when
the local server is down.

CryptoMove High Availability Clusters
-------------------------------------

At startup, the server attempts to connect to servers running on other
hosts. After connections are established, the server can exchange the
data parts. The hosts to which the server connects constitute this
host’s *cluster*. Because every CryptoMove server has its cluster of
hosts, the set of all clusters becomes a web of interconnected clusters.
Together, they can exchange data parts saved by a client from any host
of any cluster.

You can control cluster membership, i.e. the servers to which the
starting server connects, by listing the host names in file
$HELLO_PACKSRC_PATH/.hello_hosts, in the format similar to the system
file /etc/hosts. Alternatively, you can have empty
$HELLO_PACKSRC_PATH/.hello_hosts. This setting effectively prohibits the
server from connecting to any other host at startup. Finally, if
$HELLO_PACKSRC_PATH/.hello_hosts is absent, the server attempts
connecting to hosts listed in the system file /etc/hosts. You can modify
cluster membership later, while the server is running, from the client
using *cluster commands*. Note that after a server starts up, all
clusters of servers to which it connects automatically include it in
their membership.

Cluster membership control is important because it determines where the
saved data travels after it has been saved in CryptoMove. For example,
if the server has no hosts in its cluster but itself, the saved data
randomly moves only within the data store of the local server.
Alternatively, if other hosts belong to the cluster, the data randomly
moves onto those hosts, and then onto the cluster hosts of those hosts,
etc. This way, the data can proliferate deep into the network.

In the following example, two CryptoMove servers start up one after
another on two computer hosts [10]_ -- think and RoyalPenguin1. Note
that the first server think lists only itself in its default cluster
named DIRECT_CRYPTOMOVE_CLUSTER because its file
$HELLO_PACKSRC_PATH/.hello_hosts is empty. The second server
RoyalPenguin1 lists both itself and think because its file
$HELLO_PACKSRC_PATH/.hello_hosts contains the name of the first host
think:

*Starting CryptoMove server on host think:*

   think@think:~/cryptomove.prepare.1.0.6$ cat /dev/null >
   ./.hello_hosts

   think@think:~/cryptomove.prepare.1.0.6/bla$ crv ss

   hee: HELLO ENGINE V1.0.6 (alpha) COPYRIGHT (C) CRYPTOMOVE 2011-2016.

   (9950) Enter secret key #1 out of 1:

   think@think:~/cryptomove.prepare.1.0.6/bla$

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 1(min=1/max=1) hosts

   1.
   [ONLINE]<THINK>[monitored]<THINK><12357:192.168.1.4><DBE610BB996B425ABE3ECE518C6AEB7C>

   #### no parts to restart

   cryptomove server pid=9953 started...

   think@think:~/cryptomove.prepare.1.0.6$

*Starting CryptoMove server on host RoyalPenguin1:*

   think@RoyalPenguin1:~/cryptomove.prepare.1.0.6$ echo think >
   ./.hello_hosts

   think@RoyalPenguin1:~/cryptomove.prepare.1.0.6$ crv ss

   CryptoMove_World/ ss>

   hee: HELLO ENGINE V1.0.6 (alpha) COPYRIGHT (C) CRYPTOMOVE 2011-2016.

   (23276) Enter secret key #1 out of 1:

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 2(min=1/max=2) hosts

   1.
   [ONLINE]<THINK>[monitored]<THINK><12357:192.168.1.4><DBE610BB996B425ABE3ECE518C6AEB7C>

   2.
   [ONLINE]<ROYALPENGUIN1>[monitored]<ROYALPENGUIN1><12357:192.168.1.10><45A4CC9FFF4347168DD8762673E830BC>

   #### no parts to restart

   cryptomove server pid=23279 started...

   think@RoyalPenguin1:~/cryptomove.prepare.1.0.6$

To shut down a running server issue the following command – enter the
system key upon its request:

   think@think:~/cryptomove.prepare.1.0.6/bla$ crv st

   (4552) Enter secret key #1 out of 1:

   cryptomove server stopping...

   `think@think:~/cryptomove.prepare.1.0.6/bla$ <mailto:think@think:~/cryptomove.prepare.1.0.4/bla$>`__

During shutdown, the server indicates its progress by printing the
ongoing count of completed requests:

   think@think:~/cryptomove.prepare.1.0.6/bla$

   cryptomove server is about to stop .734368.734742.734979.734979.

   cryptomove server stopped (code=0)

   1:21888:2:NOTICE:G5:5::HELLO ENGINE pid=21888 finished.

   think@think:~/cryptomove.prepare.1.0.6/bla$

Cluster Operations
~~~~~~~~~~~~~~~~~~

CryptoMove Clusters are *Highly Available* in the sense that the host
continuously monitors the states of hosts in its cluster. The server
maintains a constant heartbeat with other servers, which are members of
its cluster. When cluster members respond to a heartbeat message, the
server marks their state *online*. The server moves data only to online
servers.

When a cluster member stops responding to heartbeats for any reason, the
server marks that member with a different state – usually *faulted* or
*offline*. The system owner may also exclude a cluster member from the
cluster, or direct the server to stop monitoring a cluster member. In
cases like these, the server stops sending or receiving data to or from
the cluster member.

The system owner can use a variety of client commands to control its
clusters; Section 4.3.11 explains CryptoMove cluster operations in
detail. The following steps show examples of some basic commands –
starting three servers, examining their clusters, and modifying cluster
membership. If you are not interested in arranging CryptoMove clusters
at this time, proceed to the next section 2.10, which explains basic
CryptoMove data operations; it is OK to review the cluster operations
any time later.

A. Following the `installation
   instructions <#typographical-conventions>`__ make sure that
   CryptoMove software has been successfully installed on three hosts.
   This example uses host names think, RoyalPenguin1, and RoyalPenguin2.
   In your case, you can use hosts with any names as long as you apply
   actions from the example to your respective hosts.

B. On each host, set up the path to the installed CryptoMove package:

..

   think@think:~$ export
   HELLO_PACKSRC_PATH=/home/think/cryptomove.prepare.1.0.6

   think@think:~$

C. On each host, create a directory, which you want to contain
   CryptoMove data store; then set CRYPTOMOVE_PATH to that directory,
   and make that directory current [11]_:

..

   think@think:~$ mkdir -p /home/think/cryptomove.prepare.1.0.6/cdds

   think@think:~$ export
   CRYPTOMOVE_PATH=/home/think/cryptomove.prepare.1.0.6/cdds

   think@think:~$ cd /home/think/cryptomove.prepare.1.0.6/cdds

   think@think:~/cryptomove.prepare.1.0.6/cdds$

D. On each host, in that directory, create a file .hello_hosts that
   contains names of two other hosts. These files define the hosts each
   host can connect. For example, on host think .hello_hosts can be
   created like this:

..

   think@think:~/cryptomove.prepare.1.0.6/cdds$ echo RoyalPenguin1 >
   .hello_hosts

   think@think:~/cryptomove.prepare.1.0.6/cdds$ echo RoyalPenguin2 >>
   .hello_hosts

   think@think:~/cryptomove.prepare.1.0.6/cdds$ cat .hello_hosts

   RoyalPenguin1

   RoyalPenguin2

   think@think:~/cryptomove.prepare.1.0.6/cdds$

E. On each host, create file .hello_in with the exact same contents as
   their respective files .hello_hosts. These files list the only hosts
   that allow to connect to a local host:

..

   think@think:~/cryptomove.prepare.1.0.6/cdds$ cp .hello_hosts
   .hello_in

   think@think:~/cryptomove.prepare.1.0.6/cdds$

F. On each host generate its CryptoMove system key:

..

   think@think:~/cryptomove.prepare.1.0.6/cdds$ crv gs 12

   system key generated -- <66f71444c4307be1f0384f2f>

   think@think:~/cryptomove.prepare.1.0.6/cdds$

G. On each host generate one CryptoMove data key (enter the just
   generated system key upon the prompt):

..

   think@think:~/cryptomove.prepare.1.0.6/cdds$ crv gd 8 1

   (22949) Enter secret key #1 out of 1:

   Begin generating 1 data key...

   Generating data key #1 out of 1 -- <3ca1f0b72bb1a783>

   End generating data keys

   think@think:~/cryptomove.prepare.1.0.6/cdds$

H. Make sure no CryptoMove server is running on any server – the
   following command kills all Hello engines, and with them any
   CryptoMove servers, if they happen to hang around from the previous
   runs:

..

   think@think:~/cryptomove.prepare.1.0.6/cdds$ sudo hee -Q

   0:23231:1:NOTICE:S61:80::Cleaning up from previous Hello runtime by
   running /bin/sleep 1;sudo /usr/bin/pkill -9 hee;sudo /usr/bin/ipcs -a
   \| /bin/egrep "`whoami`|root" \| /usr/bin/awk '{ print $2 }' >
   .hello_ipc;sudo /bin/sed -i 's/^/-m /' .hello_ipc; sudo
   /usr/bin/ipcrm \`cat .hello_ipc`;sudo /bin/rm -rf .hello_ipc
   /opt/hello/mapped/\* &

   think@think:~/cryptomove.prepare.1.0.6/cdds$

I. Startup CryptoMove server on each host – enter just generated system
   key on each host when prompted. You should see different cluster
   compositions on each host: the first host lists only itself in its
   cluster because no other host is running CryptoMove server yet. The
   second host shows itself and the first host because the third host is
   not up yet. The third host shows all hosts. All hosts shown on all
   hosts shall be in online state:

..

   *First host CryptoMove startup:*

   think@think:~/cryptomove.prepare.1.0.6/cdds$ crv ss

   hee: HELLO ENGINE V1.0.6 (alpha) COPYRIGHT (C) AMSDEC 2011-2016.

   (30062) Enter secret key #1 out of 1: # enter 1st system key here

   think@think:~/cryptomove.prepare.1.0.6/cdds$

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 1(min=1/max=1) hosts

   1.
   [ONLINE]<THINK>[monitored]<THINK><12357:192.168.1.4><DBE610BB996B425ABE3ECE518C6AEB7C>

   #### no parts to restart

   cryptomove server pid=30065 started...

   think@think:~/cryptomove.prepare.1.0.6/cdds$

   *Second host CryptoMove startup:*

   think@RoyalPenguin1:~/cryptomove.prepare.1.0.6/cdds$ crv ss

   hee: HELLO ENGINE V1.0.6 (alpha) COPYRIGHT (C) CRYPTOMOVE 2011-2016.

   (7633) Enter secret key #1 out of 1: # enter 2nd system key here

   think@RoyalPenguin1:~/cryptomove.prepare.1.0.6/cdds$

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 2(min=1/max=2) hosts

   1.
   [ONLINE]<THINK>[monitored]<THINK><12357:192.168.1.4><DBE610BB996B425ABE3ECE518C6AEB7C>

   2.
   [ONLINE]<ROYALPENGUIN1>[monitored]<ROYALPENGUIN1><12357:192.168.1.10><45A4CC9FFF4347168DD8762673E830BC>

   #### no parts to restart

   cryptomove server pid=7637 started...

   think@RoyalPenguin1:~/cryptomove.prepare.1.0.6/cdds$

   *Third host CryptoMove startup:*

   think@RoyalPenguin2:~/cryptomove.prepare.1.0.6/cdds$ crv ss

   hee: HELLO ENGINE V1.0.6 (alpha) COPYRIGHT (C) CRYPTOMOVE 2011-2016.

   (7335) Enter secret key #1 out of 1: # enter 3rd system key here

   think@RoyalPenguin2:~/cryptomove.prepare.1.0.6/cdds$

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 3(min=2/max=3) hosts

   1.
   [ONLINE]<THINK>[monitored]<THINK><12357:192.168.1.4><DBE610BB996B425ABE3ECE518C6AEB7C>

   2.
   [ONLINE]<ROYALPENGUIN1>[monitored]<ROYALPENGUIN1><12357:192.168.1.10><45A4CC9FFF4347168DD8762673E830BC>

   3.
   [ONLINE]<ROYALPENGUIN2>[monitored]<ROYALPENGUIN2><12357:192.168.1.16><AA981823ECBA487B8210F42B4AEC0889>

   #### no parts to restart

   cryptomove server pid=7361 started...

   think@RoyalPenguin2:~/cryptomove.prepare.1.0.6/cdds$

J. After startup, each of three hosts formed its cluster, named locally
   by the default name DIRECT_CRYPTOMOVE_CLUSTER. Each cluster contains
   all hosts.

..

   *On the fiirst host:*

   think@think:~/cryptomove.prepare.1.0.6/cdds$ export
   HELLO_PACKSRC_PATH=/home/think/cryptomove.prepare.1.0.6

   think@think:~/cryptomove.prepare.1.0.6/cdds$ crv os

   (31103) Enter secret key #1 out of 1: # enter 1st system key here

   starting cryptomove system owner client

   (31103)crv>cluster DIRECT_CRYPTOMOVE_CLUSTER --dump

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 3(min=1/max=1) hosts

   1.
   [ONLINE]<ROYALPENGUIN2>[monitored]<ROYALPENGUIN2><12357:192.168.1.16><AA981823ECBA487B8210F42B4AEC0889>

   2.
   [ONLINE]<ROYALPENGUIN1>[monitored]<ROYALPENGUIN1><12357:192.168.1.10><45A4CC9FFF4347168DD8762673E830BC>

   3.
   [ONLINE]<THINK>[monitored]<THINK><12357:192.168.1.4><DBE610BB996B425ABE3ECE518C6AEB7C>

   (31103)crv>

   *On the second host:*

   think@RoyalPenguin1:~/cryptomove.prepare.1.0.6/cdds$ export
   HELLO_PACKSRC_PATH=/home/think/cryptomove.prepare.1.0.6

   think@RoyalPenguin1:~/cryptomove.prepare.1.0.6/cdds$ crv os

   (8465) Enter secret key #1 out of 1: # enter 2nd system key here

   starting cryptomove system owner client

   (8465)crv>cluster DIRECT_CRYPTOMOVE_CLUSTER --dump

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 3(min=1/max=2) hosts

   1.
   [ONLINE]<ROYALPENGUIN2>[monitored]<ROYALPENGUIN2><12357:192.168.1.16><AA981823ECBA487B8210F42B4AEC0889>

   2.
   [ONLINE]<THINK>[monitored]<THINK><12357:192.168.1.4><DBE610BB996B425ABE3ECE518C6AEB7C>

   3.
   [ONLINE]<ROYALPENGUIN1>[monitored]<ROYALPENGUIN1><12357:192.168.1.10><45A4CC9FFF4347168DD8762673E830BC>

   (8465)crv>

Stored Data Space
-----------------

When saving the data, you can direct CryptoMove to place and move the
saved data in different storage spaces, according to one of the
following movement patterns:

+-----------------------+-----------------------+-----------------------+
| **Pattern**           | **How Parts Move**    | **Controls**          |
+=======================+=======================+=======================+
| *Separate*            | Parts saved on other  | To preclude other     |
|                       | clients *never reach* | CryptoMove computer   |
|                       | this client.          | hosts from connecting |
|                       |                       | to this client host,  |
|                       |                       | create an empty file  |
|                       |                       |                       |
|                       |                       | $HELLO_PACKSRC_PATH/. |
|                       |                       | hello_in              |
+-----------------------+-----------------------+-----------------------+
| *Isolated*            | The data saved on     | To preclude the       |
|                       | this client computer  | client host from      |
|                       | *moves* inside its    | connecting to other   |
|                       | CryptoMove store; it  | hosts, create empty   |
|                       | never leaves onto     | file                  |
|                       | other computer hosts. |                       |
|                       |                       | $HELLO_PACKSRC_PATH/. |
|                       |                       | hello_hosts           |
+-----------------------+-----------------------+-----------------------+
| *Client Base*         | Some or all parts of  | This is the default   |
|                       | the data saved on     | -- if no options or   |
|                       | this client computer  | modes are set         |
|                       | *move* both inside    | explicitly, then      |
|                       | its store and onto    | CryptoMove operates   |
|                       | other computer hosts. | in this mode. To      |
|                       |                       | control which parts   |
|                       |                       | to move, use option   |
|                       |                       | ‘–n number’ in        |
|                       |                       | commands \*put\* and  |
|                       |                       | \*mov\* (see          |
|                       |                       | sub-section 4.3).     |
+-----------------------+-----------------------+-----------------------+

CCLI -- Client Operations
-------------------------

CryptoMove Client program allows interacting with the store using
CryptoMove Command Line Interface CCLI. You start up the client program
with the command

   crv cs

The client starts up only if the server on the local host is already
running. You must have shell variable CRYPTOMOVE_PATH set to the
directory containing CryptoMove data store. Any number of clients can
run at the same time connecting to the local server. You can start up a
client from any directory. The client asks you for a command with the
prompt ‘crv>’:

   think@think:~/cryptomove.prepare.1.0.6/bla$ crv cs

   starting cryptomove data user client

   (23181)crv>

CryptoMove server and clients all run on behalf of the Hello runtime
executable /usr/bin/hee. Therefore, they all appear in the list of the
OS processes with the same name. To help distinguish them, each client
dumps its process id before the user prompt, like (23181) in the example
above.

Saving Data
~~~~~~~~~~~

From the client prompt, you can type commands to save data in CryptoMove
data store. There are ways to save a single file, multiple files from
the list of files, files that match one or more regular expressions,
files listed in another file, as well as the files from the list
returned by a shell command. You can either copy or move a file from
your UNIX directory to CryptoMove. While copying just creates a copy of
your data in CryptoMove, moving files causes deletion of the original
after completing the copy. Therefore, if you want to hide your data in
CryptoMove, then you should move it, or copy and then delete it from
your directory with an explicit UNIX command.

Section 4.3.8 lists all CryptoMove data handling commands, explaining
their full syntax and semantics, including all possible parameters,
modes, and options. The current section shows some characteristic
examples – you can start saving data following these examples
immediately.

Saving One File
^^^^^^^^^^^^^^^

To copy just a single file named X type command ‘put X’. When the client
asks you for a data key, enter the key that you have previously
generated as a system owner (or that has been given to you by the system
owner). The client checks your key, finds X in your UNIX directory, and
proceeds to prepare file X for saving in CryptoMove. The result of the
preparation is some data parts that are the permuted, encrypted, padded,
XOR-ed and bit-scattered portions of the original file X, as well as
some additional information. After all preparation steps succeed, the
client commands the server to save the parts in CryptoMove store. By
default, the client dumps on the screen all phases of its operations, so
you should see the following:

   (23181)crv>put X

   \***\* saving commences (entropy 153)

   enter data key:

   1. compressing...

   2. cooking 96 keys.....

   3. making 96 part names....

   4. making 96 hash names....

   5. making 96 trace names.....

   7. permuting....

   8. splitting...

   9. encrypting....

   10. padding & signing...

   11. confusing...

   12. scattering...

   13. flushing...

   14. encrypting traces...

   15. dumping traces...

   16. logging record <#1>...

   17. start moving 96 out of 96...

   \***\* saved file <X>

   (23181)crv>

Details of client operations can be found in the subsequent sections
about CryptoMove Architecture and Reference. Here we just briefly
explain the meaning of the Client’s dump:

   \***\* saving commences (entropy 153)

   The number 153 is the entropy calculated from the number of copies
   and parts of the saved file. The higher the number, the harder it is
   for an attacker to recover the saved file. The minimal default value
   is 127, so the number 153 shown indicates a very low possibility for
   the attacker to recover the saved file.

   enter data key:

   This is a client prompt for the user to enter the data key. The
   client does not echo the input so you cannot see it in this dump.

   1. compressing...

   The client compresses the file before saving by submitting the file
   to the UNIX utility gzip.

   2. cooking 96 keys.....

   Before saving, the client splits the original file into parts, and
   then encrypts each part. This is a preparatory step, which generates
   distinct cryptographic keys each to encrypt its part. By default, the
   client splits a file into 12 parts and creates 4 copies of each part
   – total of 48 parts. Before splitting, it permutes each copy using a
   unique random permutation. Then it splits the permutation into the
   same number of parts – 48 by default. Thus, there are total 48x2=96
   parts of data and permutations. Each one of them possesses a separate
   *key*, *name*, and *hash name*.

   3. making 96 part names....

   Here the client generates unique names for each part. Because of the
   cryptographic algorithm used in this generation, it is impossible to
   correlate the part name to the original file name or the part number.

   4. making 96 hash names....

   The client also generates another set of names – one per part –
   called hash names. The client and the servers will use these names
   when restoring the part back from the CryptoMove store to the client
   store. Again, one cannot correlate these names to any other name in
   the world. The servers encrypt hash names inside the trace files that
   accompany the parts during their movement.

   5. making 96 trace names.....

   A trace file describes a data part. There is as many trace file as
   there are parts. They move together, yet their names cannot be
   correlated. This step generates names for the trace files, one per
   part.

   7. permuting....

   In this step, the client generates a unique random permutation and
   uses it to permute the original data using 16 bytes long portions.

   8. splitting...

   The client splits both data and permutation into parts.

   9. encrypting....

   Using previously generated encryption keys, the client encrypts each
   part created in the previous step.

   10. padding & signing...

   Each encrypted part is padded with the cryptographically generated
   random data of random length. For each result, its cryptographic
   signature is calculated and appended to the encrypted data.

   11. confusing...

   Each data part now is bitwise XOR-ed with each next part, the last
   part XOR-ed with the first part. The separate cycle is performed for
   data and permutation parts.

   12. scattering...

   The client now gathers all data parts back into one contiguous array
   of bytes. After that, a bit of each byte is collected into a separate
   area. Then the client concatenates all areas again and splits it one
   more time into the parts of the original parts’ lengths. The same
   happens with the permutation parts.

   13. flushing...

   The parts resulting from the previous step are all flushed on disk,
   in the server data store under the names generated in step 3.

   14. encrypting traces...

   The client generates a separate trace data for each flushed out part
   and asks the server to encrypt them with the system key. The trace
   contains various parameters related to this part, including the part
   name and its hash name.

   15. dumping traces...

   Each generated trace is dumped into the server data store under the
   trace name generated in step 5.

   16. logging record <#1>...

   A log record is appended to the file’s log containing information
   about the ongoing save operation.

   17. start moving 96 out of 96...

   The client commands the server to start moving the parts and their
   related trace files around the CryptoMove data store.

   \***\* saved file <X>

   This line indicates successful completion of the put command.

When saving data, CryptoMove client assigns a version to each saved
file. For example, in the previous operation it assigned version 1 as
indicated in step 16 because it is the first time this file is saved. If
you save the same file again, CryptoMove assigns version 2 to the saved
data as the next example demonstrates, and so on – incrementing the
version at each next save:

   (23181)crv>put X

   \***\* saving commences (entropy 153)

   enter data key:

   1. compressing...

   2. cooking 96 keys.....

   3. making 96 part names....

   4. making 96 hash names....

   5. making 96 trace names.....

   7. permuting....

   8. splitting...

   9. encrypting....

   10. padding & signing...

   11. confusing...

   12. scattering...

   13. flushing...

   14. encrypting traces...

   15. dumping traces...

   16. logging record <#2>...

   17. start moving 96 out of 96...

   \***\* saved file <X>

   (23181)crv>

You can avoid excessive verbose output by switching the client into a
silent mode using command ‘client silent’. After that, it does not
record each step of its operations on the screen, but performs them
quietly:

   (23181)crv>client silent

   (23181)crv>put X

   enter data key:

   (23181)crv>

Still, in the previous command the client asked you for a data key. If
you are tired entering the same key, switch the client into the ‘trust’
mode – the client will ask you for the key again and then continue using
that key for all subsequent operations. You can always switch the client
back from the trust mode using command ‘trust off’ – then it resumes
asking for keys:

   (23181)crv>trust on

   enter data key:

   (23181)crv>put X

   (23181)crv>

Data Log File
^^^^^^^^^^^^^

After several commands that put file X into the CryptoMove data store,
the store now contains several versions of file X. You can always dump
information about all saved versions of a particular file from that
file’s encrypted log file using command ‘log X’. Each output line
contains a record that describes a saved file version at the beginning
of the record. Record fields are separated from each other by the plus
sign ‘+’:

(23181)crv>log X

1+Sun Sep 6 11:29:47 2015
+9b13505d19ee476789f0874d91c4d946+12+4+b1c8ebd6a7cf5f34e14ace9a767a0d83+32+33204+0+8+1+0+8+8…

2+Sun Sep 6 12:23:15 2015
+6b437766f20448328695b097c609130a+12+4+bc122cf9378bc6b420211b5fc98c32e2+32+33204+0+8+1+0+8+8…

3+Sun Sep 6 12:26:44 2015
+0d04af7f65c54c4793649e13b366a56c+12+4+bdcdf210e69ee6edcf30bb188183bee1+32+33204+0+8+1+0+8+8…

4+Sun Sep 6 12:32:11 2015
+942cd9b30ea045ac8a88b67a583efacc+12+4+a08984d4e1812bdfcd5f4316509f0053+32+33204+0+8+1+0+8+8…

(23181)crv>

The following comments explain some of the fields of the CryptoMove log
record. None of this information is secret because without knowing the
data key and optional key, no attacker can use it to uncover any part of
the saved data:

1+Sun Sep 6 11:29:47 2015
+9b13505d19ee476789f0874d91c4d946+12+4+b1c8ebd6a7cf5f34e14ace9a767a0d83+32+33204+0+8+1+0+8+8+1…

entry_number

current_time

hex_file_uuid

parts_count

copy_count

random seed

key_size

protection

compression_strength

data_depth

move_pace

number_count

scatter_bits

arms_length

permutation

If you know several data keys, you may use different keys while saving
the same or different files. Different keys will generate records in
different log files. Therefore, different keys will create the same file
versions in different log files. Still, in the store, names of all parts
of all versions never collide because their client-generated names are
unique in the world.

All log files are located in the same user directory where the saved
file resides or used to reside. Their names are cryptographically
generated and uncorrelated to the original file name or any part name of
the saved data. Their content is encrypted: without knowing the data
key, it is very hard to decrypt them. All log files have extension .clg:

   (23181)crv>!ls \*.clg

   FEh2vxsRc5tSHTI7FRM2HW5KwSZNeHrQERALOXeOX7oUwaRr4y5T3z0cR0cEKwmstgtWIOiL6CuQXBzhEuC_IgXLfbRrYwQ90nL3iiEHD7egbSERHxo78AYq4RJFI_oatzi7p7grodwVhq2Uu72WSr.crv

   (23181)crv>

Note the above command on the client’s prompt – it is just a UNIX
command ls. CryptoMove client allows you to execute any shell command if
it is preceded with the exclamation sign ‘!’.

Log files are used for two different purposes. First, as a data user,
you may always recall from the logs the history of your data operations,
provided you know the required data key. Second, the client uses
information from the log to delete and restore saved data, fetch the
right number of parts and properly assemble them after decryption.

CryptoMove encrypts all logs. At the same time, the data key and
o0ptional key used to save data is missing from the log. Therefore, the
log information, even if decrypted, **cannot help the attacker to
recover the saved data.**

Restoring and Deleting One File
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once file X has been saved, you can restore it using command ‘get X’:

   (23181)crv>get X

   file <X> exists, use 'force on' to override

   operation has not succeeded

   command failed <get X>

   (23181)crv>force on

   (23181)crv>!rm X

   (23181)crv>!cat X

   cat: X: No such file or directory

   (23181)crv>get X

   (23181)crv>!cat X

   Secret List: Alice Bob Mallory

   (23181)crv>

In the above example, the first attempt to restore X fails because
CryptoMove client detects the file with the same name X in the current
user directory. You can always switch the client into ‘force’ mode –
then it restores the file and overwrites the existing file with the
restored content: the command force on does exactly that. The next two
commands delete file X from the user store – to illustrate the
subsequent successful data restoration from the CryptoMove store. The
next command restores X again – this time it succeeds. The last command
prints the file’s content.

Restore operations are not logged in the log files. However, delete
operations mark the respective record in the log that the saved file has
been deleted. You delete file X using command ‘del X’:

   (23181)crv>del X

   (23181)crv>

Dumping the log after deletion, observe the last record, which indicates
that version 4 of the saved file has been deleted:

1+Sun Sep 6 11:29:47 2015
+9b13505d19ee476789f0874d91c4d946+12+4+b1c8ebd6a7cf5f34e14ace9a767a0d83+32+33204+0+8+1+0+8+8…

2+Sun Sep 6 12:23:15 2015
+6b437766f20448328695b097c609130a+12+4+bc122cf9378bc6b420211b5fc98c32e2+32+33204+0+8+1+0+8+8…

3+Sun Sep 6 12:26:44 2015
+0d04af7f65c54c4793649e13b366a56c+12+4+bdcdf210e69ee6edcf30bb188183bee1+32+33204+0+8+1+0+8+8…

4+Sun Sep 6 12:32:11 2015
+942cd9b30ea045ac8a88b67a583efacc+12+4+a08984d4e1812bdfcd5f4316509f0053+32+33204+0+8+1+4+8+8+DELETED+Sun
Sep 6 14:06:51 2015…

Attempting to restore file X again now fails (after the default timeout
of 20 seconds) because it has been deleted from the CryptoMove store:

   (23181)crv>!rm X

   (23181)crv>get X

   combined strands decryption failed (-1 0)

   \***\* GET failed on <X>

   operation has not succeeded

   (23181)crv>!cat X

   cat: X: No such file or directory

   (23181)crv>

By default, CryptoMove commands get and del operate on the last version
of the saved file. One can always specify a particular lesser file
version for these commands. For example, while the fourth version of
file X has been deleted from the CryptoMove store, you can attempt
restoring the previous third version by setting option ‘-v 3’:

   (23181)crv>get -v 3 X

   (23181)crv>!cat X

   Secret List: Alice Bob Mallory

   (23181)crv>

Now, if you want to delete the second version, observe the resulting log
file, and finally restore the first version, issue the following
commands:

(23181)crv>!rm X

(23181)crv>del -v 2 X

(23181)crv>log X

1+Sun Sep 6 11:29:47 2015
+9b13505d19ee476789f0874d91c4d946+12+4+b1c8ebd6a7cf5f34e14ace9a767a0d83+32+33204+0+8+1+0+8+8

2+Sun Sep 6 12:23:15 2015
+6b437766f20448328695b097c609130a+12+4+bc122cf9378bc6b420211b5fc98c32e2+32+33204+0+8+1+4+8+8+DELETED+Sun
Sep 6 14:19:53 2015…

3+Sun Sep 6 12:26:44 2015
+0d04af7f65c54c4793649e13b366a56c+12+4+bdcdf210e69ee6edcf30bb188183bee1+32+33204+0+8+1+0+8+8

4+Sun Sep 6 12:32:11 2015
+942cd9b30ea045ac8a88b67a583efacc+12+4+a08984d4e1812bdfcd5f4316509f0053+32+33204+0+8+1+4+8+8+DELETED+Sun
Sep 6 14:11:24 2015…

(23181)crv>get -v 2 X

combined strands decryption failed (-1 0)

\***\* GET failed on <X>

operation has not succeeded

(23181)crv>!cat X

cat: X: No such file or directory

(23181)crv>get -v 1 X

(23181)crv>!cat X

Secret List: Alice Bob Mallory

(23181)crv>

Working with Multiple Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^

There is no command in CryptoMove that saves entire user directory with
all its files – you can only save individual files. For example, after
creating a subdirectory, attempt to save it into CryptoMove store
aborts:

   (23181)crv>put sub

   gzip: sub is a directory -- ignored

   /bin/cp: cannot stat 'sub.gz': No such file or directory

   gzip: sub.gz: No such file or directory

   file length is zero -- aborting...

   some file(s) may not have been saved

   (23181)crv>

Still, CryptoMove allows saving, restoring, and deleting multiple files
by executing a single command. The commands for dealing with multiple
files all start with letter ‘m’ – they are mput, mget and mdel.

There are several ways to specify the target files for all three
commands. The simplest one is just to list the files one after another
on the command line, separated by spaces. In the next example, you
create files Y and Z with secret lists in the directory sub and save all
three files X, sub/Y, and sub/Z in one command mput:

   (23181)crv>!echo "Sekretnii spisok: Ivan Nikolai Alena" > sub/Y

   (23181)crv>!echo "Liste Secrete : Jacques Francois Josephine" > sub/Z

   (23181)crv>mput X sub/Y sub/Z

   (23181)crv>

You can also specify one or more regular expressions to denote the
target files. In the next example, command mget fetches the just saved
files X, sub/Y, and sub/Z by specifying two regular expressions: one
‘?’, another ‘sub/?’:

   (23181)crv>mget ? sub/?

   (23181)crv>

Multiple files can be listed in a file one line at a time – you can
specify that file preceded with the indirection symbol ‘@’ – the command
will operate by fetching and operating on one line at a time. The next
example deletes files X, sub/Y and sub/Z from the CryptoMove store by
reading their names from file del_file:

   (23181)crv>!echo X > del_file

   (23181)crv>!echo sub/Y >> del_file

   (23181)crv>!echo sub/Z >> del_file

   (23181)crv>!cat del_file

   X

   sub/Y

   sub/Z

   (23181)crv>mdel @del_file

   (23181)crv>

Lastly, you can execute a shell command that produces a list of files –
one file per line – and dumps that list on its standard output. The
CryptoMove command picks up that list and iterates on all files from the
list. You specify the shell command and its arguments on the CryptoMove
command line, preceded by the exclamation sign ‘!’. The next command
saves files X, sub/Y, and sub/Z again from the ouptu of a shell command
ls:

   (23181)crv>mput !ls -1 ? \*/?

   (23181)crv>

Moving Files
^^^^^^^^^^^^

CryptoMove client has two commands to move data from a client store to
the CryptoMove store – mov and mmov. The former operates on one file,
the latter on multiple files, as explained in the previous subsection
2.10.1.4. Each command copies the file or files into the CryptoMove
store and then deletes them from the client store.

CryptoMove client has no command for the reverse operation – to move
data from the CryptoMove store back into the client store. For that you
should use a sequence of two commands – (m)get and (m)del.

The following example moves all secret lists from the client store into
CryptoMove store, restores them back into the client store, and finally
deletes all lists from the CryptoMove store:

   (23181)crv>mmov X sub/Y sub/Z

   (23181)crv>!ls -lt X sub/Y sub/Z

   ls: cannot access X: No such file or directory

   ls: cannot access sub/Y: No such file or directory

   ls: cannot access sub/Z: No such file or directory

   (23181)crv>mget X sub/Y sub/Z

   /bin/ls: cannot access X: No such file or directory

   /bin/ls: cannot access sub/Y: No such file or directory

   /bin/ls: cannot access sub/Z: No such file or directory

   (23181)crv>get X

   (23181)crv>get sub/Y

   (23181)crv>get sub/Z

   (23181)crv>!ls -lt X sub/Y sub/Z

   -rw-rw-r-- 1 root root 43 Sep 7 09:54 sub/Z

   -rw-rw-r-- 1 root root 37 Sep 7 09:54 sub/Y

   -rw-rw-r-- 1 root root 31 Sep 7 09:54 X

   (23181)crv>mdel ? sub/?

   (23181)crv>

There are several interesting lines in the above example. Command ‘mget
X sub/Y sub/Z’ fails because for m\* commands the client looks up the
file names for its operation in the user directory However, they were
moved out into the CryptoMove store. However, the next three commands
‘get X’, ‘get sub/Y’, and ‘get sub/Z’ succeed because single-file
commands do not look up the user directory.

Client Parameters, Options, and Modes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove client commands offer a rich set of parameters to control
save/restore operations. You can specify one or more parameters on an
individual client command. Also, CryptoMove client can operate in
several modes and options, which affect every command the client
executes. You can specify the mode or option using a special client
command. The reference section 4.3 explains all parameters, options, and
modes in detail. This section just explains them briefly.

To see all current client modes, options and default parameters type
command ‘default’:

   (613)crv>d

   DEFAULT CLIENT PARAMETERS:

   --------------------------

   IN COMMANDS TO SAVE DATA:

   -------------------------

   ARMSLENGTH 4

   CLEANCOMMIT 0

   COPYCOUNT 1

   DATADEPTH 4

   DIRFULL 0

   ENTROPY 0

   EXPIRATION 525600000

   ENCRYPTCLIENT 1

   ENCRYPTSERVER 0

   FILLGROUP 100

   FORCEPURGE 0

   GROUPNAME --------

   HMACCLIENT 0

   LOGSAVE 1

   MODNAME --------

   MODOFFSET 0

   MODPOSITION 1

   MOVEPACE 2

   NOWAIT 0

   NUMBER all

   PERMUTATION 1

   PULSEBEAT 2

   PURGELOG 0

   RECORDCOUNT 0

   SAVELOCAL 0

   SCATTERLENGTH 0

   SIZE 8192

   SPLITCOUNT 1

   SRCID 0

   SRCTYPE LINUX

   STRENGTH 0

   TARGETSTORE -NOTARGET-

   TESTCOUNT 0

   TRACE 0

   USER root

   USRID -ALL-

   UUIDONLY 0

   XORPART 0

   --------------------

   IN COMMANDS TO RESTORE DATA:

   ----------------------------

   LOGRECORD --------

   TIMEOUT 30

   VERSION last

   ----------------------

   CLIENT OPTIONS AND TOGGLED MODES:

   ---------------------------------

   AUDITNAME NONE

   CLIENT VERBOSE

   CTMO 360

   ECHO off

   FORCE off

   KEY off

   LINK off

   TRUST off

   ----------------------

   SERVER OPTIONS:

   ---------------

   APICOUNT 0

   AUDIT on

   BREACH off

   BOXRETRY 0

   COUNT 800

   EDGE false

   EXPIRE off

   KILL off

   MEML 4194304

   POWER 20

   PROGRESS off

   RSHELL off

   STATE [ONLINE]

   STMO 360

   TIMESCALE 1

   TIMESHIFT 120

   TRNC 0

   VERBOSE <SILENT>

   WATCH 0

   WATCHPIDS 0

   ----------------------

   (613)crv>

You can specify one or more parameters on the command line using
parameter keys and their values as explained in the following syntax
diagrams:

+-----------------------------------+-----------------------------------+
| *Saving data*                     |    [mov \| mmov \| put \| mput \| |
|                                   |    putl \| putr \| mod \| modl \| |
|                                   |    com \| coml \| mkdir]          |
|                                   |                                   |
|                                   |    [{-a \| --armslength}          |
|                                   |    ARMSLENGTH ]                   |
|                                   |                                   |
|                                   |    [{-b \| --bitscatter}          |
|                                   |    SCATTERLENGTH ]                |
|                                   |                                   |
|                                   |    [{-c \| --copy} COPYCOUNT ]    |
|                                   |                                   |
|                                   |    [{-co \| --compress} STRENGTH  |
|                                   |    ]                              |
|                                   |                                   |
|                                   |    [{-cl \| --clean} CLEANCOMMIT  |
|                                   |    ]                              |
|                                   |                                   |
|                                   |    [{-cu \| --cur} MODOFFSET ]    |
|                                   |                                   |
|                                   |    [{-d \| --depth} DATADEPTH ]   |
|                                   |                                   |
|                                   |    [{-e \| --entropy} ENTROPY ]   |
|                                   |                                   |
|                                   |    [{-ec \| --encryptclient}      |
|                                   |    CLIENTENCRYPT]                 |
|                                   |                                   |
|                                   |    [{-en \| --end} MODOFFSET ]    |
|                                   |                                   |
|                                   |    [{-es \| --encryptserver}      |
|                                   |    SERVERENCRYPT]                 |
|                                   |                                   |
|                                   |    [{-ex \| --expire} EXPIRATION  |
|                                   |    ]                              |
|                                   |                                   |
|                                   |    [{-g \| --group} GROUPNAME ]   |
|                                   |                                   |
|                                   |    [{-hc \| --hmacclient}         |
|                                   |    CLIENTHMAC ]                   |
|                                   |                                   |
|                                   |    [{-in \| --in} MODNAME ]       |
|                                   |                                   |
|                                   |    [{-l \| --log} LOGSAVE ]       |
|                                   |                                   |
|                                   |    [{-m \| --move} MOVEPACE ]     |
|                                   |                                   |
|                                   |    [{-n \| --number} NUMBER ]     |
|                                   |                                   |
|                                   |    [{-p \| --pulse} PULSEBEAT ]   |
|                                   |                                   |
|                                   |    [{-pe \| --permute}            |
|                                   |    PERMUTATION ]                  |
|                                   |                                   |
|                                   |    [{-pu \| --purge} PURGELOG ]   |
|                                   |                                   |
|                                   |    [{-r \| --remote} REMOTENAME ] |
|                                   |                                   |
|                                   |    [{-rs \| --resize} MODSIZE ]   |
|                                   |                                   |
|                                   |    [{-s \| --split} SPLITCOUNT ]  |
|                                   |                                   |
|                                   |    [{-sa \| --save} SAVELOCAL ]   |
|                                   |                                   |
|                                   |    [{-se \| --set} MODOFFSET ]    |
|                                   |                                   |
|                                   |    [{-si \| --size} SIZE ]        |
|                                   |                                   |
|                                   |    [{-to \| --to} TARGETSTORE ]   |
|                                   |                                   |
|                                   |    [{-uo \| --uuidonly} UUIDONLY  |
|                                   |    ]                              |
|                                   |                                   |
|                                   |    [{-v \| --version} VERSION ]   |
|                                   |                                   |
|                                   |    [{-x \| --xor} XORPART ]       |
|                                   |    [FILENAME... \|                |
|                                   |                                   |
|                                   |    @FILELIST \|                   |
|                                   |                                   |
|                                   | !SHELLCOMMAND]                    |
+===================================+===================================+
| *Restoring data*                  | {get \| mget \| getl}             |
|                                   |                                   |
|                                   | [{-lr \| --lr} LOGRECORD]         |
|                                   |                                   |
|                                   | [{-v \| --version} VERSION ]      |
|                                   |                                   |
|                                   | [{-n \| --number} NUMBER ]        |
|                                   |                                   |
|                                   | [{-nw \| --nowait} NOWAIT ]       |
|                                   |                                   |
|                                   | [{-r \| --remote} REMOTENAME]     |
|                                   |                                   |
|                                   | [{-t \| --timeout} TIMEOUT ]      |
|                                   |                                   |
|                                   | [{-u \| --unittest} TESTCOUNT]    |
|                                   |                                   |
|                                   | [{-us \| --user} USERNAAME]       |
|                                   | [FILENAME... \|                   |
|                                   |                                   |
|                                   | @FILELIST \|                      |
|                                   |                                   |
|                                   | !SHELLCOMMAND]                    |
+-----------------------------------+-----------------------------------+
| *Deleting data*                   | {del \| mdel \| dell}             |
|                                   |                                   |
|                                   | [{-lr \| --lr} LOGRECORD ]        |
|                                   |                                   |
|                                   | [{-v \| --version} VERSION ]      |
|                                   |                                   |
|                                   |    [{-r \| --remote} REMOTENAME]  |
|                                   |                                   |
|                                   | [{-t \| --timeout} TIMEOUT ]      |
|                                   | [FILENAME... \|                   |
|                                   |                                   |
|                                   | @FILELIST \|                      |
|                                   |                                   |
|                                   | !SHELLCOMMAND]                    |
+-----------------------------------+-----------------------------------+

The following table lists all command parameters with their brief
explanations; their default values can be seen from the output of the
previous default command:

+-----------------------------------+-----------------------------------+
| **Parameter**                     | **Explanation**                   |
+===================================+===================================+
| CLEANCOMMIT                       | Delete modificatyion data after   |
|                                   | commit succeeds.                  |
+-----------------------------------+-----------------------------------+
| COPYCOUNT                         | Number of copies of the saved     |
|                                   | data.                             |
+-----------------------------------+-----------------------------------+
| DATADEPTH                         | The maximum depth of the path     |
|                                   | when a file part traverses the    |
|                                   | network.                          |
+-----------------------------------+-----------------------------------+
| ENCRYPTCLIENT                     | Toggles data encryption on the    |
|                                   | client side.                      |
+-----------------------------------+-----------------------------------+
| ENCRYPTSERVER                     | Toggles data encryption on the    |
|                                   | server side.                      |
+-----------------------------------+-----------------------------------+
| ENTROPY                           | This number determines the        |
|                                   | cumulative entropy of the split   |
|                                   | data parts.                       |
+-----------------------------------+-----------------------------------+
| EXPIRATION                        | Time in minutes after which the   |
|                                   | saved data expires.               |
+-----------------------------------+-----------------------------------+
| MODOFFSET                         | Defines offset from various data  |
|                                   | positions to modify the already   |
|                                   | saved data.                       |
+-----------------------------------+-----------------------------------+
| MODNAME                           | Defines name of file containing   |
|                                   | modification data.                |
+-----------------------------------+-----------------------------------+
| MOVEPACE                          | This number determines how many   |
|                                   | times a day a part moves.         |
+-----------------------------------+-----------------------------------+
| NUMBER                            | Indicates how many parts of saved |
|                                   | file to move around the network.  |
+-----------------------------------+-----------------------------------+
| NOWAIT                            | Commands the server to wait or    |
|                                   | not to wait for the delivery of   |
|                                   | missing pulse events.             |
+-----------------------------------+-----------------------------------+
| PERMUTATION                       | Triggers random permutation of    |
|                                   | data parts before saving them in  |
|                                   | the data store.                   |
+-----------------------------------+-----------------------------------+
| PULSEBEAT                         | This number determines how many   |
|                                   | times a day a pulse message is    |
|                                   | generated from the moment a part  |
|                                   | lands on the host till the time   |
|                                   | it departs from the host.         |
+-----------------------------------+-----------------------------------+
| PURGELOG                          | Toggles purging log records when  |
|                                   | committing changes.               |
+-----------------------------------+-----------------------------------+
| RECORDCOUNT                       | Sets the log record number from   |
|                                   | the end of the log to display in  |
|                                   | log command.                      |
+-----------------------------------+-----------------------------------+
| REMOTENAME                        | The name of the saved file it     |
|                                   | acquires in the CryptoMove        |
|                                   | Namespace.                        |
+-----------------------------------+-----------------------------------+
| SCATTERLENGTH                     | This length of the bit-scattering |
|                                   | interval.                         |
+-----------------------------------+-----------------------------------+
| SAVELOCAL                         | Cave locally or on standard       |
|                                   | output the committed data.        |
+-----------------------------------+-----------------------------------+
| SPLITCOUNT                        | This number determines into how   |
|                                   | many parts the client splits the  |
|                                   | file before saving the parts in   |
|                                   | CryptoMove data store.            |
+-----------------------------------+-----------------------------------+
| STRENGTH                          | This compression strength of the  |
|                                   | saved data.                       |
+-----------------------------------+-----------------------------------+
| TARGETSTORE                       | Optional host and directory that  |
|                                   | confine the data movement.        |
+-----------------------------------+-----------------------------------+
| TESTCOUNT                         | This number determines how many   |
|                                   | times to repeat the specified     |
|                                   | command.                          |
+-----------------------------------+-----------------------------------+
| TIMEOUT                           | The number of seconds the client  |
|                                   | waits until CryptoMove servers    |
|                                   | complete the operation.           |
+-----------------------------------+-----------------------------------+
| UUIDONLY                          | Produces short version of the log |
|                                   | record.                           |
+-----------------------------------+-----------------------------------+
| VERSION                           | The version of saved file to      |
|                                   | operate upon, or the versions of  |
|                                   | purged log records.               |
+-----------------------------------+-----------------------------------+
| XORPART                           | Controls mutual XOR-ing of saved  |
|                                   | parts.                            |
+-----------------------------------+-----------------------------------+

You can change any of the client mode and options using a respective
client mode command:

   auditname [AUDITNAME \| NONE]

   client [VERBOSE \| SILENT]

   ctmo [TIMEOUT]

   {e \| echo} [on \| off]

   {f \| force} [on \| off]

   {k \| key} [on \| off]

   {l \| link} [on \| off]

   meml [SIZE]

   {t \| trust} [on \| off]

The following table explains all client modes and options:

+-----------------------------------+-----------------------------------+
| **Parameter**                     | **Explanation**                   |
+===================================+===================================+
| auditname                         | Sets audit name of the current    |
|                                   | client – this name tags every     |
|                                   | client’s audit log message.       |
+-----------------------------------+-----------------------------------+
| client                            | Toggles verbose or silent mode    |
|                                   | for all client commands.          |
+-----------------------------------+-----------------------------------+
| ctmo                              | Client command timeout.           |
+-----------------------------------+-----------------------------------+
| echo                              | Toggles printing on the standard  |
|                                   | output the key which is typed     |
|                                   | from on the standard inptu.       |
+-----------------------------------+-----------------------------------+
| force                             | Allows or prohibits restoring     |
|                                   | files from the CryptoMove store   |
|                                   | on top of the existing file in    |
|                                   | the user store.                   |
+-----------------------------------+-----------------------------------+
| key                               | Saves the secret key for use in   |
|                                   | subsequent client commands.       |
+-----------------------------------+-----------------------------------+
| link                              | Creates a link to the log of a    |
|                                   | saved file.                       |
+-----------------------------------+-----------------------------------+
| meml                              | Setrs maximum size of locked      |
|                                   | virtual memory for the data for   |
|                                   | putl command.                     |
+-----------------------------------+-----------------------------------+
| trust                             | Saves the data key for use in     |
|                                   | subsequent client commands.       |
+-----------------------------------+-----------------------------------+

Client Operations for System Owner
----------------------------------

If you are a system owner, you can start up the client in the system
owner mode like this:

   think@think:~/cryptomove.prepare.1.0.6/bla$ crv os

   (3725) Enter secret key #1 out of 1: # enter system owner key here

   starting cryptomove system owner client

   (3725)crv>

As a system owner, you can run the following client commands that
control local CryptoMove server:

   audit [on \| off]

   breach [on \| off]

   count [PERIOD]

   edge

   kill [on \| off]

   power [POWERVALUE]

   progress [on \| off]

   silent [PATH \|

   MOVE \|

   PULSE \|

   DELETE \|

   TRACK \|

   WATCH \|

   GROUP \|

   RACE \|

   DECRYPT \|

   ALL]

   state

   stmo [TIMEOUT]

   timeshift [SHIFT]

   trnc [PERIOD]

   verbose [PATH \|

   MOVE \|

   PULSE \|

   DELETE \|

   TRACK \|

   WATCH \|

   GROUP \|

   RACE \|

   DECRYPT \|

   ALL]

   watch [PERIOD]

   watchpids [PID ...]

The following table briefly explains server options (see sub-section
4.3.7 for details):

+-----------------------------------+-----------------------------------+
| **Command or**                    | **Explanation**                   |
|                                   |                                   |
| **Parameter**                     |                                   |
+===================================+===================================+
| audit                             | Indicates if the server shall     |
|                                   | audit user commands.              |
+-----------------------------------+-----------------------------------+
| breach                            | Indicates if the server must      |
|                                   | declare itself breached upon      |
|                                   | detecting an intrusion.           |
+-----------------------------------+-----------------------------------+
| edge                              | Indicates if CryptoMove server is |
|                                   | running in edge mode.             |
+-----------------------------------+-----------------------------------+
| kill                              | Indicates if the server shall     |
|                                   | kill an intruding process.        |
+-----------------------------------+-----------------------------------+
| trnc                              | This value defines the frequency  |
|                                   | the server logs the total number  |
|                                   | of parts in its data store.       |
+-----------------------------------+-----------------------------------+
| power                             | Indiciates how frequently the     |
|                                   | server moves the saved data       |
|                                   | parts.                            |
+-----------------------------------+-----------------------------------+
| progress                          | Indiocates if the server must     |
|                                   | show progress of the command      |
|                                   | execution.                        |
+-----------------------------------+-----------------------------------+
| rshell                            | Allows or prohibits the server to |
|                                   | execute local and remote shell    |
|                                   | commands.                         |
+-----------------------------------+-----------------------------------+
| silent                            | Controls trace level of server    |
|                                   | runtime. Not available in         |
|                                   | production version.               |
+-----------------------------------+-----------------------------------+
| state                             | Shows current state of the        |
|                                   | CryptoMove server.                |
+-----------------------------------+-----------------------------------+
| stmo                              | This sets timeout for             |
|                                   | server-to-server requests.        |
+-----------------------------------+-----------------------------------+
| timeshift                         | Allowes discrepancy in seconds    |
|                                   | between CryptoMove internal clock |
|                                   | and OS realtime clock.            |
+-----------------------------------+-----------------------------------+
| trnc                              | Controls server log truncation    |
|                                   | period.                           |
+-----------------------------------+-----------------------------------+
| verbose                           | Controls trace level of server    |
|                                   | runtime. Not available in         |
|                                   | production version.               |
+-----------------------------------+-----------------------------------+
| watch                             | Toggles the watch mode where      |
|                                   | server watches for foreign        |
|                                   | processes to access its data      |
|                                   | store.                            |
+-----------------------------------+-----------------------------------+
| watchpids                         | Contains list of process ids for  |
|                                   | processes with permission to      |
|                                   | access CryptoMove data store.     |
+-----------------------------------+-----------------------------------+

CCLI Automation Using Command Files
-----------------------------------

CryptoMove client also allows you to automate execution of the sequences
of CCLI commands by combining them into a command file and then
executing that file. This way, the commands from the file run one after
another, without the need to enter them one at a time from the client
program’s prompt. In addition to individual commands, command file may
contain sequential or concurrent invocation of other command files.

Suppose you have prepared a file F containing CCLI commands. Then there
are several ways to run F:

+-----------------------------------+-----------------------------------+
| **Method**                        | **Explanation**                   |
+===================================+===================================+
| When starting a client program    | Invoke the client program like    |
|                                   | crv @F cs or crv @F os. The       |
|                                   | client executes all commands from |
|                                   | F and then exits.                 |
+-----------------------------------+-----------------------------------+
| From a client prompt              | Launch F from the already running |
|                                   | client program, at its prompt,    |
|                                   | like @F or @@F. In the former     |
|                                   | case, the client program waits    |
|                                   | until F completes and then        |
|                                   | returns to the prompt. In the     |
|                                   | latter case, the client program   |
|                                   | launches F and then immediately   |
|                                   | returns to the prompt without     |
|                                   | waiting for F to complete.        |
+-----------------------------------+-----------------------------------+

Sub-sections 4.3.4.3 and 4.3.4.4 explain the details of the command file
execution.

Architecture
============

Overview of the Encrypted Data Movement
---------------------------------------

CryptoMove allows users to save their data in the CryptoMove distributed
data store and to retrieve the saved data from the store. By
*continuously encrypting*, *renaming* and *randomly moving* the split
data parts around its store, CryptoMove conceals the stored data. This
process assures that **nobody can identify and locate the stored data
without knowing the key**. Therefore, neither an external adversary nor
an insider attacker, even with root privileges, can discover the data to
start a cryptographic attack. The same strategy avoids data corruption
and loss during software and hardware failures.. In addition, CryptoMove
watches for interference with its operations and for unauthorized access
to its data store. When it detects an intrusion, it moves the stored
data away.

Location of the User Data
~~~~~~~~~~~~~~~~~~~~~~~~~

Cryptomove saves user data located in one of the following data sources:

-  A file from a local file system.

-  A file from a cloud file system. The current version supports saving
   files from AWS S3 and Box clouds.

-  A memory buffer supplied from an application linked with CryptoMove C
   language API library.

-  Data supplied from a standard input of the CryptoMove client program
   crv.

Location of the CryptoMove Data Store
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cryptomove data store can be located in one or more of the following
locations:

-  A local UNIX file system.

-  A UNIX file system on a remote CryptoMove server.

-  A cloud file system such as AWS S3 or Box.

When CryptoMove servers save and move data in their data stores, the
saved data parts may move either confined to one or more UNIX file
systems, to one or more clouds, or to a mix of any number of UNIX file
systems and clouds, depending on the configuration set up for each
server.

CryptoMove Data Operations
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _saving-data-1:

Saving Data
^^^^^^^^^^^

To save data in CryptoMove, the user supplies the data, from a named
file or standard input, and a key to the CryptoMove client program,
which performs the following protective operations:

-  compresses and duplicates the data,

-  randomly permutes original and duplicate data,

-  randomly splits all into randomly named parts,

-  encrypts the parts,

-  pads the parts with random data,

-  cyclically XOR-s resulting parts,

-  bit-scatters the parts.

Finally, it sends the parts to multiple CryptoMove base servers
distributed over the network. The base servers save the received parts
in the persistent store and initiate a continuous cycle of random

-  encryption,

-  renaming,

-  padding,

-  network movement.

**This way, the saved data do not persist in a fixed place, but always
change their locations, content and names in a random fashion.** Parts
move independently of each other in a star-like pattern, following
random paths that lead deep into the network up to a certain depth, and
back to the base server.

When a server decides to move a part, it can end up leaving that part in
place, move it forward, or move it backward. The forward movement is
unpredictable – each server chooses the next server, to which it sends
the part, randomly and independently from other servers and parts. When
moving backward, it always moves back to the server from where the part
has come onto the current server, but still randomly and independently
of other servers and parts. The destination host for either forward or
backward movement can be the same current host. The following figure
illustrates the movement of different parts on the network.

.. image:: /images/saving_data.png

Restoring Data
^^^^^^^^^^^^^^

When a part lands on a server, it schedules a periodic pulse message
back to the base server that originated its movement. When the part
departs from the server, that pulse is terminated. When the pulse
message visits a server on its way, it drops an encrypted randomly named
track file that points to the immediate server the pulse had come from.
Next pulse creates a new track and deletes the previous track. When the
pulse is terminating, all its track files throughout the network are
deleted [12]_.

The ever-changing temporary paths of connected tracks lead from the base
servers to the current location of the part. CryptoMove uses these
temporary paths to restore the data as follows:

-  The user supplies the data or file name, together with the data key,
   to the CryptoMove client program, which submits them to the
   CryptoMove servers.

-  The servers communicate amongst themselves to retrace, following the
   temporary tracks, the prior paths of the moving data parts and enable
   them to be collected.

-  Finally, the client program recombines the collected parts, decrypts
   the data one last time and delivers it to the user.

Modifying Data
^^^^^^^^^^^^^^

After data has been saved in CryptoMove data store, it can be modified
while in the store. Each modification is saved in the data store as a
separate piece of data, which is subject to movement and mutation. A
series of modifications can be committed, which creates a new version of
the saved data with all modifications applied.

Deploying Decoys
^^^^^^^^^^^^^^^^

CryptoMove client program allows for storing random data in specified OS
directories on specified hosts. This data is subject to movement and
mutation, and can serve as decoys for real customer data, in order to
attract and monitor potential attackers.

CryptoMove Namespace
~~~~~~~~~~~~~~~~~~~~

When saving data, the user may assign to the saved data a remote name.
Cryptomove servers maintain a common CryptoMove Namespace for saved data
remote names. Using the remote name, the user can restore the saved
named data into any directory onto any host under any file name. When
saving data without assigning it a remote name, the saved data can be
restored only under the same name in the same directory onto the same
host from which it has been saved.

CryptoMove Directories
~~~~~~~~~~~~~~~~~~~~~~

CryptoMove data store can be optionally organized into a hierarchy of
(sub-) directories. Each directory contains a list of names of saved
data and files, and other sub-directories. The client program saves and
maintains that list in the data store automatically in the course of
saving user data and files. The directory information is secure because
it is a subject of CryptoMove concealment operations such as encryption,
split, movement, etc. Cryptomove allows access to its directories from
any client, because directory names belong to CryptoMove namespace.

Using Strong Symmetric Keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~

For secure encryption, CryptoMove supplies automatically generated
*symmetric keys* based on the entropy bits collected from local or
distributed entropy sources. As the parts move around, each server
re-encrypts the arriving part with its key on top of the encryption made
by the previous server.

Maintaining Data Quorum
~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove servers continuously exchange the hashed results of their
protective operations. This way, the system self-checks and drops
corrupt data from further operations.

Intrusion Detection
~~~~~~~~~~~~~~~~~~~

CryptoMove server actively watches for two distinct conditions, caused
by a potential intrusion, that may jeopardize its operations and
integrity of the stored data:

-  **Continuous runtime flow.** The server watches if its runtime
   scheduling results in the uneven allocation of the processor time
   (e.g. when a debugger or any other process attaches to the server for
   the purpose of analyzing its runtime behavior or reading its internal
   memory).

-  **Unauthorized access to the data store.** The server watches if
   another process opens files in its data store.

In both cases, the server immediately schedules all pending data
movement requests. This way, the server mutates and moves away the
stored data as soon as possible, thus avoiding the potential attacker’s
intrusion. As a result, the potential intruder may not access enough CPU
cycles and system resources needed to complete the attack. The server
also changes its state to breached after detecting an intrusion.

Secure Data Redundancy
~~~~~~~~~~~~~~~~~~~~~~

Because CryptoMove duplicates data parts and spreads them randomly over
the network and across distinct partitions of the different overlapping
server groups, it assures enough data is still available even after some
of the CryptoMove servers fail, become breached, or go offline. The same
code that moves data continuously reshapes server groups and their
partitions, thus maintaining the security of the saved data while
preventing attacker from identifying groups and their partitions.

Administration
~~~~~~~~~~~~~~

CryptoMove software can reside in corporate data centers, in the cloud,
and even across different clouds. Also, it can operate on the Internet
utilizing computers that belong to independent owners from remote
geographical regions. Because it keeps the file parts in the native file
system, each CryptoMove operator is free to utilize any backup/restore
facility while using any data storage systems to add/remove the storage
capacity.

Auditing
~~~~~~~~

CryptoMove allows for auditing: every user command can be logged in the
system log syslog for further analyses and archiving.

Defeating Insider Attacks
~~~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove protects user data not only from external attackers, but also
from malicious insiders who operate its servers, clients, and data
store. Due to the nature of the CryptoMove’s pro-active data defense, no
CryptoMove system owner can understand the contents of the saved data;
neither can she recover the data without knowing the original data key
and key.

**Knowing the root keys on every computer cannot help as it only opens
access to a massive amount of continuously re-encrypted unidentified
name- and content-changing file parts moving around the network.**

CryptoMove architecture makes data recovery without the correct data key
impossible under any threats or legal orders. Stealing data by an
insider (even possessing the root keys) also brings no benefit, as it is
impossible to decrypt the whole data from random file parts without the
data key and key. Moreover, the servers protect against the data theft
in real time by watching for intrusion into their operations and
unauthorized data access.

CryptoMove Environment
----------------------

Deployment
~~~~~~~~~~

CryptoMove versatile architecture allows for its deployment in a variety
of environments – anywhere its servers can establish TCP/IP connections:

+-----------------------------------+-----------------------------------+
| 4. *Corporate Data Center*        | CryptoMove can work inside a      |
|                                   | corporate cloud located on        |
|                                   | premises in a single data center  |
|                                   | or spread out geographically.     |
+===================================+===================================+
| 1. *Shared Clouds*                | CryptoMove can maintain data for  |
|                                   | one, two or any number of         |
|                                   | distinct cloud users, individual  |
|                                   | or corporate.                     |
+-----------------------------------+-----------------------------------+
| 2. *Mixed Environment*            | CryptoMove can maintain data      |
|                                   | spread out between the clouds     |
|                                   | located on premises and inside    |
|                                   | cloud provider’s data centers,    |
|                                   | and corporate data centers        |
+-----------------------------------+-----------------------------------+
| 3. *Peer to Peer*                 | CryptoMove can work on a variety  |
|                                   | of peer computers spread over the |
|                                   | Internet, efficiently moving data |
|                                   | between them.                     |
+-----------------------------------+-----------------------------------+

System and Data Key Administration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When deployed in the corporate data center and the clouds, the system
administrators may become CryptoMove system owners. They establish and
control the system keys. When deployed over the internet, the
participating peers necessarily become both system owners and data
users. They create a web of trust outside of the CryptoMove proper.
There, they distribute the data keys.

Data Store Administration
~~~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove stores its data in the native file systems. For this reason,
it allows for an independent data store administration as long as its
policies preserve the content of the native file system. In particular,
system administrators can backup and restore file system, add more
storage, exclude the storage from CryptoMove operations, add and remove
computers with CryptoMove daemons.

CryptoMove System Components 
-----------------------------

CryptoMove works with the following hardware infrastructure:

+-----------------------------------+-----------------------------------+
| -  **Computers**                  | Keep the saved data and           |
|                                   | CryptoMove control data inside    |
|                                   | their *native* *file systems*.    |
+===================================+===================================+
| -  **Native file system**         | Any number of file systems,       |
|                                   | local, distributed, or            |
|                                   | cloud-based where computers store |
|                                   | and retrieve files in various     |
|                                   | (sub-) directories.               |
+-----------------------------------+-----------------------------------+
| -  **Network**                    | Connects the computers and moves  |
|                                   | the data between them.            |
+-----------------------------------+-----------------------------------+

CryptoMove software runs as one, two or more *daemons* on networked
computers. To exchange and safeguard data, the daemons communicate, move
user data in and out of the native file systems, and around the network
within CryptoMove.

Software Daemon
---------------

Each computer runs one or more *CryptoMove Daemons*. At different times,
depending on the task, the daemon assumes the roles of the *manager,*
*server* or *client*:

1. **CryptoMove manager**\ *manages keys, starts up and shuts down*
   CryptoMove servers; starts up CryptoMove clients.

2. **CryptoMove server** continuously performs the following *protective
   operations*:

+-----------------------------------+-----------------------------------+
| -  *Encryption and signing*       | Conceals data and assures its     |
|                                   | integrity.                        |
+===================================+===================================+
| -  *Mutation*                     | Pads and renames data.            |
+-----------------------------------+-----------------------------------+
| -  *Random movement*              | Moves data around the network,    |
|                                   | proactively evading a potential   |
|                                   | attacker.                         |
+-----------------------------------+-----------------------------------+
| -  *Online monitoring*            | Makes sure there are always       |
|                                   | enough online CryptoMove servers. |
+-----------------------------------+-----------------------------------+
| -  *Quorum maintenance*           | Verifies other server’s           |
|                                   | operations, drops damaged data.   |
+-----------------------------------+-----------------------------------+
| -  *Intrusion watch*              | Detects unauthorized data access  |
|                                   | and intrusion into server’s       |
|                                   | operations.                       |
+-----------------------------------+-----------------------------------+
| -  *Availability & Redundancy*    | Continuously and randomly         |
|                                   | reshapes redundancy groups and    |
|                                   | their partitions.                 |
+-----------------------------------+-----------------------------------+

3. **CryptoMove client** runs on behalf of the data users and system
   owners. It connects to the CryptoMove servers to move the data in and
   out of the CryptoMove store. In addition, the client performs the
   following operations:

+-----------------------------------+-----------------------------------+
| -  *Compression*                  | Compresses cleartext data to a    |
|                                   | compact form.                     |
+===================================+===================================+
| -  *Encryption and signing*       | Conceals data and assures its     |
|                                   | integrity.                        |
+-----------------------------------+-----------------------------------+
| -  *Mutation*                     | Permutes, partitions, duplicates, |
|                                   | pads and bit-scatters data.       |
+-----------------------------------+-----------------------------------+
| -  *Reassembling*                 | Decrypts and reassembles the data |
|                                   | after retrieval from the store.   |
+-----------------------------------+-----------------------------------+
| -  *Cluster control*              | Controls the cluster of available |
|                                   | computers used to move the saved  |
|                                   | data.                             |
+-----------------------------------+-----------------------------------+
| -  *Server Control*               | Controls runtime modes and        |
|                                   | options on the local CryptoMove   |
|                                   | server.                           |
+-----------------------------------+-----------------------------------+

4. **CryptoMove API client** is a user written program that uses
   CryptoMove C language API (CCAPI) and runs on behalf of the data
   users and system owners. It first forks and then connects to one or
   more CryptoMove Clients. Each forked out client then invokes an API
   thread inside the local CryptoMove server, connects the API client
   with that API thread, and then exits. As a result, the API client
   directly communicates with one or more API threads inside the
   CryptoMove server.

Protection Goals
----------------

CryptoMove daemon employs protective operations towards its *security
goals*, which are *authorization, anonymity,* *confidentiality*,
*integrity*, *availability, redundancy,* and *access control*:

+---------+---------+---------+---------+---------+---------+---------+
| **Goals | **Autho | **Anony | **Confi | **Integ | **Avail | **Acces |
| **      | rizatio | mity**  | dential | rity**  | ability | s       |
|         | n**     |         | ity**   |         | &       | Control |
| **Opera |         | *Stored |         | *the    | Redunda | **      |
| tions** | *Only   | data    | *Nobody | data    | ncy**   |         |
|         | authori | does    | but the | retriev |         | *nobody |
|         | zed     | not     | owner   | ed      | *Saved  | but the |
|         | data    | reveal  | can     | is the  | data is | owner   |
|         | users   | the     | discove | same as | always  | can     |
|         | can     | identit | r       | the     | availab | restore |
|         | save/   | y       | the     | data    | le      | the     |
|         | restore | of the  | content | saved*  | for     | data*   |
|         | data*   | data    | of the  |         | retriev |         |
|         |         | user*   | saved   |         | al*     |         |
|         |         |         | data*   |         |         |         |
+=========+=========+=========+=========+=========+=========+=========+
| *Compre |         |         | +       |         |         |         |
| ssion*  |         |         |         |         |         |         |
+---------+---------+---------+---------+---------+---------+---------+
| *Encryp |         |         | +       | +       |         | +       |
| tion    |         |         |         |         |         |         |
| and     |         |         |         |         |         |         |
| signing |         |         |         |         |         |         |
| *       |         |         |         |         |         |         |
+---------+---------+---------+---------+---------+---------+---------+
| *Mutati |         |         | +       | +       | +       |         |
| on*     |         |         |         |         |         |         |
+---------+---------+---------+---------+---------+---------+---------+
| *Random |         |         | +       | +       | +       |         |
| movemen |         |         |         |         |         |         |
| t*      |         |         |         |         |         |         |
+---------+---------+---------+---------+---------+---------+---------+
| *Quorum |         |         |         | +       |         |         |
| mainten |         |         |         |         |         |         |
| ance*   |         |         |         |         |         |         |
+---------+---------+---------+---------+---------+---------+---------+
| *Key    | +       | +       |         |         |         | +       |
| managem |         |         |         |         |         |         |
| ent*    |         |         |         |         |         |         |
+---------+---------+---------+---------+---------+---------+---------+
| *Intrus |         |         |         | +       | +       |         |
| ion     |         |         |         |         |         |         |
| watch*  |         |         |         |         |         |         |
+---------+---------+---------+---------+---------+---------+---------+
| *Random |         |         | +       | +       | +       |         |
| Group   |         |         |         |         |         |         |
| Reshapi |         |         |         |         |         |         |
| ng*     |         |         |         |         |         |         |
+---------+---------+---------+---------+---------+---------+---------+

Users
-----

There are two categories of CryptoMove users:

-  **Data users** -- own the data that they save and restore in
   CryptoMove data store. A data user is sometimes a system owner if she
   is responsible for starting up and shutting down CryptoMove server on
   the client computer. That computer utilizes its native store and a
   network interface to communicate with other CryptoMove computers.

-  **System owners** -- own CryptoMove components such as computers,
   native file systems, and daemons, starts up, shuts down and manages
   CryptoMove servers.

Keys
----

CryptoMove automatically generates symmetric *system and data keys*
using available entropy sources. It does not store them persistently.
Instead, it stores their *hashes*. In addition, CryptoMove can store
hashes of either system or data keys that it had not generated. In
either case, it uses saved hashes to verify keys supplied for any
operation that requires a key.

System and Data Keys
~~~~~~~~~~~~~~~~~~~~

CryptoMove does not contain any centralized or distributed user
database. Because of that, it does not authenticate users. CryptoMove
controls access to its components via keys:

-  Data users control access to the saved file by keeping a *secret*
   **data key**.

-  System owners control access to their system resources -- computers,
   native file systems, and daemons -- by keeping their
   *secret*\ **system keys**.

.. image:: /images/system_and_data_keys.png

Key Revocation
~~~~~~~~~~~~~~

CryptoMove *system key revocation* denies the attacker, who obtained
previous system key, access to the server data, yet allows for
legitimate use, with the new key, of data encrypted previously with the
compromised key. Similarly, CryptoMove *data key revocation* denies the
attacker, who obtained previous data key, access to data stored in
CryptoMove store, yet allows the legitimate user to still access, using
a new key, data previously saved with the compromised data key.

System Key Revocation
^^^^^^^^^^^^^^^^^^^^^

The system owner can revoke a system key. When revoking a system key,
the server generates a new system key. From that point, the system owner
must use the new system key instead of the revoked one in all CryptoMove
operations that require a system key. However, if the server receives a
data part that comes back to it encrypted with the revoked key, the
server is still able to decrypt such part because it remembers the old
system key encrypted with the new key.

Data Key Revocation
^^^^^^^^^^^^^^^^^^^

The system owner can revoke a data key. When revoking a data key, the
server generates a new data key. From that point, the user cannot
utilize revoked data key in any data operations. However, the user can
still restore, check, or delete the data saved with the revoked data key
using the new data key because the server remembers the revoked key
encrypted with the new key.

Data Key Distribution and Copy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The data key generated on a given client host allows for saving and
restoring data only from and to that host. After this key is distributed
on other client hosts, they become capable of accessing the data created
by the original host. Using distributed keys, one can save and restore
data from anywhere on the network as long as the client host has the
distributed duplicate of the original key.

CryptoMove can duplicate an existing data key. The duplicate, also
called a *copy key*, can be used interchangeably with the original key
on the same client host. Different copies of the same key, or copies of
copies, can be assigned to different users: all of them will have access
to the same data in the data store.

User Keys and Restricted Data Keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The data user may combine a data key, which is known to the system
owner, with a secret **user key**, which is known only to the user.
Without knowing the key, even system owner, who had generated and
distributed the data key to the data user, may not attack the user data.

In order to retain access to user data, the system owner may generate
and distribute a **restricted data key** – such key cannot be combined
with a user key.

High Availability Clusters
--------------------------

An individual server handles all mutating operations on the computer
where it is running. It also exchanges the mutated data over the network
with other CryptoMove servers that belong to its *cluster*.

Each CryptoMove server maintains a High Availability cluster of
CryptoMove servers running on other hosts, to which it distributes the
file parts. In turn, each server from the cluster maintains its cluster,
to where it forwards the file parts stored in its store. This way
CryptoMove creates a web of interconnected clusters. The owner of the
CryptoMove server controls cluster membership.

.. image:: /images/high_availability_clusters.png

Each server keeps track of the operational state of its cluster servers.
Only if the cluster member state is *online*, it decides to forward the
file parts to that member server. When a member server fails or goes
offline, the controlling server may choose to procure an alternative
server from the cluster. This way, CryptoMove maintains a local
high-availability quorum of online servers.

Processes
---------

At runtime, CryptoMove daemons work as one of the three groups of
processes: a *client*, *server* or *server* *manager*. Each CryptoMove
client is an interactive program that communicates with the user via a
terminal through CCLI or with a user process via standard input/output
channel through CCLI or CCAPI. The client engine also communicates with
the cryptomove server, which runs on the same computer, in order to move
user data files in and out of the CryptoMove store.

The following figure illustrates a computer that runs both CryptoMove
client and server:

.. image:: /images/processes.png


Once the server starts up, it can operate continuously without the need
of a client. CryptoMove servers with and without clients may operate on
any number of computers.

OS Directories and Files
------------------------

During its operations, CryptoMove servers and clients access its data
store which is located under the UNIX directory set in the environmental
variable $CRYPTOMOVE_PATH. They use and create the following directories
and files where they access and store user and system data:

+-----------------------------------+-----------------------------------+
| *User Directories*                | User directories are regular OS   |
|                                   | file system directories where     |
|                                   | users keep their files outside of |
|                                   | the CryptoMove data store.        |
|                                   | CryptoMove uses them for the      |
|                                   | following purposes:               |
|                                   |                                   |
|                                   | -  to retrieve a file for saving  |
|                                   |    in CryptoMove,                 |
|                                   |                                   |
|                                   | -  to restore a file previously   |
|                                   |    saved in the CryptoMove,       |
|                                   |                                   |
|                                   | -  to keep and maintain an        |
|                                   |    encrypted *CryptoMove* *log*   |
|                                   |    *file* that contains the       |
|                                   |    history of protective          |
|                                   |    operations for an individual   |
|                                   |    user file,                     |
|                                   |                                   |
|                                   | -  to keep optional soft links    |
|                                   |    indicating which files have    |
|                                   |    been saved in CryptoMove       |
|                                   |    store.                         |
+===================================+===================================+
| *CryptoMove Data Store*           | Server native directories are     |
|                                   | located under the top cryptomove  |
|                                   | directory                         |
|                                   |                                   |
|                                   |    .crptmv_top_dir                |
|                                   |                                   |
|                                   | They contain all files CryptoMove |
|                                   | server uses in its operations,    |
|                                   | including data and permutation    |
|                                   | parts, pulse, hash and trace      |
|                                   | files. Each server                |
|                                   | (sub)-directory has a name of one |
|                                   | digit from 0 to 7. Subdirectories |
|                                   | go no deeper than eight levels.   |
|                                   |                                   |
|                                   | CryptoMove server distributes     |
|                                   | data parts and related files      |
|                                   | between server native directories |
|                                   | in a random fashion. Because the  |
|                                   | names of parts and related files  |
|                                   | are cryptographically generated,  |
|                                   | there is no danger that different |
|                                   | files end up with the same name   |
|                                   | in the same subdirectory. By      |
|                                   | randomly placing the data parts,  |
|                                   | CryptoMove server assures that    |
|                                   | they do not concentrate in any    |
|                                   | particular subdirectory. As a     |
|                                   | result, UNIX utilities (e.g. ls)  |
|                                   | and CryptoMove server can access  |
|                                   | data efficiently within           |
|                                   | CryptoMove data store.            |
|                                   |                                   |
|                                   | The system owner can create any   |
|                                   | number of these subdirectories in |
|                                   | advance on any volume of its data |
|                                   | store. The system owner can       |
|                                   | utilize any data store management |
|                                   | facility to                       |
|                                   | add/remove/backup/restore         |
|                                   | additional data volumes as long   |
|                                   | as they are mounted on the        |
|                                   | CryptoMove native store           |
|                                   | (sub)-directories.                |
|                                   |                                   |
|                                   | If not present, CryptoMove server |
|                                   | generates these sub-directories   |
|                                   | automatically.                    |
+-----------------------------------+-----------------------------------+

Files in the CryptoMove data store have cryptographically generated
names that are not correlated – it is impossible to associate different
CryptoMove files with each other or with the original name of the user
file (except for files with extension .cln).

Logging
-------

Each time a user saves data in the data store, CryptoMove client program
records the parameters of the save operation in the log file; it updates
that record when the client deletes the saved file. The client program
uses data from the log records when retrieving and deleting the saved
data.

*The recorded information is not secret* – the attacker cannot use it to
recover the saved data without having the data key (and key) in her
possession. Still, the client program encrypts all log files by the
corresponding data keys.

To allow user operations in case of a log file is lost or destroyed, the
client program saves each record from the log file inside the CryptoMove
data store. CryptoMove client program also allows the user to retrieve
from the store the saved log records in order to reconstruct the missing
log file on the client computer.

When the user saves data into the CryptoMove namespace, its log is also
duplicated in the data store.

CCLI Reference
==============

Preliminaries
-------------

This sub-section explains general information needed to install and
uninstall CryptoMove software, and to run all CryptoMove commands from
CryptoMove Command Line Interface (CCLI). It is followed by the detailed
descriptions of all CryptoMove commands.

System Owner Privileges
~~~~~~~~~~~~~~~~~~~~~~~

It is best to make sure that you, as a UNIX user who installs CryptoMove
software, have sudo privileges, and an entry in /etc/sudoers that allows
for executing privileged commands without being asked for a password,
like this (say, for user ubuntu) [13]_:

ubuntu ALL=NOPASSWD:ALL

CryptoMove Dependencies
~~~~~~~~~~~~~~~~~~~~~~~

Hello Packages
^^^^^^^^^^^^^^

CryptoMove software is written in Hello programming language, therefore
it requires a small Hello runtime engine for it to work. Hello
distribution is installed automatically during CryptoMove installation.

External Packages
^^^^^^^^^^^^^^^^^

In addition, CryptoMove software uses a number of supporting packages,
such as a rest api library, command line editing library, cryptographic
library, Python, etc. All such supporting packages, and other packages
on which these packages depend upon, are installed (recursively)
automatically during CryptoMove installation.

For successful download of dependent packages, you shall assure your
server’s connectivity with the points that contain those packages.

If needed, update your download environment with command:

   sudo apt-get update

CryptoMove System Requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The current version of CryptoMove software runs on servers supported by
64-bit CPU of Intel Architecture that operate LINUX OS from UBUNTU
distribution v16.04 or higher. The minimum hardware requirements are as
follows:

+-------------------------+--------+
| Number of CPUs or cores | 8      |
+=========================+========+
| Size of virtual memory  | 16 GB  |
+-------------------------+--------+
| Disk size               | 100 GB |
+-------------------------+--------+

Shared Memory Parameters
~~~~~~~~~~~~~~~~~~~~~~~~

During installation, CryptoMove installation script automatically resets
LINUX shared memory parameters as follows:

-  the max total shared memory segment size kernel.shmall is set to
   20971520 4KB pages,

-  the maximum size of individual shared memory segment kernel.shmmax is
   set to 8589934592 bytes,

-  the maximum count in the socket backlog connection
   net.ipv4.tcp_max_syn_backlog is set to 65536,

-  the maximum number of socket connections net.core.somaxconn is set to
   65535.

Single-Host and Multi-Host Installations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove software can run on a single host or on multiple hosts. You
must install CrypoMove software on a host before running that software.
For a single-host system, you shall install CruptoMove software on that
host.

If you intend to run CryptoMove software in a cluster of multiple hosts,
you shall install it on every host separately, each time repeating the
steps described in the following sub-section 4.1.6. After completing the
installation on every host, you may choose to edit files .hello_in and
.hello_hosts on every host by listing the names of hosts to which a
given host can send data (.hello_hosts), and from which the host can
accept data (.hello_in).

CryptoMove Installation
~~~~~~~~~~~~~~~~~~~~~~~

You install CryptoMove software on a Linux system from the CryptoMove
distribution zip file, following the subsequent steps, as a UNIX user
with sudo privileges. Note – these steps result in the CryptoMove
working only with the data store on a mounted disk, not in any cloud
data store:

1. Choose a directory on a mounted disk where Cryptomove data store is
   going to be located, then make that directory current using shell cd
   command. We refer to the full path of that directory as *cryptomove
   path*.

..

   Note: you may install CryptoMove software in multiple cryptomove
   paths. Each cryptomove path contains a separate cryptomove data
   store. However, you may run only one instance from one cryptomove
   path of the installed CryptoMove software at a time.

2. If you have installed CryptoMove software in a cryptomove path
   before, then, before installing it again, you must uninstall the
   previously installed CryptoMove software from that path with the
   command:

   ./uninstall.sh

3. CryptoMove software is supplied inside a zipped archive named:

..

   buildid.cryptomove.hello.zip

   where buildid is a 7-digits hexadecimal identifier of the CryptoMove
   software build, for example 8ab2431. Copy that file into the current
   cryptomove path and unpack all files from the CryptoMove software
   archive with the command

   unzip buildid.cryptomove.hello.zip

Answer A to the unzip’s prompt. If your UBUNTU system does not have
utility unzip, install it as

sudo apt-get install unzip

On RedHat or Centos LINUX install it as

sudo yum install unzip

After that, rerun he unzip command.

4. Install CryptoMove software with the command:

   ./install.sh

   Answer y to all questions. When the following prompt appears on your
   screen:

   enter host names from Cryptomove cluster, one name per line (finish
   by CTRL/d):

   enter the names of hosts this host shall exchange data from its data
   store (CTRL/D if your host is the only host in the cluster).

5. During the installation, your shell startup file .bashrc is modified
   by setting a number of environmental variables needed to run
   CryptoMove software. When you login to your LINUX account later,
   after CryptoMove software is installed, those variables will be
   automatically set up because of running .bashrc. One of the most
   important variables is CRYPTOMOVE_PATH – it defines the cryptomove
   path where CryptoMove software is installed.

6. Initialize CryptoMove data store directory structure with the
   command:

   ./.crvr

   While executing this command, you may observe a message, which looks
   as follows and which you should ignore:

   ./.crvr: line 18: 27588 Killed sudo /usr/bin/hee -Q

7. Set up environmental variables needed to run CryptoMove server with
   the command:

   source .crvrunrc

8. At this point, cryptomove installation is complete. Check that the
   environmental variable CRYPTOMOVE_PATH is set to <`pwd`/datastore>:

..

   export \| grep CRYPTOMOVE_PATH

9. | Remove default BOX configuration file for your data to be securely
     saved on a mounted disk under $CRYPTOMOVE_PATH (otherwise your data
     may try to go to a BOX cloud test account):
   | rm -f datastore/.conf/.data_dir.conf

   This step completes CryptoMove installation on a given host. From now
   on, you can perform normal cryptomove operations using cryptomove
   management program crv. For example, get crv help as follows:

..

   crv h

Testing CryptoMove Software after Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Installed CryptoMove software contains a number of executable shell
scripts \*.sh and CryptoMove client command files \*.icrv that together
constitute a unit test of the CryptoMove software. It is best to run
this test right after installation when your data store does not yet
contain any valuable data because the test will destroy any data in the
data store.

**WARNING: The unittest destroys your current CryptoMove data store and
recreates it anew a number of times during its runtime. Therefore,
before running the unittest, you shall extract from that data store any
valuable data saved there previously. The extraction shall happen from
all hosts of the cluster to which this host belongs, as the parts of
data saved from other cluster hosts may have landed on the current host
during their travel within the cluster.**

**NOTE**: CryptoMove unittest does not use CryptoMove client crv to
execute its command files \*.icrv. Instead, it uses CryptoMove client
program icrv written with the help of the C language CryptoMove API. Any
manual execution of the \*.icrv files by crv will result in error. See
sub-section 5.3 for description of icrv.

You can run CryptoMove unittest right after CryptoMove software
installation, or any time later following these steps:

1. Make sure the host where you intend to run the unittest is up.

2. Shut down CryptoMove servers on all other hosts from the cluster to
   which that host belongs. Failure to do this will result in your test
   failing.

3. Login to the host where you intend to run the test.

4. Make cryptomove path your current directory and establish proper
   CryptoMove environment by running these commands:

..

   cd $CRYPTOMOVE_PATH

   source .crvrunrc

5. Start up the unittest with this shell command:

..

   ./ct.sh

6. While running, the unittest produces a number of \*.out files that
   contain traces of the test execution. At the end of the run, they are
   compared to the reference trace files \*.good. The difference between
   the output files and the reference files shall be empty if the test
   succeeds. You can view the summary of all differences after the test
   finishes by viewing file ct.sh.out. You may also observe this file
   being populated while the test is running with the following command:

..

   tail –f ct.sh.out

7. You may also observe the trace of all CryptoMove server actions
   executed by the unittest using this command:

..

   sudo tail –f /tmp/*.trc

Unittest Results
^^^^^^^^^^^^^^^^

The results of a successful unittest run shall look similar to this
printout:

BEGIN ############################ ct0.icrv -- startup/shutdown
cryptomove server several times

END ############################ ct0.icrv -- startup/shutdown cryptomove
server several times

BEGIN ############################ ct1.icrv -- startup cryptomove server

END ############################ ct1.icrv -- startup cryptomove server

BEGIN ############################ ct2.icrv -- revoke data key

END ############################ ct2.icrv -- revoke data key

BEGIN ############################ ct3.icrv -- generate data key

END ############################ ct3.icrv -- generate data key

BEGIN ############################ ct4.icrv -- copy data key

END ############################ ct4.icrv -- copy data key

BEGIN ############################ ct5.icrv -- generate another data key

END ############################ ct5.icrv -- generate another data key

BEGIN ############################ ct6.icrv -- distribute data key

END ############################ ct6.icrv -- distribute data key

BEGIN ############################ ct7.icrv -- simple data save restore

END ############################ ct7.icrv -- simple data save restore

BEGIN ############################ ct8.icrv -- simple line data save
restore

END ############################ ct8.icrv -- simple line data save
restore

BEGIN ############################ ct9.icrv -- accessing non-existing
files

END ############################ ct9.icrv -- accessing non-existing
files

BEGIN ############################ ct10.icrv -- accessing non-existing
data

END ############################ ct10.icrv -- accessing non-existing
data

BEGIN ############################ ct11.icrv -- put data, kill server,
restart server, get and del files and data

END ############################ ct11.icrv -- put data, kill server,
restart server, get and del files and data

BEGIN ############################ ct12.icrv -- create, populate, work
inside, and delete directories

END ############################ ct12.icrv -- create, populate, work
inside, and delete directories

BEGIN ############################ ct14.icrv -- work with client
directories

END ############################ ct14.icrv -- work with client
directories

BEGIN ############################ ct15.icrv -- work with client
directories

END ############################ ct15.icrv -- work with client
directories

BEGIN ############################ ct16.icrv -- various management
commands with valid and invalid server keys

END ############################ ct16.icrv -- various management
commands with valid and invalid server keys

Reinitializing CryptoMove Data Store
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once CryptoMove software is installed in a cryptomove path, you may
start up and shut down CryptoMove from that cryptomove path any number
of times. You can also erase the CryptoMove data store from that path,
and then recreate a new data store, in the same cryptomove path, without
reinstalling CryptoMove software. In order to erase your current
CryptoMove data store from a cryptomove path and initialize a brand new
one, run these commands:

   ./.crvr

   rm -f datastore/.conf/.data_dir.conf

Uninstalling CryptoMove
~~~~~~~~~~~~~~~~~~~~~~~

To uninstall CryptoMove software after it has been installed, run this
command:

./uninstall.sh

Starting up CryptoMove Server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You shall issue command source .crvrunrc before you start CryptoMove
server the first time after login to your LINUX host. This command sets
proper environmental variables for the server to start up, including
CRYPTOMOVE_PATH. Once this command completes, you may start up
CryptoMove server any number of times, without the need to rerun this
command again.

CryptoMove and Hello Runtime Engine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove software is written in the Hello programming language. It
consists of several Hello packages with the main package:

+-----------------------------------+-----------------------------------+
| CryptoMove_World                  | This package is the CryptoMove    |
|                                   | software proper, responsible for  |
|                                   | all CryptoMove data protection    |
|                                   | operations by cryptomove manager, |
|                                   | client and server daemons.        |
+-----------------------------------+-----------------------------------+

When CryptoMove manager crv is about to execute a command from its
command line, it launches Hello runtime engine /usr/bin/hee. That
program loads all CryptoMove packages and then transfers control to
CryptoMove_World.

CryptoMove Hello Runtime Parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As a Hello application, CryptoMove daemons – both clients and servers --
set up some Hello parameters before its startup as listed in the table
below. Other parameters acquire default values by the Hello runtime
engine. You may override any parameters by setting respective shell
variables.

+-------------+-------------+-------------+-------------+-------------+
| **Hello     | **Respectiv | **Default   | **Purpose** |             |
| Parameter** | e           | value**     |             |             |
|             | env         |             |             |             |
|             | variable**  |             |             |             |
+=============+=============+=============+=============+=============+
|             |             | **Server    | **Client    |             |
|             |             | (memory)**  | (disk)**    |             |
+-------------+-------------+-------------+-------------+-------------+
| *Size of    | HELLO_HOSTM | 32MB        | 2MB         | Main host   |
| the main    | AIN_SIZE    |             |             | partition   |
| host        |             |             |             | keeps Hello |
| partition*  |             |             |             | runtime     |
|             |             |             |             | system      |
|             |             |             |             | data.       |
+-------------+-------------+-------------+-------------+-------------+
| *Size of    | HELLO_ENGMA | 32MB        | 2MB         | Main engine |
| the main    | IN_SIZE     |             |             | partition   |
| engine      |             |             |             | keeps       |
| partition*  |             |             |             | events,     |
|             |             |             |             | method      |
|             |             |             |             | parameters, |
|             |             |             |             | and other   |
|             |             |             |             | system      |
|             |             |             |             | data.       |
+-------------+-------------+-------------+-------------+-------------+
| *Size of    | HELLO_SECUR | 16MB        | Secure      |             |
| the secure  | E_MEMORY    |             | memory      |             |
| memory*     |             |             | buffer      |             |
|             |             |             | keeps       |             |
|             |             |             | symmetric   |             |
|             |             |             | keys.       |             |
+-------------+-------------+-------------+-------------+-------------+
| *Max thread | HELLO_UNLIM | 100,000     | Threads run |             |
| count*      | ITED_RES    | threads     | CryptoMove  |             |
|             |             |             | package.    |             |
+-------------+-------------+-------------+-------------+-------------+
| *Max time   | HELLO_TIME_ | 120 seconds | Time        |             |
| shift*      | SHIFT       |             | floating    |             |
|             |             |             | interval    |             |
|             |             |             | checks the  |             |
|             |             |             | clocks’     |             |
|             |             |             | difference. |             |
|             |             |             | If the      |             |
|             |             |             | processor   |             |
|             |             |             | clock and   |             |
|             |             |             | the server  |             |
|             |             |             | clock       |             |
|             |             |             | differ more |             |
|             |             |             | than the    |             |
|             |             |             | specified   |             |
|             |             |             | value of    |             |
|             |             |             | the time    |             |
|             |             |             | shift, then |             |
|             |             |             | the server  |             |
|             |             |             | performs    |             |
|             |             |             | all         |             |
|             |             |             | outstanding |             |
|             |             |             | requests    |             |
|             |             |             | without     |             |
|             |             |             | waiting for |             |
|             |             |             | their       |             |
|             |             |             | expiration  |             |
|             |             |             | time        |             |
|             |             |             | interval.   |             |
+-------------+-------------+-------------+-------------+-------------+

Troubleshooting
~~~~~~~~~~~~~~~

There are several steps to take when things go wrong or unexpected with
CryptoMove daemons and their underlying Hello runtime engines:

1. If for whatever reason the running CryptoMove daemons do not go down
   following normal shutdown procedures, you can always forcefully kill
   them knowing their process ids with the UNIX command kill.

2. You may also choose to run command ‘hee –Q’ which not only kills the
   daemons but also reclaims all memory partitions and mapped files used
   by the CryptoMove daemons.

3. Observe the runtime command line parameters for the underlying Hello
   engine when starting CryptoMove server and client after setting to 1
   the shell variable

..

   CRYPTOMOVE_HELLO_ENGINE_STARTUP_LINE_DUMP

4. You can change any of the used Hello runtime parameters, or add new
   ones by setting ALL Hello options and their parameters in the quoted
   argument of option –o on the CryptoMove manager command crv.

5. To allocate all CryptoMove partitions within the mapped files under
   directory dir, use ‘–X dir’ inside that quoted argument.

6. Consult Hello Programming Guide for all Hello runtime parameters.

7. Run ’hee –h’ to print all Hello runtime parameters and their default
   values.

Cryptomove Logging
~~~~~~~~~~~~~~~~~~

Cryptomove server and client issue extensive logging messages in
/var/log/syslog for Ubuntu, or /var/log/messages for RedHat or Centos.
Consult those logs in order to observe Cryptomove system events such as
server startup and shutdown; client connects, execution of client
commands, etc.

CryptoMove Shell Variables
~~~~~~~~~~~~~~~~~~~~~~~~~~

The following UNIX shell variables control CryptoMove server and client
operations:

+-----------------------------------+-----------------------------------+
| **Env variable**                  | **Purpose**                       |
+===================================+===================================+
| CRYPTOMOVE_API_STDERR             | If set, then defines a file to    |
|                                   | redirect CryptoMove API standard  |
|                                   | error.                            |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_API_STDOUT             | If set, then defines a file to    |
|                                   | redirect CryptoMove API standard  |
|                                   | output.                           |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_API_PATH               | Defines directory for CryptoMove  |
|                                   | server to temporarily store the   |
|                                   | saved data to be recovered with   |
|                                   | putr command after a crash.       |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_HELLO_ENGINE_STARTUP_L | If set, allows for client and     |
| INE_DUMP                          | server dumping at startup of the  |
|                                   | full hello command line.          |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_ICRV_LOG               | If set, allows for client         |
|                                   | commands and data to be recorded  |
|                                   | in syslog.                        |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_LOG_PATH               | Defines directory that holds log  |
|                                   | files for saved data.             |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_NO_PTRACE              | If set, then prohibits            |
|                                   | system-wide use of UNIX system    |
|                                   | call ptrace().                    |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_NOALTER                | If set, then does not allow       |
|                                   | redirecting CryptoMove server’s   |
|                                   | standard output and standard      |
|                                   | error into files defined by       |
|                                   | CRYPTOMOVE_STDOUT and             |
|                                   | CRYPTOMOVE_STDERR.                |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_NOAUDIT                | If set then prohibits CryptoMove  |
|                                   | server output of auditing records |
|                                   | into syslog.                      |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_OOM                    | If set, then allows for OS to     |
|                                   | kill CryptoMove server when it    |
|                                   | begins to use large amounts of    |
|                                   | memory.                           |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_PATH                   | Defines directory that holds      |
|                                   | CryptoMove data store.            |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_POWER                  | If set to a numeric value from 1  |
|                                   | to 100, then it determines        |
|                                   | default cryptomove power at       |
|                                   | startup (which is 20 by default). |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_SLOW                   | If set to 1, then it slows down   |
|                                   | the high frequencies of parts’    |
|                                   | movement and pulse transmission.  |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_STARTUP_DELAY          | If set to an integer value T,     |
|                                   | delays execution of CryptoMove    |
|                                   | client for T seconds after its    |
|                                   | invocation.                       |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_STDERR                 | If set, defines a non-default     |
|                                   | file for CryptoMove server        |
|                                   | standard error.                   |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_STDOUT                 | If set, defines a non-default     |
|                                   | file for CryptoMove server        |
|                                   | standard output.                  |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_TRACE                  | If set to value verbose X Y …,    |
|                                   | defines CryptoMove server trace   |
|                                   | options X Y … to use at its       |
|                                   | startup.                          |
+-----------------------------------+-----------------------------------+
| CRYPTOMOVE_USER                   | Name of the user which becomes    |
|                                   | owner of the data restored onto   |
|                                   | local host. If not set then that  |
|                                   | name is root.                     |
+-----------------------------------+-----------------------------------+

CryptoMove Hello Package Path
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Hello runtime engine loads its packages from either the current
directory or the directory designated by environmental variable
HELLO_PACKSRC_PATH. Point that variable to the directory where both
CryptoMove_World and Wide_World packages reside, or else CryptoMove
manager will not start up:

think@centos-middle:~/cryptomove.prepare.1.0.6/bla$ export
HELLO_PACKSRC_PATH=/home/think/cryptomove.prepare.1.0.6

CryptoMove Server Trace
^^^^^^^^^^^^^^^^^^^^^^^

CryptoMove server traces some events at its startup, and later during
execution. The trace messages are printed on the server’s standard
output and standard error channels. By default, these channels are
directed to files /tmp/cryptomove_out.trc and /tmp/cryptomove_err.trc.
You can alter the default channels before starting up CryptoMove server
as follows:

-  Define environmental variable CRYPTOMOVE_NOALTER: both channels will
   remain unchanged. For example, when starting from an interactive
   shell they end up directed to the terminal.

-  Set one or both environmental variables CRYPTOMOVE_STDOUT or
   CRYPTOMOVE_STDERR to a respective file name: corresponding channels
   will be redirected to the files with the specified names.

CryptoMove Client Trace
^^^^^^^^^^^^^^^^^^^^^^^

CryptoMove client traces execution of its commands in a trace file,
which file name is specified in the shell variable
CRYPTOMOVE_CLIENT_STDOUT. If it is not defined, client commands are not
traced in any file, only on the client’s standard output. Each line
related to a command that saves data starts with the prefix ‘put:’;
lines related to commands that restore or delete data start with ‘get:’.

Entering Keys
~~~~~~~~~~~~~

Many CryptoMove manager commands require you to enter keys. For example,
to create a data key you shall enter a system key. CryptoMove manager
asks all keys one after another at the very beginning using a special
prompt. Upon the entry, CryptoMove saves the keys in secured memory.
After all keys are entered, they are extracted from the secure memory
and checked according to the semantics of a particular CryptoMove
command, as explained in the following sub-sections. CryptoMove always
erases the key from the secure memory after its extraction. When you
enter the key(s), CryptoMove suppresses the echo on the terminal. Below
is the example of entering a key, which fails verification:

   think@centos-middle:~/cryptomove.prepare.1.0.6$ ./crv gd 8 3

   (30775) Enter secret key #1 out of 1:

   decryption failure

   cryptomove server is about to stop

   cryptomove server stopped (code=-1)

   think@centos-middle:~/cryptomove.prepare.1.0.6$

Entering gpg-Encrypted Keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~

You may choose to encrypt a system key with the LINUX utility gpg into a
local file. For that, you will have to supply a gpg passphrase during
the encryption. After that, when invoking CryptoMove client, you may
type in the full path name of the file with the encrypted system key
instead of the key: CryptoMove server then asks you to enter the
passphrase. After that, it invokes the gpg utility with the supplied
file name and passphrase. If gpg successfully decrypts the file with the
passphrase, then CryptoMove server will use the resulting system key as
if it had been entered on its prompt instead of the gpg file name:

   think@centos-middle:~/cryptomove.prepare.1.0.6$ ./crv os

   (4498) Enter secret key #1 out of 1:

   CRYPTOMOVE v1.0.6.46 (alpha) COPYRIGHT (C) CRYPTOMOVE, Inc.
   2015-2017.

   enter gpg passphrase:

   gpg: AES encrypted data

   gpg: encrypted with 1 passphrase

   gpg: AES encrypted data

   gpg: encrypted with 1 passphrase

   starting cryptomove system owner client

   (4498)crv>

CryptoMove Manager Commands
---------------------------

You interact with CryptoMove servers and perform auxiliary operations
using CryptoMove manager program crv. Upon startup, crv analyzes its
command line and launches Hello runtime with the Hello package
CryptoMove_World. That package further loads the Hello High Availability
package Wide_World. After that, crv quits while CryptoMove_World
performs one of the following three actions:

-  Manages keys,

-  Starts up or shuts down CryptoMove server,

-  Starts up CryptoMove client.

Below is the summary of the CryptoMove manager commands – you obtain it
by running crv while supplying help or h command on its command line:

   think@centos-middle:~/cryptomove.prepare.1.0.6$ crv h

   CRYPTOMOVE v1.0.6 (alpha)

   --------------------------------------------------------------------

   CRYPTOMOVE MANAGER HELP

   -----------------------

   In this help text:

   lower case -- abbreviations or keywords

   UPPER CASE -- PARAMETERS

   --------------------------------------------------------------------

   \**HELP COMMANDS*\*

   { h \| help }

   { le \| legend }

   { v \| version }

   --------------------------------------------------------------------

   \*********CRYPTOMOVE MANAGER COMMANDS********\*

   \**KEY MANAGEMENT*\*

   { gs \| generate system key } {KEY_LENGTH \| SYSTEM_KEY} [KEY_ID]

   { rs \| revoke system key } {KEY_LENGTH \| SYSTEM_KEY}

   { gd \| generate data key } {KEY_LENGTH COUNT \| DATA_KEY}

   { gr \| generate restricted key } {KEY_LENGTH COUNT \| DATA_KEY}

   { ed \| erase data key }

   { vd \| verify data key }

   { rd \| revoke data key } {KEY_LENGTH \| DATA_KEY}

   { co \| copy data key } {KEY_LENGTH \| DATA_KEY}

   { csk \| copy system key } {KEY_LENGTH \| SYSTEM_KEY} [KEY_ID]

   { dd \| distribute data key }

   { gk \| generate user key } KEY_LENGTH

   { gf \| generate fingerprint }

   --------------------------------------------------------------------

   \**SERVER MANAGEMENT*\*

   { ss \| server start }

   { se \| server edge }

   { st \| server stop }

   --------------------------------------------------------------------

   \**CLIENT MANAGEMENT*\*

   [ @cryptomove_command_file ] { cs \| client start }

   [ @cryptomove_command_file ] { os \| owner start }

   --------------------------------------------------------------------

   \**For details about all commands type 'le' or 'legend

   --------------------------------------------------------------------

   think@centos-middle:~/cryptomove.prepare.1.0.6$

The following sub-sections explain each of these commands. You can get
the detailed online explanation of all CryptoMove manager commands by
typing

   think@centos-middle:~/cryptomove.prepare.1.0.6$ crv legend

Version Dump
~~~~~~~~~~~~

   { v \| version }

This command dumps installed CryptoMove version.

Example:

think@centos-middle:~/cryptomove.prepare.1.0.6$ crv v

CRYPTOMOVE v1.0.6 (alpha)

think@centos-middle:~/cryptomove.prepare.1.0.6$

Key Management Commands
~~~~~~~~~~~~~~~~~~~~~~~

Setting up CRYPTOMOVE_PATH
^^^^^^^^^^^^^^^^^^^^^^^^^^

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running most of the CryptoMove
manager commands.

Command gs – generate system key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { gs \| generate system key } {LENGTH \| KEY} [KEY_ID]

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. When
LENGTH is specified, this command cryptographically generates a unique
system key of LENGTH bytes long and stores its hash in the data store.
When KEY is specified, it must be provided in hexadecimal format (as an
even number of hexadecimal ASCII digits) – the hash of that key is
stored in the CryptoMove data store. When LENGTH = 0, you enter the key
value on the prompt printed by crv.

Once the hash of the system key is saved in the data store, the user
must supply it for each subsequent CryptoMove manager command.
CryptoMove checks the hash of the supplied system key against the one on
record by comparing its hash with the hash saved in the data store.
CryptoMove does not execute the command if the key check fails.
CryptoMove prints on standard output the generated key in hex format.

The allowed LENGTH is between 8 and 8192 bytes (except when it is set to
0). The longer the key length, the stronger is the key.

Optional KEY_ID identifies the system user who owns the generated system
key. Up to 15 copies of a system key can be created for 15 different
owners, thus allowing multiple system owners to have owner access to the
system.

Example:

   hellouser@RoyalPenguin1:~/cryptomove.prepare.1.0.6$ crv gs 12

   system key generated -- <02c36ad7bbdf47d20a62818d>

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 1(min=1/max=1) hosts

   1.
   [ONLINE]<ROYALPENGUIN1><>[monitored]<ROYALPENGUIN1><12357:127.0.1.1><45A4CC9FFF4347168DD8762673E830BC>

   #### no parts to restart

   cryptomove server pid=21826 started...

   cryptomove server stopping...

   cryptomove server is about to stop .143.143.143.143.

   cryptomove server stopped (code=0)

   hellouser@RoyalPenguin1~/cryptomove.prepare.1.0.6$

Command rs – revoke system key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { rs \| revoke system key } {LENGTH \| KEY}

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. When
LENGTH is specified, this command cryptographically generates a unique
system key of LENGTH bytes long and stores its hash in the data store.
When KEY is specified, it must be provided in hexadecimal format (as an
even number of hexadecimal ASCII digits) – the hash of that key is
stored in the CryptoMove data store. When LENGTH = 0, you enter the key
value on the prompt printed by crv. The generated or supplied system key
replaces the system key that existed prior to this command.

Once the existing system key is revoked, it cannot be used in any
operation that requires a system key. Instead, the user must supply the
newly set system key for each subsequent CryptoMove manager command.

The allowed LENGTH is between 8 and 8192 bytes. The longer the key
length, the stronger is the key.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$ crv rs 12

   (13676) Enter secret key #1 out of 1:

   system key generated -- <68cdf699733149cc3c40cb2d>

   think@centos-middle: ~/cryptomove.prepare.1.0.6/bla/blo/blu$

Command gd – generate data key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { gd \| generate data key } {LENGTH COUNT \| KEY}

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. When
LENGTH is specified, this command cryptographically generates COUNT
unique data keys each LENGTH bytes long. When a KEY is specified, it
must be set in hexadecimal format (as an even number of hexadecimal
ASCII digits) – CryptoMove uses that key as the new data key. When
LENGTH = 0, you enter the key value on the prompt printed by crv.
CryptoMove saves the hash of the new data key in the data store.

CryptoMove uses data key when saving and restoring data, planning the
active data protection measures, encrypting the cleartext and decrypting
the ciphertext.

You shall start up CryptoMove server, as described in sub-section 4.2.3,
before running gd command.

Each generated data key is printed on the screen in hex format.

The allowed LENGTH is between 8 and 8192 bytes. The longer the key
length, the stronger is the key. COUNT must be a positive number.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$ crv gd 8
   1

   (13736) Enter secret key #1 out of 1:

   wrong key

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$ crv gd 8
   1

   (13946) Enter secret key #1 out of 1:

   Begin generating 1 data key...

   Generating data key #1 out of 1 -- <83347fe952466b0f>

   End generating data keys

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$

Default system data key SYSTEM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As a system owner, you may generate the *default system data key*. Any
system owner can use that key to save and restore data, after supplying
the literal “SYSTEM” in place of a data key in the respective save or
restore types of commands. To generate the default system data key,
enter command

gd SYSTEM

Command gr – generate restricted key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { gr \| generate restricted key } {LENGTH COUNT \| KEY}

This command works exactly like command gd – generate data key, except
the data key generated may not be used later when combined with a user
key.

You shall start up CryptoMove server before running gr command.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$ crv gr 12
   1

   (13946) Enter secret key #1 out of 1:

   Begin generating 1 restricted key...

   Generating restricted key #1 out of 1 -- < b7ed64be58762e08087f5155>

   End generating restricted keys

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$

Command ed – erase data key [14]_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { ed \| erase data key }

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command erases a data key. It prompts for two keys:

-  The first key must be the system key.

-  The second key must be the data key that shall be erased.

After a data key is erased, it cannot be used in subsequent CryptoMove
data operations. If any data encrypted with this key remains in
CryptoMove store, then there is no way to delete or retrieve it from the
store – it is lost there forever.

You shall start up CryptoMove server, as described in sub-section 4.2.3,
before running ed command.

Examples:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$ cd blu

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$ crv ed

   (15259) Enter secret key #1 out of 2:

   (15259) Enter secret key #2 out of 2:

   finished erasing key

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$

Command vd – verify data key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { vd \| verify data key }

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command checks the validity of the data key. The command prompts for two
keys:

-  The first key must be the system key.

-  The second key must be the data key that shall be verified.

..

   You shall start up CryptoMove server before running vd command.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo/blu$ cd ..

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$ crv vd

   (15501) Enter secret key #1 out of 2:

   (15501) Enter secret key #2 out of 2:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$

Command rd – revoke data key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { rd \| revoke data key } {LENGTH \| KEY}

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command revokes, i.e. erases, an existing data key and generates a new
data key instead. After that, the user cannot utilize revoked data key
in any data operations. However, the same or different user can use the
newly generated data key to restore, check, or delete the data saved
with the revoked data key. The command prompts for the key to be
revoked.

When LENGTH is specified, this command cryptographically generates
unique data key LENGTH bytes long that replaces the existing data key.
When a KEY is specified, it must be set in hexadecimal format (as an
even number of hexadecimal ASCII digits) – CryptoMove uses that value as
the new data key. When LENGTH = 0, you enter the key value on the prompt
printed by crv. CryptoMove saves the hash of the new data key in the
data store.

CryptoMove first prompts for the system key, then for the data key to be
revoked. The generated key is printed on the screen in hex format. The
allowed LENGTH is between 8 and 8192 bytes. The longer the key length,
the stronger is the key.

You shall start up CryptoMove server, as described in sub-section 4.2.3,
before running rd command.

Example:

   hellouser@RoyalPenguin1:~/hem$ crv rd 12

   (31769) Enter secret key #1 out of 2:

   (31769) Enter secret key #2 out of 2:

   1. starting data key revocation...

   2. generating replacement key...

   Begin generating 1 restricted key...

   Generating restricted key #1 out of 1 -- <f7b9a2de9af16ef6fc392e2a>

   End generating restricted key(s)

   3. saving revoked hash...

   4. erasing revoked key...

   finished erasing key

   data key revocation completed

   hellouser@RoyalPenguin1:~/hem$

Command co – copy data key
^^^^^^^^^^^^^^^^^^^^^^^^^^

   { co \| copy data key } {LENGTH \| KEY}

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command creates a copy of an existing data key by generating a different
restricted data key that can be used interchangeably with the existing
data key. From that point, the user can utilize the copy data key in any
data operations where the existing data key is allowed. The command
prompts for the key to be copied.

When LENGTH is specified, this command cryptographically generates
unique data key LENGTH bytes long, which becomes a copy of the existing
data key. When a KEY is specified, it must be set in hexadecimal format
(as an even number of hexadecimal ASCII digits) – CryptoMove uses that
value as a copy of the existing data key. When LENGTH = 0, you enter the
key value on the prompt printed by crv. CryptoMove saves the hash of the
new data key in the data store.

CryptoMove first prompts for the system key, then for the data key to be
copied. The generated key is printed on the screen in hex format. The
allowed LENGTH is between 8 and 8192 bytes. The longer the key length,
the stronger is the key. Both original and copy key control access to
CryptoMove data store from the same client host.

You shall start up CryptoMove server, as described in sub-section 4.2.3,
before running cd command.

Example:

   hellouser@RoyalPenguin1:~/hem$ crv co 12

   (7853) Enter secret key #1 out of 2:

   (7853) Enter secret key #2 out of 2:

   1. starting data key copy...

   2. generating new key...

   Begin generating 1 restricted key...

   Generating restricted key #1 out of 1 -- <1a8a00bf50f78617ad142145>

   End generating restricted key(s)

   3. saving existing hash...

   data key copy completed

   hellouser@RoyalPenguin1:~/hem$

Command csk – copy system key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { csk \| copy system key } {LENGTH \| KEY} [KEY_ID]

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command creates a copy of an existing data key by generating a different
restricted data key that can be used interchangeably with the existing
data key. From that point, the user can utilize the copy data key in any
data operations where the existing data key is allowed. The command
prompts for the key to be copied.

When LENGTH is specified, this command cryptographically generates
unique data key LENGTH bytes long, which becomes a copy of the existing
data key. When a KEY is specified, it must be set in hexadecimal format
(as an even number of hexadecimal ASCII digits) – CryptoMove uses that
value as a copy of the existing data key. When LENGTH = 0, you enter the
key value on the prompt printed by crv. CryptoMove saves the hash of the
new data key in the data store.

CryptoMove first prompts for the system key, then for the data key to be
copied. The generated key is printed on the screen in hex format. The
allowed LENGTH is between 8 and 8192 bytes. The longer the key length,
the stronger is the key. Both original and copy key control access to
CryptoMove data store from the same client host.

Optional KEY_ID identifies the system user who owns the generated system
key. Up to 15 copies of a system key can be created for 15 different
owners, thus allowing multiple system owners to have owner access to the
system.

You shall start up CryptoMove server, as described in sub-section 4.2.3,
before running cd command.

Example:

   hellouser@RoyalPenguin1:~/hem$ crv csk 12 key1

   (7853) Enter secret key #1 out of 1:

   system key copied -- <02ef7226e182a96784953fcb>

   hellouser@RoyalPenguin1:~/hem$

Command dd – distribute data key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { dd \| distribute data key }

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command distributes an existing data key from one client host by
generating a new restricted data key on another client host so that both
client hosts can access the same data in CryptoMove data store. The
command prompts for the key to be distributed.

CryptoMove first prompts for the system key, then for the data key of a
remote host that is going to be distributed. The generated key is
printed on the screen in hex format – its value is the same as the
original key.

If you run this command on the same host that issued the original key,
which was not a restricted key, the command makes it a restricted key.

You shall start up CryptoMove server, as described in sub-section 4.2.3,
before running cd command.

Example:

   hellouser@RoyalPenguin1:~/hem$ crv dd

   (22324) Enter secret key #1 out of 2:

   (22324) Enter secret key #2 out of 2:

   1. starting data key distribution...

   2. generating new key...

   Begin generating 1 restricted key...

   Generating restricted key #1 out of 1 -- <bf477c7a056f91f29d0420d2>

   End generating restricted key(s)

   data key distribution completed

   hellouser@RoyalPenguin1:~/hem$

Converting Unrestricted Key to Restricted Key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When dd command duplicates a key, the duplicate becomes a restricted
key. One can use this command to convert an unrestricted data key to a
restricted data key by running it on the host that has generated
unrestricted data key.

Command gk – generate user key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { gk \| generate user key } LENGTH

This command generates a cryptographically strong sequence of characters
of LENGTH-bytes long. The command does not ask for or check any keys.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6$ crv gk 16

   key generated -- <29ed324c392cf841894a0047d97233a7>

   think@centos-middle:~/cryptomove.prepare.1.0.6$

Command gf – generate fingerprint
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { gf \| generate fingerprint }

This command generates a unique 16 byte ID and saves it in file
$CRYPTOMOVE_PATH/.fingerprint and also prints it on standard output. If
that file already exists, it prints its content on standard output. The
generated uuid fingerprint can be useful to uniquely identify CryptoMove
server.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6$ crv gf

   2c390ada-0702-11e7-bd81-778dbc832b8d

   think@centos-middle:~/cryptomove.prepare.1.0.6$

CryptoMove Server Startup and Shutdown
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store prior to running either startup or
shutdown command.

Command ss – start server
^^^^^^^^^^^^^^^^^^^^^^^^^

   { ss \| server start }

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command starts CryptoMove server. It prompts for the system key. Upon
obtaining the system key, CryptoMove matches it with the system shadow
file – if no match found the server is not started.

Only a single instance of CryptoMove server can start on a given
computer.

Upon startup, CryptoMove server releases its controlling terminal and
runs as a daemon. It then prints on the screen information about the
current state of the cluster it operates on.

Example:

   think@centos-middle:~$ crv ss

   hee: HELLO ENGINE V1.0.6 (alpha) COPYRIGHT (C) CRYPTOMOVE 2011-2016.

   (22058) Enter secret key #1 out of 1:

   think@centos-middle:~$

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 1(min=1/max=1) hosts

   1.
   [ONLINE]<CENTOS-MIDDLE>[monitored]<CENTOS-MIDDLE><12357:192.168.1.20><1B201A633CE149F1B9D1AC2286C0F876>

   #### no parts to restart

   cryptomove server pid=22062 started...

Command se – start edge server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   { se \| server edge }

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command starts CryptoMove server in *edge* mode. It prompts for the
system key. Upon obtaining the system key, CryptoMove matches it with
the system shadow file – if no match found the server is not started.

The edge server does not notify other servers about its state changes.
Therefore, they never see the edge server in an online state. You should
start an edge server on a host that is not a part of the CryptoMove data
store. The edge server is able to accept the client commands to save
data from the local host, and to restore data onto the local host.
However, it can never participate in the movement of the data parts
saved in the store from other hosts. You may determine if the running
server is in edge mode by typing command default on the client – it
displays ‘edge tue’ if the server is indeed an edge server.

Only a single instance of CryptoMove server can start on a given
computer.

Upon startup, CryptoMove server releases its controlling terminal and
runs as a daemon. It then prints on the screen information about the
current state of the cluster it operates on.

Example:

   think@centos-middle:~$ crv se

   hee: HELLO ENGINE V1.0.6 (alpha) COPYRIGHT (C) CRYPTOMOVE 2011-2016.

   (22058) Enter secret key #1 out of 1:

   think@centos-middle:~$

   #### cluster <DIRECT_CRYPTOMOVE_CLUSTER> 1(min=1/max=1) hosts

   1.
   [ONLINE]<CENTOS-MIDDLE>[monitored]<CENTOS-MIDDLE><12357:192.168.1.20><1B201A633CE149F1B9D1AC2286C0F876>

   #### no parts to restart

   cryptomove server pid=22062 started...

Command st – stop server
^^^^^^^^^^^^^^^^^^^^^^^^

   { st \| server stop }

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command stops CryptoMove server. It prompts for the key. Upon obtaining
the system key, CryptoMove matches it with the system shadow file – if
no match found the server does not stop.

The server stops after suppressing all ongoing data operations, which
include, among others, mutation, movement, data save and retrieval.
During the shutdown process, it dumps on the screen information about
the shutdown progress – the number of requests performed in the last few
seconds. Only after a period of absolute passivity, it finally shuts
down. After CryptoMove shuts down, the Hello engine that used to run
CryptoMove shuts down as well [15]_.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$ crv st

   (16905) Enter secret key #1 out of 1:

   cryptomove server stopping...

   cryptomove server is about to stop .0.0.0.0.

   cryptomove server stopped (code=0)

   1:16295:2:NOTICE:G5:5::HELLO ENGINE pid=16295 finished.

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$

CryptoMove Client Startup
~~~~~~~~~~~~~~~~~~~~~~~~~

Data User Client Startup – command cs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   [ @cryptomove_command_file ] { cs \| client start }

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command starts CryptoMove client on behalf of a data user. CryptoMove
server must be running before client startup. You can run this command
from any directory.

Once started, CryptoMove client can perform various CCLI commands such
as data save, restore, delete, and check. If cryptomove_command_file is
specified, then the client immediately executes CCLI commands from the
file and then exits. If the command file is not specified, the client
works interactively with the user who can enter CCLI commands onto
client prompt one at a time. Data user client rejects commands allowed
for execution only by system owners.

The user may need to enter data key(s) during execution of some CCLI
commands, following the prompts from the client.

The user quits client either by typing CTRL/D or by typing command quit
or q.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$ crv cs

   starting cryptomove data user client

   (17629)crv> quit

   cryptomove client finished

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$

System Owner Client Startup – command os
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   [ @cryptomove_command_file ] { os \| owner start }

You must have shell variable CRYPTOMOVE_PATH set to the directory
containing CryptoMove data store before running this command. This
command starts CryptoMove client on behalf of a system owner. It prompts
for the system key. This command starts up CryptoMove client only if the
key verification succeeds. CryptoMove server must be running before
client startup.

Once started, CryptoMove client can perform various CCLI commands such
as data save, restore, delete, and check. If cryptomove_command_file is
specified, then the client immediately executes CCLI commands from the
file and then exits. If the command file is not specified, the client
works interactively with the user who can enter CCLI commands onto the
prompt one at a time. System owner client performs commands allowed for
execution by either data users or system owners.

The user quits client either by typing CTRL/D or by typing command quit
or q.

Example:

   think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$ crv os

   (17934) Enter secret key #1 out of 1:

   starting cryptomove system owner client

   (17934)crv>quit

   cryptomove client finished

   `think@centos-middle:~/cryptomove.prepare.1.0.6/bla/blo$ <mailto:think@centos-middle:~/cryptomove.prepare.1.0.5/bla/blo$>`__

Client Interrupt
^^^^^^^^^^^^^^^^

You can interrupt CryptoMove client at any time by sending UNIX signal
SIGINT (e.g. when typing CTRL/C on the keyboard). If the client is on
the prompt or executing a command typed on the prompt, the client quits
upon receiving this signal. When the client is executing a command file,
the signal interrupts the command file execution. After that, the client
returns back to the interactive prompt.

CryptoMove Client Commands
--------------------------

After starting a CryptoMove client, you execute CryptoMove CCLI commands
one after another interactively, typing them after a prompt from the
cryptomove client. You may also create a CryptoMove *command file*
placing one command per line, and then execute all commands in batch
mode by typing the filename preceded with the indirection character ‘@’.
CryptoMove client runs commands from command file one after another,
without issuing an interactive prompt.

In all subsequent client commands’ descriptions:

-  keywords are printed in lower case, parameter values in upper case,

-  section 4.3.4.5 explains all parameters and their values,

-  section 4.3.8.1 describes the use of keys,

-  section 4.3.8.3 describes the use of file names.

Command Line History
~~~~~~~~~~~~~~~~~~~~

CryptoMove client keeps the history of all non-repetitive commands
issued since its last startup; you may navigate inside the historic list
of commands by using keyboard’s up- and down-arrow keys. While entering
a command, you may use left- and right-arrows, as well as backspace and
delete keys to edit the command line [16]_.

Blanks, Comments, and Spaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove skips any blank line – the one that consists only of spaces
or tabs. Similarly, it skips and does not interpret or execute any
command line that begins with the pound sign ‘#’ – it treats such line
as a comment. If a file name includes a space, the whole file name must
be included within double quotes, like ”my file”.

List of All Client Commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can obtain a list of all CryptoMove client commands by typing help
or h at the client prompt:

   (17386)crv>help

You can get a list of all commands with their full online explanations
by typing command legend or le:

   (15985)crv>legend

Command version or v dumps current CryptoMove version:

   (15985)crv>version

The following is an abbreviated summary of all CryptoMove client
commands:

+-----------------------+-----------------------+-----------------------+
| **Command**           | **Purpose**           | **Sub-section**       |
+=======================+=======================+=======================+
| **Help Commands**     |                       |                       |
+-----------------------+-----------------------+-----------------------+
| h \| help             | Dumps list of all     | 4.3.2                 |
|                       | commands.             |                       |
+-----------------------+-----------------------+-----------------------+
| le \| legend          | Dumps detailed        | 4.3.2                 |
|                       | explanations of all   |                       |
|                       | commands.             |                       |
+-----------------------+-----------------------+-----------------------+
| v \| version          | Dumps current         | 4.3.2                 |
|                       | CryptoMove version.   |                       |
+-----------------------+-----------------------+-----------------------+
| **Client Operational  |                       |                       |
| Commands**            |                       |                       |
+-----------------------+-----------------------+-----------------------+
| q \| quit \| ctrl/d   | Quits CryptoMove      | 4.3.4.1               |
|                       | client.               |                       |
+-----------------------+-----------------------+-----------------------+
| p \| pids             | Dumps UNIX process    | 4.3.4.2               |
|                       | ids of CryptoMove     |                       |
|                       | daemons.              |                       |
+-----------------------+-----------------------+-----------------------+
| !shell_command        | Executes a shell      | 4.3.4.1               |
|                       | command and waits for |                       |
|                       | its completion.       |                       |
+-----------------------+-----------------------+-----------------------+
| @cryptomove_command_f | Executes CryptoMove   | 4.3.4.3               |
| ile                   | command file and      |                       |
|                       | waits for its         |                       |
|                       | completion.           |                       |
+-----------------------+-----------------------+-----------------------+
| @@cryptomove_command_ | Concurrently executes | 4.3.4.4               |
| file                  | CryptoMove command    |                       |
|                       | file (does not wait   |                       |
|                       | for its completion).  |                       |
+-----------------------+-----------------------+-----------------------+
| w \| wait             | Waits for completion  | 4.3.4.5               |
|                       | of all concurrently   |                       |
|                       | running CryptoMove    |                       |
|                       | command files.        |                       |
+-----------------------+-----------------------+-----------------------+
| **Client Parameters   |                       |                       |
| and Modes**           |                       |                       |
+-----------------------+-----------------------+-----------------------+
| auditname             | Sets client audit     | 4.3.5.4               |
|                       | name, which tags      |                       |
|                       | every client’s audit  |                       |
|                       | message in syslog.    |                       |
+-----------------------+-----------------------+-----------------------+
| d \| default          | Dumps or sets current | 4.3.4.5               |
|                       | default parameters,   |                       |
|                       | modes and options.    |                       |
+-----------------------+-----------------------+-----------------------+
| t \| trust            | Dumps or sets current | 4.3.5.4               |
|                       | trust mode.           |                       |
+-----------------------+-----------------------+-----------------------+
| k \| key              | Dumps or sets current | 4.3.5.4               |
|                       | key mode.             |                       |
+-----------------------+-----------------------+-----------------------+
| l \| link             | Dumps or sets current | 4.3.5.4               |
|                       | link mode.            |                       |
+-----------------------+-----------------------+-----------------------+
| f \| force            | Dumps or sets current | 4.3.5.4               |
|                       | force mode.           |                       |
+-----------------------+-----------------------+-----------------------+
| ctmo                  | Dumps or sets current | 4.3.5.4               |
|                       | client timeout.       |                       |
+-----------------------+-----------------------+-----------------------+
| client verbose        | Switches client to    | 4.3.5.4               |
|                       | verbose mode.         |                       |
+-----------------------+-----------------------+-----------------------+
| client silent         | Switches client to    | 4.3.5.4               |
|                       | silent mode.          |                       |
+-----------------------+-----------------------+-----------------------+
| **Data Handling       |                       |                       |
| Commands**            |                       |                       |
+-----------------------+-----------------------+-----------------------+
| mov                   | Moves one file to     | 4.3.8.4               |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| dmov                  | Moves all files from  | 4.3.8.4               |
|                       | a client directory to |                       |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| dmovr                 | Recursively moves all | 4.3.8.4               |
|                       | files from a client   |                       |
|                       | directory to          |                       |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| mmov                  | Moves several files   | 4.3.8.4               |
|                       | to CryptoMove store.  |                       |
+-----------------------+-----------------------+-----------------------+
| put                   | Copies one file to    | 4.3.8.4               |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| dput                  | Copies all files from | 4.3.8.4               |
|                       | a client directory to |                       |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| dputr                 | Recursively copies    | 4.3.8.4               |
|                       | all files from a      |                       |
|                       | client directory to   |                       |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| mput                  | Copies several files  | 4.3.8.4               |
|                       | to CryptoMove store.  |                       |
+-----------------------+-----------------------+-----------------------+
| mod                   | Modifies the file     | 4.3.8.4               |
|                       | previously saved in   |                       |
|                       | the data store.       |                       |
+-----------------------+-----------------------+-----------------------+
| com                   | Commits the last      | 4.3.8.4               |
|                       | series of             |                       |
|                       | midifications from    |                       |
|                       | mod commands.         |                       |
+-----------------------+-----------------------+-----------------------+
| purge                 | Purges log records    | 4.3.8.4               |
|                       | for deleted versions  |                       |
|                       | of a single file.     |                       |
+-----------------------+-----------------------+-----------------------+
| dpurge                | Purges log record for | 4.3.8.4               |
|                       | deleted versions of   |                       |
|                       | all files saved from  |                       |
|                       | a directory.          |                       |
+-----------------------+-----------------------+-----------------------+
| log                   | Dumps operational     | 4.3.8.9               |
|                       | log.                  |                       |
+-----------------------+-----------------------+-----------------------+
| rlog                  | Recovers operational  | 4.3.8.10              |
|                       | log of the saved      |                       |
|                       | file.                 |                       |
+-----------------------+-----------------------+-----------------------+
| dlog                  | Dumps operational log | 4.3.8.10              |
|                       | for all files saved   |                       |
|                       | from a directory.     |                       |
+-----------------------+-----------------------+-----------------------+
| get                   | Restores one file     | 4.3.8.5               |
|                       | from CryptoMove       |                       |
|                       | store.                |                       |
+-----------------------+-----------------------+-----------------------+
| mget                  | Restores multiple     | 4.3.8.5               |
|                       | files from CryptoMove |                       |
|                       | store.                |                       |
+-----------------------+-----------------------+-----------------------+
| dget                  | Restores all files    | 4.3.8.5               |
|                       | previously saved from |                       |
|                       | a directory.          |                       |
+-----------------------+-----------------------+-----------------------+
| ddir                  | Lists file names of   | 4.3.8.5               |
|                       | all files saved       |                       |
|                       | previously from a     |                       |
|                       | directory.            |                       |
+-----------------------+-----------------------+-----------------------+
| chk                   | Checks one file in    | 4.3.8.6               |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| dchk                  | Checks all files      | 4.3.8.6               |
|                       | previously saved from |                       |
|                       | a directory.          |                       |
+-----------------------+-----------------------+-----------------------+
| mchk                  | Checks multiple files | 4.3.8.6               |
|                       | in CryptoMove store.  |                       |
+-----------------------+-----------------------+-----------------------+
| del                   | Deletes one file from | 4.3.8.7               |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| ddel                  | Deletes all files     | 4.3.8.7               |
|                       | previously saved from |                       |
|                       | a directory.          |                       |
+-----------------------+-----------------------+-----------------------+
| mdel                  | Deletes multiple      | 4.3.8.7               |
|                       | files from CryptoMove |                       |
|                       | store.                |                       |
+-----------------------+-----------------------+-----------------------+
| clr                   | Clears one file from  | 4.3.8.8               |
|                       | client native store.  |                       |
+-----------------------+-----------------------+-----------------------+
| mclr                  | Clears multiple files | 4.3.8.8               |
|                       | from client native    |                       |
|                       | store.                |                       |
+-----------------------+-----------------------+-----------------------+
| putl                  | Copies data from      | 4.3.8.4               |
|                       | standard input to     |                       |
|                       | CryptoMove store.     |                       |
+-----------------------+-----------------------+-----------------------+
| putr                  | Restores data         | 4.3.8.4               |
|                       | preserved from putl   |                       |
|                       | command.              |                       |
+-----------------------+-----------------------+-----------------------+
| modl                  | Modifies previously   | 4.3.8.4               |
|                       | saved data from       |                       |
|                       | standard input.       |                       |
+-----------------------+-----------------------+-----------------------+
| coml                  | Commits the last      | 4.3.8.4               |
|                       | series of             |                       |
|                       | midifications from    |                       |
|                       | modl commands.        |                       |
+-----------------------+-----------------------+-----------------------+
| purgel                | Purges log records    | 4.3.8.4               |
|                       | for deleted data.     |                       |
+-----------------------+-----------------------+-----------------------+
| logl                  | Dumps operational     | 4.3.8.9               |
|                       | log.                  |                       |
+-----------------------+-----------------------+-----------------------+
| rlogl                 | Recovers operational  | 4.3.8.10              |
|                       | log of the saved      |                       |
|                       | data.                 |                       |
+-----------------------+-----------------------+-----------------------+
| getl                  | Restores data from    | 4.3.8.5               |
|                       | CryptoMove store onto |                       |
|                       | standard output.      |                       |
+-----------------------+-----------------------+-----------------------+
| chkl                  | Checks data saved     | 4.3.8.6               |
|                       | from standard input   |                       |
|                       | in CryptoMove store.  |                       |
+-----------------------+-----------------------+-----------------------+
| dell                  | Deletes data saved    | 4.3.8.7               |
|                       | from standard input   |                       |
|                       | from the store.       |                       |
+-----------------------+-----------------------+-----------------------+
| clrl                  | Clears data saved     | 4.3.8.8               |
|                       | from standard input   |                       |
|                       | from client native    |                       |
|                       | store.                |                       |
+-----------------------+-----------------------+-----------------------+
| **Data Redundancy     |                       |                       |
| Group Management      |                       |                       |
| Commands**            |                       |                       |
+-----------------------+-----------------------+-----------------------+
| putg                  | Creates redundancy    | 4.3.9.1               |
|                       | group                 |                       |
+-----------------------+-----------------------+-----------------------+
| chkg                  | Checks redundancy     | 4.3.9.2               |
|                       | group                 |                       |
+-----------------------+-----------------------+-----------------------+
| delg                  | Deletes redundancy    | 4.3.9.3               |
|                       | group                 |                       |
+-----------------------+-----------------------+-----------------------+
| logg                  | Dumps log of          | 4.3.9.4               |
|                       | redundancy group      |                       |
+-----------------------+-----------------------+-----------------------+
| rlogg                 | Recovers log record   | 4.3.9.5               |
|                       | of redundancy group   |                       |
+-----------------------+-----------------------+-----------------------+
| **CryptoMove          |                       |                       |
| Directory Management  |                       |                       |
| Commands**            |                       |                       |
+-----------------------+-----------------------+-----------------------+
| mkdir                 | Creates CryptoMove    | 4.3.10.1              |
|                       | directory             |                       |
+-----------------------+-----------------------+-----------------------+
| rmdir                 | Removes CryptoMove    | 4.3.10.2              |
|                       | directory             |                       |
+-----------------------+-----------------------+-----------------------+
| cd                    | Sets current          | 4.3.10.3              |
|                       | CryptoMove directory  |                       |
+-----------------------+-----------------------+-----------------------+
| pwd                   | Displays current      | 4.3.10.4              |
|                       | CryptoMove directory  |                       |
+-----------------------+-----------------------+-----------------------+
| uncd                  | Unsets current        | 4.3.10.5              |
|                       | CryptoMove directory  |                       |
+-----------------------+-----------------------+-----------------------+
| dir                   | Displays one          | 4.3.10.6              |
|                       | CryptoMove directory  |                       |
+-----------------------+-----------------------+-----------------------+
| dirr                  | Displays recursively  | 4.3.10.7              |
|                       | a hierarchy of        |                       |
|                       | CryptoMove            |                       |
|                       | directories           |                       |
+-----------------------+-----------------------+-----------------------+
| **Server Parameters   |                       |                       |
| and Modes**           |                       |                       |
+-----------------------+-----------------------+-----------------------+
| audit                 | Toggles server        | 4.3.7                 |
|                       | auditing on or off.   |                       |
+-----------------------+-----------------------+-----------------------+
| power                 | Changes server        | 4.3.7                 |
|                       | movement power        |                       |
+-----------------------+-----------------------+-----------------------+
| rshell                | Allows for execution  | 4.3.7                 |
|                       | remote shell          |                       |
|                       | commands.             |                       |
+-----------------------+-----------------------+-----------------------+
| stmo                  | Sets server-to-server | 4.3.7                 |
|                       | timeout.              |                       |
+-----------------------+-----------------------+-----------------------+
| watch                 | Sets interval between | 4.3.7                 |
|                       | watching acts for     |                       |
|                       | unauthorized access   |                       |
|                       | to stored data.       |                       |
+-----------------------+-----------------------+-----------------------+
| watchpids             | Sets a list of pids   | 4.3.7                 |
|                       | of processes that can |                       |
|                       | touch CryptoMove      |                       |
|                       | store                 |                       |
+-----------------------+-----------------------+-----------------------+
| timeshift             | Sets allowed time     | 4.3.7                 |
|                       | shift between         |                       |
|                       | CryptoMove and OS     |                       |
|                       | clock                 |                       |
+-----------------------+-----------------------+-----------------------+
| edge                  | Indicates if the      | 4.3.7                 |
|                       | server works in edge  |                       |
|                       | mode                  |                       |
+-----------------------+-----------------------+-----------------------+
| state                 | Shows server state    | 4.3.7                 |
+-----------------------+-----------------------+-----------------------+
| sync                  | Write all buffered    | 4.3.7                 |
|                       | data on disk          |                       |
+-----------------------+-----------------------+-----------------------+
| **High Availability   |                       |                       |
| Cluster Management    |                       |                       |
| Commands**            |                       |                       |
+-----------------------+-----------------------+-----------------------+
| cluster               | Executes one of       | 4.3.12.3              |
|                       | cluster management    |                       |
|                       | commands.             |                       |
+-----------------------+-----------------------+-----------------------+

Client Operational Commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Command q – quit
^^^^^^^^^^^^^^^^

   q \| quit \| CTRL/D

This command quits CryptoMove client.

Command p – pids
^^^^^^^^^^^^^^^^

   p \| pids

This command prints UNIX process identifiers for client and server
CryptoMove processes.

Command ! – Execute Shell Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   !shell_command

This command executes a shell command by the client. The client must be
a system owner.

Command !! – Execute Remote Shell Command
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   !!shell_command

This command executes a shell command by the local server, and by all
remote servers connected to the local server, if they operate in the
‘rshell on’ mode. The client must be a system owner.

Command @ -- Run Command File
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   @cryptomove_command_file [-u LOOPCOUNT]

Runs a sequence of cryptomove commands from the specified file and waits
for its completion. If LOOPCOUNT is set, then repeats running command
file LOOPCOUNT times. The command file retains standard input, output
and error channels of the cryptomove client that invoked the file.

Command @@ -- Run Command File Concurrently
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   @@concurent_cryptomove_command_file

Runs a sequence of cryptomove commands from the specified file but does
not wait for its completion. The command file retains standard input,
output and error channels of the cryptomove client that invoked the
file.

Command w – wait
^^^^^^^^^^^^^^^^

   w \| wait

This command waits for completion of all concurrently executing command
files launched from the same client.

Command wakeup
^^^^^^^^^^^^^^

   wakeup

This command reschedules the movement of all data parts on the local
host and recursively on all online hosts accessible from the local host.
It is available only for clients running in system owner mode (started
as ‘crv os’).

Command d – Default Parameter Values
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Many client commands use default parameters. Also, CryptoMove client
program can operate in certain modes that affect the execution of all
commands. You can examine or change current default parameters and modes
with the following commands:

{d \| default} [PARAMETER [VALUE]]

Without PARAMETER and VALUE, this command prints current default values
of all parameters and modes:

   (17386)crv>d

   DEFAULT CLIENT PARAMETERS:

   --------------------------

   IN COMMANDS TO SAVE DATA:

   -------------------------

   ARMSLENGTH 4

   CLEANCOMMIT 0

   COPYCOUNT 1

   DATADEPTH 4

   DIRFULL 0

   ENTROPY 0

   EXPIRATION 525600000

   ENCRYPTCLIENT 1

   ENCRYPTSERVER 0

   FILLGROUP 100

   FORCEPURGE 0

   GROUPNAME --------

   HMACCLIENT 0

   LOGSAVE 1

   MODNAME --------

   MODOFFSET 0

   MODPOSITION 1

   MOVEPACE 2

   NOWAIT 0

   NUMBER all

   PERMUTATION 1

   PULSEBEAT 2

   PURGELOG 0

   RECORDCOUNT 0

   SAVELOCAL 0

   SCATTERLENGTH 0

   SIZE 8192

   SPLITCOUNT 1

   SRCID 0

   SRCTYPE LINUX

   STRENGTH 0

   TARGETSTORE -NOTARGET-

   TESTCOUNT 0

   TRACE 0

   USER root

   USRID -ALL-

   UUIDONLY 0

   XORPART 0

   --------------------

   IN COMMANDS TO RESTORE DATA:

   ----------------------------

   LOGRECORD --------

   TIMEOUT 30

   VERSION last

   ----------------------

   CLIENT OPTIONS AND TOGGLED MODES:

   ---------------------------------

   AUDITNAME NONE

   CLIENT VERBOSE

   CTMO 360

   ECHO off

   FORCE off

   KEY off

   LINK off

   TRUST off

   ----------------------

   SERVER OPTIONS:

   ---------------

   APICOUNT 0

   AUDIT on

   BREACH off

   BOXRETRY 0

   COUNT 800

   EDGE false

   EXPIRE off

   KILL off

   MEML 4194304

   POWER 20

   PROGRESS off

   RSHELL off

   STATE [ONLINE]

   STMO 360

   TIMESCALE 1

   TIMESHIFT 120

   TRNC 0

   VERBOSE <SILENT>

   WATCH 0

   WATCHPIDS 0

   ----------------------

   (17386)crv>

When only PARAMETER is set, CryptoMove prints the current default value
of that parameter:

   (17386)crv>d copycount

   COPYCOUNT 4

   (17386)crv>

With both PARAMETER and VALUE set, client sets PARAMETER to VALUE [17]_
and prints VALUE on the screen [18]_:

   (17386)crv>d copycount 8

   COPYCOUNT 8

   (17386)crv> d version last

   VERSION last

   (17386)crv> d number all

   VERSION all

   (17386)crv>

Data Version Specification
^^^^^^^^^^^^^^^^^^^^^^^^^^

Every time a client saves the same data, a new version of the data is
created in the CryptoMove data store. You may specify the version on
which to perform many client commands in two ways. If a positive number
N denotes a version, then that number N is the version of the data. If
it is a negative number -N, then the version is the one recorded in the
log record numbered N from the end of the log.

Parameters in Commands to Save Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following table explains CryptoMove client parameters and flags that
set those parameters in the data saving commands mov, mmov, put, mput,
and putl. Default indicates the value set to the parameter upon client
startup. Interval shows allowed values.

+-------------+-------------+-------------+-------------+-------------+
| **Parameter | **Flag**    | **Default** | **Interval* | **Explanati |
| **          |             |             | *           | on**        |
+=============+=============+=============+=============+=============+
| ARMSLENGTH  | -a          | 4           | 1:32        | This number |
|             |             |             |             | determines  |
|             |             |             |             | how many    |
|             |             |             |             | different   |
|             |             |             |             | hosts the   |
|             |             |             |             | part may    |
|             |             |             |             | travel      |
|             |             |             |             | through.    |
|             |             |             |             | When set to |
|             |             |             |             | 1, it stays |
|             |             |             |             | on the host |
|             |             |             |             | where the   |
|             |             |             |             | client      |
|             |             |             |             | runs, even  |
|             |             |             |             | if its      |
|             |             |             |             | local       |
|             |             |             |             | server is   |
|             |             |             |             | an edge     |
|             |             |             |             | server.     |
+-------------+-------------+-------------+-------------+-------------+
| CLEANCOMMIT | -cl         | 0           | 0:1         | Determines  |
|             |             |             |             | if the last |
|             |             |             |             | committed   |
|             |             |             |             | and         |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             | versions    |
|             |             |             |             | must be     |
|             |             |             |             | deleted     |
|             |             |             |             | from the    |
|             |             |             |             | data store  |
|             |             |             |             | after a     |
|             |             |             |             | successful  |
|             |             |             |             | commit      |
|             |             |             |             | operation.  |
|             |             |             |             | If set to 0 |
|             |             |             |             | then the    |
|             |             |             |             | last        |
|             |             |             |             | committed   |
|             |             |             |             | and         |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             | versions    |
|             |             |             |             | are left    |
|             |             |             |             | intact; if  |
|             |             |             |             | set to 1    |
|             |             |             |             | those       |
|             |             |             |             | versions    |
|             |             |             |             | are         |
|             |             |             |             | deleted.    |
+-------------+-------------+-------------+-------------+-------------+
| COPYCOUNT   | -c          | 1           | 1:256       | The number  |
|             |             |             |             | of copies   |
|             |             |             |             | of the      |
|             |             |             |             | saved data. |
+-------------+-------------+-------------+-------------+-------------+
| DATADEPTH   | -d          | 4           | 0:32        | The maximum |
|             |             |             |             | depth of    |
|             |             |             |             | the path    |
|             |             |             |             | each file   |
|             |             |             |             | part        |
|             |             |             |             | traverses   |
|             |             |             |             | the         |
|             |             |             |             | network.    |
|             |             |             |             | Each time a |
|             |             |             |             | part moves, |
|             |             |             |             | i.e.        |
|             |             |             |             | changes its |
|             |             |             |             | name,       |
|             |             |             |             | padding,    |
|             |             |             |             | encryption, |
|             |             |             |             | and         |
|             |             |             |             | possibly    |
|             |             |             |             | location,   |
|             |             |             |             | it adds to  |
|             |             |             |             | the current |
|             |             |             |             | depth of    |
|             |             |             |             | its path.   |
|             |             |             |             | All changes |
|             |             |             |             | are random, |
|             |             |             |             | so it is    |
|             |             |             |             | not         |
|             |             |             |             | possible to |
|             |             |             |             | predict the |
|             |             |             |             | sequence of |
|             |             |             |             | future      |
|             |             |             |             | actions and |
|             |             |             |             | the hosts   |
|             |             |             |             | the part    |
|             |             |             |             | will        |
|             |             |             |             | traverse.   |
|             |             |             |             |             |
|             |             |             |             | When the    |
|             |             |             |             | depth       |
|             |             |             |             | reaches the |
|             |             |             |             | maximum     |
|             |             |             |             | value, the  |
|             |             |             |             | part        |
|             |             |             |             | reverses    |
|             |             |             |             | its         |
|             |             |             |             | movement    |
|             |             |             |             | and         |
|             |             |             |             | backtracks  |
|             |             |             |             | to the base |
|             |             |             |             | host that   |
|             |             |             |             | originated  |
|             |             |             |             | the         |
|             |             |             |             | movement.   |
|             |             |             |             | On the way  |
|             |             |             |             | back, it    |
|             |             |             |             | continues   |
|             |             |             |             | to change   |
|             |             |             |             | its name,   |
|             |             |             |             | size,       |
|             |             |             |             | padding,    |
|             |             |             |             | and         |
|             |             |             |             | encryption. |
|             |             |             |             | When it     |
|             |             |             |             | reaches the |
|             |             |             |             | base host,  |
|             |             |             |             | it resumes  |
|             |             |             |             | its forward |
|             |             |             |             | movement,   |
|             |             |             |             | but with a  |
|             |             |             |             | different   |
|             |             |             |             | random      |
|             |             |             |             | path.       |
|             |             |             |             |             |
|             |             |             |             | When        |
|             |             |             |             | DATADEPTH   |
|             |             |             |             | is set to   |
|             |             |             |             | 0, it does  |
|             |             |             |             | not move.   |
+-------------+-------------+-------------+-------------+-------------+
| DIRFULL     | -f          | 0           | -1:1        | Sets the no |
|             |             |             |             | date (-1),  |
|             |             |             |             | short (0)   |
|             |             |             |             | or full (1) |
|             |             |             |             | format when |
|             |             |             |             | displaying  |
|             |             |             |             | CryptoMove  |
|             |             |             |             | directories |
|             |             |             |             | .           |
|             |             |             |             | In the      |
|             |             |             |             | short       |
|             |             |             |             | format,     |
|             |             |             |             | directory   |
|             |             |             |             | element     |
|             |             |             |             | shows the   |
|             |             |             |             | following   |
|             |             |             |             | data:       |
|             |             |             |             |             |
|             |             |             |             | -  Date and |
|             |             |             |             |    time     |
|             |             |             |             |    when the |
|             |             |             |             |    element  |
|             |             |             |             |    has been |
|             |             |             |             |    saved in |
|             |             |             |             |    the data |
|             |             |             |             |    store.   |
|             |             |             |             |             |
|             |             |             |             | -  Size of  |
|             |             |             |             |    the      |
|             |             |             |             |    original |
|             |             |             |             |    clear    |
|             |             |             |             |    text     |
|             |             |             |             |    data in  |
|             |             |             |             |    bytes.   |
|             |             |             |             |             |
|             |             |             |             | -  Letter   |
|             |             |             |             |    ‘d’ that |
|             |             |             |             |    indicate |
|             |             |             |             | s           |
|             |             |             |             |    that the |
|             |             |             |             |    element  |
|             |             |             |             |    is a     |
|             |             |             |             |    sub-dire |
|             |             |             |             | ctory,      |
|             |             |             |             |    or the   |
|             |             |             |             |    version  |
|             |             |             |             |    number   |
|             |             |             |             |    of the   |
|             |             |             |             |    saved    |
|             |             |             |             |    data if  |
|             |             |             |             |    the      |
|             |             |             |             |    element  |
|             |             |             |             |    describe |
|             |             |             |             | s           |
|             |             |             |             |    saved    |
|             |             |             |             |    data.    |
|             |             |             |             |             |
|             |             |             |             | -  Name of  |
|             |             |             |             |    the      |
|             |             |             |             |    original |
|             |             |             |             |    saved    |
|             |             |             |             |    data, or |
|             |             |             |             |    name of  |
|             |             |             |             |    the      |
|             |             |             |             |    director |
|             |             |             |             | y.          |
|             |             |             |             |             |
|             |             |             |             | -  Remote   |
|             |             |             |             |    name, if |
|             |             |             |             |    any, of  |
|             |             |             |             |    the      |
|             |             |             |             |    saved    |
|             |             |             |             |    data or  |
|             |             |             |             |    the full |
|             |             |             |             |    path of  |
|             |             |             |             |    the      |
|             |             |             |             |    director |
|             |             |             |             | y.          |
|             |             |             |             |             |
|             |             |             |             | In the long |
|             |             |             |             | format, the |
|             |             |             |             | short data  |
|             |             |             |             | is          |
|             |             |             |             | augmented   |
|             |             |             |             | with a line |
|             |             |             |             | of the      |
|             |             |             |             | following   |
|             |             |             |             | fields      |
|             |             |             |             | separated   |
|             |             |             |             | by the ‘+’  |
|             |             |             |             | sign:       |
|             |             |             |             |             |
|             |             |             |             | -  MODCMD:  |
|             |             |             |             |    If this  |
|             |             |             |             |    element  |
|             |             |             |             |    describe |
|             |             |             |             | s           |
|             |             |             |             |    modifica |
|             |             |             |             | tion        |
|             |             |             |             |    data     |
|             |             |             |             |    then     |
|             |             |             |             |    this is  |
|             |             |             |             |    the      |
|             |             |             |             |    modifica |
|             |             |             |             | tion        |
|             |             |             |             |    command  |
|             |             |             |             |    used for |
|             |             |             |             |    modifica |
|             |             |             |             | tion;       |
|             |             |             |             |    otherwis |
|             |             |             |             | e,          |
|             |             |             |             |    this is  |
|             |             |             |             |    the same |
|             |             |             |             |    as       |
|             |             |             |             |    SAVECMD. |
|             |             |             |             |             |
|             |             |             |             | -  SIZE:    |
|             |             |             |             |    Size in  |
|             |             |             |             |    bytes of |
|             |             |             |             |    the      |
|             |             |             |             |    original |
|             |             |             |             |    clear-te |
|             |             |             |             | xt          |
|             |             |             |             |    data.    |
|             |             |             |             |             |
|             |             |             |             | -  MODOFFSE |
|             |             |             |             | T:          |
|             |             |             |             |    For      |
|             |             |             |             |    modifica |
|             |             |             |             | tion        |
|             |             |             |             |    command  |
|             |             |             |             |    this is  |
|             |             |             |             |    a        |
|             |             |             |             |    modifica |
|             |             |             |             | tion        |
|             |             |             |             |    offset;  |
|             |             |             |             |    for      |
|             |             |             |             |    other    |
|             |             |             |             |    commands |
|             |             |             |             |    this     |
|             |             |             |             |    value is |
|             |             |             |             |    0.       |
|             |             |             |             |             |
|             |             |             |             | -  MODPOSIT |
|             |             |             |             | ION:        |
|             |             |             |             |    For      |
|             |             |             |             |    modifica |
|             |             |             |             | tion        |
|             |             |             |             |    commands |
|             |             |             |             |    this is  |
|             |             |             |             |    a        |
|             |             |             |             |    modifica |
|             |             |             |             | tion        |
|             |             |             |             |    position |
|             |             |             |             | .           |
|             |             |             |             |    For      |
|             |             |             |             |    other    |
|             |             |             |             |    commands |
|             |             |             |             |    this is  |
|             |             |             |             |    .0.      |
|             |             |             |             |             |
|             |             |             |             | -  SAVECMD: |
|             |             |             |             |    Command  |
|             |             |             |             |    that     |
|             |             |             |             |    saved    |
|             |             |             |             |    this     |
|             |             |             |             |    data.    |
|             |             |             |             |             |
|             |             |             |             | -  SAVETIME |
|             |             |             |             | :           |
|             |             |             |             |    Date and |
|             |             |             |             |    time     |
|             |             |             |             |    when     |
|             |             |             |             |    data was |
|             |             |             |             |    saved.   |
|             |             |             |             |             |
|             |             |             |             | -  SPLITCOU |
|             |             |             |             | NT:         |
|             |             |             |             |    Number   |
|             |             |             |             |    of parts |
|             |             |             |             |    into     |
|             |             |             |             |    each     |
|             |             |             |             |    copy is  |
|             |             |             |             |    broken.  |
|             |             |             |             |             |
|             |             |             |             | -  COPYCOUN |
|             |             |             |             | T:          |
|             |             |             |             |    Number   |
|             |             |             |             |    of       |
|             |             |             |             |    copies   |
|             |             |             |             |    of the   |
|             |             |             |             |    saved    |
|             |             |             |             |    data.    |
|             |             |             |             |             |
|             |             |             |             | -  DATADEPT |
|             |             |             |             | H:          |
|             |             |             |             |    The      |
|             |             |             |             |    depth of |
|             |             |             |             |    the path |
|             |             |             |             |    file     |
|             |             |             |             |    parts    |
|             |             |             |             |    traverse |
|             |             |             |             |    on the   |
|             |             |             |             |    network. |
|             |             |             |             |             |
|             |             |             |             | -  MOVEPACE |
|             |             |             |             | :           |
|             |             |             |             |    Number   |
|             |             |             |             |    of times |
|             |             |             |             |    per day  |
|             |             |             |             |    each     |
|             |             |             |             |    part     |
|             |             |             |             |    moves.   |
|             |             |             |             |             |
|             |             |             |             | -  SCATTERL |
|             |             |             |             | ENGTH:      |
|             |             |             |             |    length   |
|             |             |             |             |    of bits  |
|             |             |             |             |    to       |
|             |             |             |             |    scatter  |
|             |             |             |             |    among    |
|             |             |             |             |    parts.   |
|             |             |             |             |             |
|             |             |             |             | -  ARMSLENG |
|             |             |             |             | TH:         |
|             |             |             |             |    number   |
|             |             |             |             |    of hosts |
|             |             |             |             |    the part |
|             |             |             |             |    is       |
|             |             |             |             |    allowed  |
|             |             |             |             |    to       |
|             |             |             |             |    travel.  |
|             |             |             |             |             |
|             |             |             |             | -  PERMUTAT |
|             |             |             |             | ION:        |
|             |             |             |             |    Allow    |
|             |             |             |             |    (1) or   |
|             |             |             |             |    prohibit |
|             |             |             |             |    (0) data |
|             |             |             |             |    permutat |
|             |             |             |             | ion.        |
|             |             |             |             |             |
|             |             |             |             | -  OSDIRECT |
|             |             |             |             | ORY:        |
|             |             |             |             |    OS       |
|             |             |             |             |    director |
|             |             |             |             | y           |
|             |             |             |             |    that     |
|             |             |             |             |    confines |
|             |             |             |             |    movement |
|             |             |             |             |    the      |
|             |             |             |             |    saved    |
|             |             |             |             |    data.    |
|             |             |             |             |             |
|             |             |             |             | -  HOST:    |
|             |             |             |             |    Host     |
|             |             |             |             |    that     |
|             |             |             |             |    confines |
|             |             |             |             |    movement |
|             |             |             |             |    of the   |
|             |             |             |             |    saved    |
|             |             |             |             |    data.    |
+-------------+-------------+-------------+-------------+-------------+
| ENCRYPTCLIE | -ec         | 1           | 0:1         | This option |
| NT          |             |             |             | toggles     |
|             |             |             |             | data        |
|             |             |             |             | encryption  |
|             |             |             |             | on the      |
|             |             |             |             | client side |
|             |             |             |             | before the  |
|             |             |             |             | data is     |
|             |             |             |             | saved in    |
|             |             |             |             | the data    |
|             |             |             |             | store.      |
+-------------+-------------+-------------+-------------+-------------+
| ENCRYPTSERV | -es         | 0           | 0:1         | This option |
| ER          |             |             |             | toggles     |
|             |             |             |             | data        |
|             |             |             |             | encryption  |
|             |             |             |             | on the      |
|             |             |             |             | server side |
|             |             |             |             | while the   |
|             |             |             |             | data is     |
|             |             |             |             | moving      |
|             |             |             |             | inside the  |
|             |             |             |             | CryptoMove  |
|             |             |             |             | data store. |
+-------------+-------------+-------------+-------------+-------------+
| ENTROPY     | -e          | 0           | 0:8192      | This number |
|             |             |             |             | determines  |
|             |             |             |             | the         |
|             |             |             |             | cumulative  |
|             |             |             |             | entropy of  |
|             |             |             |             | the split   |
|             |             |             |             | data parts. |
|             |             |             |             | Higher      |
|             |             |             |             | entropy     |
|             |             |             |             | indicates a |
|             |             |             |             | higher      |
|             |             |             |             | amount of   |
|             |             |             |             | resources   |
|             |             |             |             | needed to   |
|             |             |             |             | attack the  |
|             |             |             |             | saved data  |
|             |             |             |             | with brute  |
|             |             |             |             | force. If   |
|             |             |             |             | the entropy |
|             |             |             |             | calculated  |
|             |             |             |             | from the    |
|             |             |             |             | command     |
|             |             |             |             | parameters  |
|             |             |             |             | is less     |
|             |             |             |             | than the    |
|             |             |             |             | set value,  |
|             |             |             |             | then the    |
|             |             |             |             | operation   |
|             |             |             |             | does not    |
|             |             |             |             | start.      |
+-------------+-------------+-------------+-------------+-------------+
| FILLGROUP   | -f          | 100         | 0:100       | The         |
|             |             |             |             | percentage  |
|             |             |             |             | of data     |
|             |             |             |             | copies that |
|             |             |             |             | participate |
|             |             |             |             | in data     |
|             |             |             |             | movement    |
|             |             |             |             | within      |
|             |             |             |             | redundancy  |
|             |             |             |             | groups. If  |
|             |             |             |             | set to 0,   |
|             |             |             |             | then all    |
|             |             |             |             | copies      |
|             |             |             |             | travel      |
|             |             |             |             | random      |
|             |             |             |             | paths. When |
|             |             |             |             | set to 100  |
|             |             |             |             | then all    |
|             |             |             |             | copies      |
|             |             |             |             | travel with |
|             |             |             |             | redundancy  |
|             |             |             |             | groups. By  |
|             |             |             |             | default,    |
|             |             |             |             | FILLGROUP   |
|             |             |             |             | is set to   |
|             |             |             |             | 100, which  |
|             |             |             |             | forces all  |
|             |             |             |             | parts to    |
|             |             |             |             | travel      |
|             |             |             |             | within      |
|             |             |             |             | redundancy  |
|             |             |             |             | groups.     |
|             |             |             |             |             |
|             |             |             |             | If          |
|             |             |             |             | GROUPNUMBER |
|             |             |             |             | option is   |
|             |             |             |             | set to 0,   |
|             |             |             |             | then        |
|             |             |             |             | FILLGROUP   |
|             |             |             |             | option is   |
|             |             |             |             | ignored –   |
|             |             |             |             | no copy     |
|             |             |             |             | travels     |
|             |             |             |             | within any  |
|             |             |             |             | redundancy  |
|             |             |             |             | group.      |
+-------------+-------------+-------------+-------------+-------------+
| FORCEPURGE  | -f          | -           | 0:1         | When this   |
|             |             |             |             | option is   |
|             |             |             |             | set to 1,   |
|             |             |             |             | it forces   |
|             |             |             |             | purging     |
|             |             |             |             | undeleted   |
|             |             |             |             | records     |
|             |             |             |             | from the    |
|             |             |             |             | log. After  |
|             |             |             |             | purging     |
|             |             |             |             | those       |
|             |             |             |             | records,    |
|             |             |             |             | the saved   |
|             |             |             |             | data        |
|             |             |             |             | versions    |
|             |             |             |             | that are    |
|             |             |             |             | described   |
|             |             |             |             | by the      |
|             |             |             |             | purged      |
|             |             |             |             | records     |
|             |             |             |             | becomes     |
|             |             |             |             | unavailable |
|             |             |             |             | until       |
|             |             |             |             | further     |
|             |             |             |             | restoration |
|             |             |             |             | of those    |
|             |             |             |             | records     |
|             |             |             |             | from the    |
|             |             |             |             | data store  |
|             |             |             |             | using rlog  |
|             |             |             |             | command. By |
|             |             |             |             | default,    |
|             |             |             |             | this option |
|             |             |             |             | is set 0.   |
+-------------+-------------+-------------+-------------+-------------+
| GROUPNAME   | -g          | -           | -           | This        |
|             |             |             |             | parameter   |
|             |             |             |             | determines  |
|             |             |             |             | the         |
|             |             |             |             | *redundancy |
|             |             |             |             | group       |
|             |             |             |             | name*. When |
|             |             |             |             | GROUPNAME   |
|             |             |             |             | is not      |
|             |             |             |             | defined,    |
|             |             |             |             | the saved   |
|             |             |             |             | data parts  |
|             |             |             |             | travel      |
|             |             |             |             | random      |
|             |             |             |             | paths in    |
|             |             |             |             | the data    |
|             |             |             |             | store. When |
|             |             |             |             | GROUPNAME   |
|             |             |             |             | is set,     |
|             |             |             |             | then the    |
|             |             |             |             | saved data  |
|             |             |             |             | parts from  |
|             |             |             |             | a given     |
|             |             |             |             | data copy   |
|             |             |             |             | travel      |
|             |             |             |             | within the  |
|             |             |             |             | hosts of a  |
|             |             |             |             | certain     |
|             |             |             |             | random      |
|             |             |             |             | partition   |
|             |             |             |             | that        |
|             |             |             |             | belongs to  |
|             |             |             |             | the         |
|             |             |             |             | redundancy  |
|             |             |             |             | group with  |
|             |             |             |             | the         |
|             |             |             |             | specified   |
|             |             |             |             | GROUPNAME.  |
|             |             |             |             |             |
|             |             |             |             | The         |
|             |             |             |             | redundancy  |
|             |             |             |             | group is a  |
|             |             |             |             | set of      |
|             |             |             |             | hosts       |
|             |             |             |             | organized   |
|             |             |             |             | into        |
|             |             |             |             | several     |
|             |             |             |             | partitions  |
|             |             |             |             | that        |
|             |             |             |             | confine the |
|             |             |             |             | movement of |
|             |             |             |             | the parts   |
|             |             |             |             | from the    |
|             |             |             |             | same data   |
|             |             |             |             | copy. The   |
|             |             |             |             | hosts       |
|             |             |             |             | self-organi |
|             |             |             |             | ze          |
|             |             |             |             | into a      |
|             |             |             |             | redundancy  |
|             |             |             |             | group named |
|             |             |             |             | GROUPNAME   |
|             |             |             |             | and further |
|             |             |             |             | into        |
|             |             |             |             | group’s     |
|             |             |             |             | partitions  |
|             |             |             |             | following   |
|             |             |             |             | command     |
|             |             |             |             | putg        |
|             |             |             |             | GROUPNAME.  |
|             |             |             |             |             |
|             |             |             |             | Partitions  |
|             |             |             |             | of the same |
|             |             |             |             | group are   |
|             |             |             |             | maintained  |
|             |             |             |             | in such a   |
|             |             |             |             | way that    |
|             |             |             |             | that        |
|             |             |             |             | different   |
|             |             |             |             | partitions  |
|             |             |             |             | from the    |
|             |             |             |             | same        |
|             |             |             |             | redundancy  |
|             |             |             |             | group have  |
|             |             |             |             | no or very  |
|             |             |             |             | small       |
|             |             |             |             | amount of   |
|             |             |             |             | common      |
|             |             |             |             | servers.    |
|             |             |             |             | Therefore,  |
|             |             |             |             | when        |
|             |             |             |             | different   |
|             |             |             |             | copies of   |
|             |             |             |             | the same    |
|             |             |             |             | data travel |
|             |             |             |             | through     |
|             |             |             |             | different   |
|             |             |             |             | partitions  |
|             |             |             |             | of the same |
|             |             |             |             | group, and  |
|             |             |             |             | one or more |
|             |             |             |             | servers     |
|             |             |             |             | from some   |
|             |             |             |             | partitions  |
|             |             |             |             | fail, the   |
|             |             |             |             | client is   |
|             |             |             |             | still able  |
|             |             |             |             | to retrieve |
|             |             |             |             | the data    |
|             |             |             |             | copy that   |
|             |             |             |             | travels     |
|             |             |             |             | through the |
|             |             |             |             | remaining   |
|             |             |             |             | partitions  |
|             |             |             |             | of that     |
|             |             |             |             | group,      |
|             |             |             |             | which hosts |
|             |             |             |             | remain      |
|             |             |             |             | alive.      |
|             |             |             |             |             |
|             |             |             |             | At the same |
|             |             |             |             | time, a     |
|             |             |             |             | given       |
|             |             |             |             | server may  |
|             |             |             |             | belong to   |
|             |             |             |             | partitions  |
|             |             |             |             | from        |
|             |             |             |             | different   |
|             |             |             |             | groups.     |
|             |             |             |             | This way,   |
|             |             |             |             | the saved   |
|             |             |             |             | parts from  |
|             |             |             |             | different   |
|             |             |             |             | data travel |
|             |             |             |             | across the  |
|             |             |             |             | groups,     |
|             |             |             |             | thus hiding |
|             |             |             |             | the current |
|             |             |             |             | structure   |
|             |             |             |             | of the      |
|             |             |             |             | groups and  |
|             |             |             |             | their       |
|             |             |             |             | partitions. |
|             |             |             |             |             |
|             |             |             |             | The layout  |
|             |             |             |             | of the      |
|             |             |             |             | groups and  |
|             |             |             |             | partitions  |
|             |             |             |             | is fluid –  |
|             |             |             |             | it randomly |
|             |             |             |             | changes     |
|             |             |             |             | with time   |
|             |             |             |             | according   |
|             |             |             |             | to          |
|             |             |             |             | parameters  |
|             |             |             |             | of putg     |
|             |             |             |             | command.    |
+-------------+-------------+-------------+-------------+-------------+
| LOGRECORD   | -lr         | -           | -           | When set in |
|             |             |             |             | commands    |
|             |             |             |             | get, getl,  |
|             |             |             |             | chk, chkl,  |
|             |             |             |             | del, and    |
|             |             |             |             | dell, this  |
|             |             |             |             | parameter   |
|             |             |             |             | specifies   |
|             |             |             |             | the full    |
|             |             |             |             | log record  |
|             |             |             |             | as returned |
|             |             |             |             | by one of   |
|             |             |             |             | the         |
|             |             |             |             | previously  |
|             |             |             |             | executed    |
|             |             |             |             | commands    |
|             |             |             |             | mov, put,   |
|             |             |             |             | or putl,    |
|             |             |             |             | with        |
|             |             |             |             | parameter   |
|             |             |             |             | LOGSAVE set |
|             |             |             |             | to 0 or -1. |
|             |             |             |             | The         |
|             |             |             |             | supplied    |
|             |             |             |             | log record  |
|             |             |             |             | is used to  |
|             |             |             |             | find the    |
|             |             |             |             | data in     |
|             |             |             |             | CryptoMove  |
|             |             |             |             | data store  |
|             |             |             |             | without     |
|             |             |             |             | consulting  |
|             |             |             |             | any log     |
|             |             |             |             | file.       |
+-------------+-------------+-------------+-------------+-------------+
| LOGSAVE     | -l          | 1           | -2:1        | When set to |
|             |             |             |             | 0 in one of |
|             |             |             |             | the         |
|             |             |             |             | commands    |
|             |             |             |             | mov, put,   |
|             |             |             |             | or putl,    |
|             |             |             |             | this        |
|             |             |             |             | parameter   |
|             |             |             |             | forces the  |
|             |             |             |             | server not  |
|             |             |             |             | to update   |
|             |             |             |             | any log,    |
|             |             |             |             | but return  |
|             |             |             |             | the log     |
|             |             |             |             | record for  |
|             |             |             |             | the saved   |
|             |             |             |             | data on the |
|             |             |             |             | client’s    |
|             |             |             |             | standard    |
|             |             |             |             | output.     |
|             |             |             |             | This log    |
|             |             |             |             | record can  |
|             |             |             |             | be used     |
|             |             |             |             | later for   |
|             |             |             |             | parameter   |
|             |             |             |             | LOGRECORD   |
|             |             |             |             | in one of   |
|             |             |             |             | the         |
|             |             |             |             | commands    |
|             |             |             |             | get, getl,  |
|             |             |             |             | chk, chkl,  |
|             |             |             |             | del, and    |
|             |             |             |             | dell.       |
|             |             |             |             |             |
|             |             |             |             | When set to |
|             |             |             |             | -1, a short |
|             |             |             |             | log record  |
|             |             |             |             | is returned |
|             |             |             |             | instead of  |
|             |             |             |             | a full log  |
|             |             |             |             | record,     |
|             |             |             |             | which is 64 |
|             |             |             |             | bytes in    |
|             |             |             |             | the hex     |
|             |             |             |             | representat |
|             |             |             |             | ion         |
|             |             |             |             | of the      |
|             |             |             |             | saved data  |
|             |             |             |             | uuid and    |
|             |             |             |             | random      |
|             |             |             |             | seed.       |
|             |             |             |             |             |
|             |             |             |             | Value -2 is |
|             |             |             |             | the same as |
|             |             |             |             | 0, except   |
|             |             |             |             | the log     |
|             |             |             |             | record      |
|             |             |             |             | contains no |
|             |             |             |             | parameters  |
|             |             |             |             | except the  |
|             |             |             |             | seed        |
|             |             |             |             | followed by |
|             |             |             |             | uuid        |
|             |             |             |             | without any |
|             |             |             |             | separator.  |
|             |             |             |             | For putl    |
|             |             |             |             | command,    |
|             |             |             |             | the data is |
|             |             |             |             | preserved   |
|             |             |             |             | in a        |
|             |             |             |             | temporary   |
|             |             |             |             | file under  |
|             |             |             |             | $CRYPTOMOVE |
|             |             |             |             | _API_PATH   |
|             |             |             |             | first. Then |
|             |             |             |             | the server  |
|             |             |             |             | replies     |
|             |             |             |             | success to  |
|             |             |             |             | the client, |
|             |             |             |             | while the   |
|             |             |             |             | preserved   |
|             |             |             |             | file is     |
|             |             |             |             | scheduled   |
|             |             |             |             | for saving  |
|             |             |             |             | on a        |
|             |             |             |             | dedicated   |
|             |             |             |             | server      |
|             |             |             |             | thread.     |
|             |             |             |             | After the   |
|             |             |             |             | preserved   |
|             |             |             |             | file is     |
|             |             |             |             | saved, the  |
|             |             |             |             | server      |
|             |             |             |             | deletes the |
|             |             |             |             | temp file.  |
|             |             |             |             |             |
|             |             |             |             | A client    |
|             |             |             |             | can issue   |
|             |             |             |             | putr        |
|             |             |             |             | command in  |
|             |             |             |             | order to    |
|             |             |             |             | push the    |
|             |             |             |             | preserved   |
|             |             |             |             | data into   |
|             |             |             |             | CryptoMove  |
|             |             |             |             | data store  |
|             |             |             |             | in case     |
|             |             |             |             | that data   |
|             |             |             |             | ended up    |
|             |             |             |             | stuck under |
|             |             |             |             | $CRYPTOMOVE |
|             |             |             |             | _API_PATH   |
|             |             |             |             | (say, due   |
|             |             |             |             | to a        |
|             |             |             |             | hardware or |
|             |             |             |             | software    |
|             |             |             |             | fault and   |
|             |             |             |             | crash).     |
|             |             |             |             |             |
|             |             |             |             | When set to |
|             |             |             |             | 1, a log    |
|             |             |             |             | record is   |
|             |             |             |             | appended to |
|             |             |             |             | the log of  |
|             |             |             |             | the saved   |
|             |             |             |             | data when   |
|             |             |             |             | that data   |
|             |             |             |             | is being    |
|             |             |             |             | saved.      |
+-------------+-------------+-------------+-------------+-------------+
| MODNAME     | -in         | -           | -           | This value  |
|             |             |             |             | determines  |
|             |             |             |             | the file    |
|             |             |             |             | name which  |
|             |             |             |             | data is     |
|             |             |             |             | used to     |
|             |             |             |             | modify the  |
|             |             |             |             | already     |
|             |             |             |             | stored file |
|             |             |             |             | or data by  |
|             |             |             |             | the mod     |
|             |             |             |             | command. If |
|             |             |             |             | not set,    |
|             |             |             |             | the client  |
|             |             |             |             | asks for    |
|             |             |             |             | the         |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             | data from   |
|             |             |             |             | its         |
|             |             |             |             | standard    |
|             |             |             |             | input.      |
+-------------+-------------+-------------+-------------+-------------+
| MODOFFSET   | -se -cu -en | -se         | -2:sup:`63` | This value  |
|             |             |             | \ +1:2\ :su | defines the |
|             |             |             | p:`63`-2    | offset in   |
|             |             |             |             | the last    |
|             |             |             |             | committed   |
|             |             |             |             | version     |
|             |             |             |             | from which  |
|             |             |             |             | the         |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             | commands    |
|             |             |             |             | mod and     |
|             |             |             |             | modl insert |
|             |             |             |             | the         |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             | data. The   |
|             |             |             |             | offset is   |
|             |             |             |             | calculated  |
|             |             |             |             | from the    |
|             |             |             |             | position    |
|             |             |             |             | determined  |
|             |             |             |             | by the      |
|             |             |             |             | option:     |
|             |             |             |             |             |
|             |             |             |             | -se: from   |
|             |             |             |             | the start   |
|             |             |             |             | of file or  |
|             |             |             |             | data        |
|             |             |             |             |             |
|             |             |             |             | -cu: from   |
|             |             |             |             | the current |
|             |             |             |             | position;   |
|             |             |             |             | the current |
|             |             |             |             | position is |
|             |             |             |             | the end of  |
|             |             |             |             | file for    |
|             |             |             |             | the first   |
|             |             |             |             | modificatio |
|             |             |             |             | n,          |
|             |             |             |             | or the end  |
|             |             |             |             | of last     |
|             |             |             |             | inserted    |
|             |             |             |             | data after  |
|             |             |             |             | each        |
|             |             |             |             | subsequent  |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             |             |
|             |             |             |             | -en: from   |
|             |             |             |             | the end of  |
|             |             |             |             | file or     |
|             |             |             |             | data        |
+-------------+-------------+-------------+-------------+-------------+
| MODPOSITION | -           | 1           | 0:2         | This value  |
|             |             |             |             | defines the |
|             |             |             |             | default     |
|             |             |             |             | kind of the |
|             |             |             |             | default     |
|             |             |             |             | offset in   |
|             |             |             |             | the         |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             | commands    |
|             |             |             |             | mod and     |
|             |             |             |             | modl:       |
|             |             |             |             |             |
|             |             |             |             | 0: from the |
|             |             |             |             | start of    |
|             |             |             |             | file or     |
|             |             |             |             | data        |
|             |             |             |             |             |
|             |             |             |             | 1: from the |
|             |             |             |             | current     |
|             |             |             |             | position;   |
|             |             |             |             | the current |
|             |             |             |             | position is |
|             |             |             |             | the end of  |
|             |             |             |             | file for    |
|             |             |             |             | the first   |
|             |             |             |             | modificatio |
|             |             |             |             | n,          |
|             |             |             |             | or the end  |
|             |             |             |             | of last     |
|             |             |             |             | inserted    |
|             |             |             |             | data after  |
|             |             |             |             | each        |
|             |             |             |             | subsequent  |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             |             |
|             |             |             |             | 2: from the |
|             |             |             |             | end of file |
|             |             |             |             | or data     |
+-------------+-------------+-------------+-------------+-------------+
| MOVEPACE    | -m          | 2           | 1:86400     | This number |
|             |             |             |             | determines  |
|             |             |             |             | how many    |
|             |             |             |             | times per   |
|             |             |             |             | day a part  |
|             |             |             |             | moves. By   |
|             |             |             |             | default, it |
|             |             |             |             | moves at    |
|             |             |             |             | least once  |
|             |             |             |             | a day.      |
|             |             |             |             | Cryptomove  |
|             |             |             |             | calculates  |
|             |             |             |             | the time in |
|             |             |             |             | seconds     |
|             |             |             |             | until next  |
|             |             |             |             | move for    |
|             |             |             |             | each part   |
|             |             |             |             | independent |
|             |             |             |             | ly,         |
|             |             |             |             | as a random |
|             |             |             |             | number      |
|             |             |             |             | equally     |
|             |             |             |             | distributed |
|             |             |             |             | between 1   |
|             |             |             |             | and 2*M,    |
|             |             |             |             | where M =   |
|             |             |             |             | 86400/MOVEP |
|             |             |             |             | ACE.        |
+-------------+-------------+-------------+-------------+-------------+
| NUMBER      | -n          | 0           | -4096:256   | Indicates   |
|             |             |             |             | how many    |
|             |             |             |             | parts of    |
|             |             |             |             | saved file  |
|             |             |             |             | to move     |
|             |             |             |             | around:     |
|             |             |             |             |             |
|             |             |             |             | -  If > 0,  |
|             |             |             |             |    then     |
|             |             |             |             |    move     |
|             |             |             |             |    only     |
|             |             |             |             |    first    |
|             |             |             |             |    specifie |
|             |             |             |             | d           |
|             |             |             |             |    number   |
|             |             |             |             |    of       |
|             |             |             |             |    copies   |
|             |             |             |             |    from all |
|             |             |             |             |    parts.   |
|             |             |             |             |    For      |
|             |             |             |             |    example, |
|             |             |             |             |    if       |
|             |             |             |             |    NUMBER   |
|             |             |             |             |    is 2,    |
|             |             |             |             |    then all |
|             |             |             |             |    parts    |
|             |             |             |             |    from the |
|             |             |             |             |    first    |
|             |             |             |             |    two      |
|             |             |             |             |    copies   |
|             |             |             |             |    of the   |
|             |             |             |             |    saved    |
|             |             |             |             |    file are |
|             |             |             |             |    moved    |
|             |             |             |             |    around   |
|             |             |             |             |    the      |
|             |             |             |             |    network  |
|             |             |             |             |             |
|             |             |             |             | -  If == 0, |
|             |             |             |             |    then     |
|             |             |             |             |    move all |
|             |             |             |             |    copies   |
|             |             |             |             |    of all   |
|             |             |             |             |    parts.   |
|             |             |             |             |             |
|             |             |             |             | -  If < 0,  |
|             |             |             |             |    then     |
|             |             |             |             |    move     |
|             |             |             |             |    only     |
|             |             |             |             |    first    |
|             |             |             |             |    -NUMBER  |
|             |             |             |             |    parts.   |
|             |             |             |             |    For      |
|             |             |             |             |    example, |
|             |             |             |             |    if       |
|             |             |             |             |    NUMBER   |
|             |             |             |             |    is -2,   |
|             |             |             |             |    then     |
|             |             |             |             |    move     |
|             |             |             |             |    only two |
|             |             |             |             |    of all   |
|             |             |             |             |    created  |
|             |             |             |             |    file     |
|             |             |             |             |    parts.   |
+-------------+-------------+-------------+-------------+-------------+
| NOWAIT      | -nw         | 0           | [0,1]       | This option |
|             |             |             |             | specifies   |
|             |             |             |             | either to   |
|             |             |             |             | wait (0) or |
|             |             |             |             | not to wait |
|             |             |             |             | (1) for     |
|             |             |             |             | part pulse  |
|             |             |             |             | events that |
|             |             |             |             | are         |
|             |             |             |             | currently   |
|             |             |             |             | missing on  |
|             |             |             |             | the base    |
|             |             |             |             | host. For   |
|             |             |             |             | example,    |
|             |             |             |             | when a part |
|             |             |             |             | has been    |
|             |             |             |             | deleted or  |
|             |             |             |             | cannot      |
|             |             |             |             | communicate |
|             |             |             |             | with the    |
|             |             |             |             | base host   |
|             |             |             |             | its pulse   |
|             |             |             |             | may never   |
|             |             |             |             | arrive, so  |
|             |             |             |             | there is no |
|             |             |             |             | need to     |
|             |             |             |             | wait for    |
|             |             |             |             | it.         |
+-------------+-------------+-------------+-------------+-------------+
| PERMUTATION | -pe         | 1           | [0,1]       | When set to |
|             |             |             |             | 1, each     |
|             |             |             |             | data part   |
|             |             |             |             | is permuted |
|             |             |             |             | according   |
|             |             |             |             | to a        |
|             |             |             |             | randomly    |
|             |             |             |             | generated   |
|             |             |             |             | permutation |
|             |             |             |             | before      |
|             |             |             |             | being saved |
|             |             |             |             | to the data |
|             |             |             |             | store;      |
|             |             |             |             | permutation |
|             |             |             |             | parts are   |
|             |             |             |             | also saved  |
|             |             |             |             | in the data |
|             |             |             |             | store. Upon |
|             |             |             |             | restoration |
|             |             |             |             | ,           |
|             |             |             |             | both data   |
|             |             |             |             | and         |
|             |             |             |             | permutation |
|             |             |             |             | parts are   |
|             |             |             |             | restored,   |
|             |             |             |             | and the     |
|             |             |             |             | data parts  |
|             |             |             |             | are         |
|             |             |             |             | un-permuted |
|             |             |             |             | according   |
|             |             |             |             | to the      |
|             |             |             |             | retrieved   |
|             |             |             |             | permutation |
|             |             |             |             | parts. When |
|             |             |             |             | set to 0,   |
|             |             |             |             | no random   |
|             |             |             |             | permutation |
|             |             |             |             | s           |
|             |             |             |             | are         |
|             |             |             |             | generated,  |
|             |             |             |             | data parts  |
|             |             |             |             | are not     |
|             |             |             |             | permuted    |
|             |             |             |             | before      |
|             |             |             |             | being       |
|             |             |             |             | saved; upon |
|             |             |             |             | retrieval   |
|             |             |             |             | only data   |
|             |             |             |             | parts are   |
|             |             |             |             | restored –  |
|             |             |             |             | they are    |
|             |             |             |             | not         |
|             |             |             |             | un-permuted |
|             |             |             |             | .           |
|             |             |             |             |             |
|             |             |             |             | Having data |
|             |             |             |             | permuted    |
|             |             |             |             | increases   |
|             |             |             |             | the entropy |
|             |             |             |             | of the      |
|             |             |             |             | saved data  |
|             |             |             |             | but slows   |
|             |             |             |             | down the    |
|             |             |             |             | restoration |
|             |             |             |             | process.    |
|             |             |             |             | Un-permuted |
|             |             |             |             | saved data  |
|             |             |             |             | has less    |
|             |             |             |             | entropy but |
|             |             |             |             | can be      |
|             |             |             |             | restored    |
|             |             |             |             | faster.     |
+-------------+-------------+-------------+-------------+-------------+
| PULSEBEAT   | -p          | 2           | 1:86400     | This number |
|             |             |             |             | determines  |
|             |             |             |             | how many    |
|             |             |             |             | times a day |
|             |             |             |             | a pulse     |
|             |             |             |             | message is  |
|             |             |             |             | generated   |
|             |             |             |             | from the    |
|             |             |             |             | moment a    |
|             |             |             |             | part lands  |
|             |             |             |             | on the host |
|             |             |             |             | till the    |
|             |             |             |             | time it     |
|             |             |             |             | departs     |
|             |             |             |             | from the    |
|             |             |             |             | host. The   |
|             |             |             |             | pulse       |
|             |             |             |             | message     |
|             |             |             |             | travels     |
|             |             |             |             | back to its |
|             |             |             |             | base host   |
|             |             |             |             | along the   |
|             |             |             |             | path        |
|             |             |             |             | traveled so |
|             |             |             |             | far. It     |
|             |             |             |             | leaves a    |
|             |             |             |             | randomly    |
|             |             |             |             | named       |
|             |             |             |             | encrypted   |
|             |             |             |             | track file  |
|             |             |             |             | on each     |
|             |             |             |             | host from   |
|             |             |             |             | the path.   |
|             |             |             |             | Each new    |
|             |             |             |             | pulse       |
|             |             |             |             | removes the |
|             |             |             |             | track left  |
|             |             |             |             | by the      |
|             |             |             |             | previous    |
|             |             |             |             | pulse       |
|             |             |             |             | before      |
|             |             |             |             | leaving its |
|             |             |             |             | track.      |
|             |             |             |             |             |
|             |             |             |             | This way,   |
|             |             |             |             | the servers |
|             |             |             |             | maintain an |
|             |             |             |             | encrypted   |
|             |             |             |             | ever-changi |
|             |             |             |             | ng          |
|             |             |             |             | sequence of |
|             |             |             |             | track files |
|             |             |             |             | that lead   |
|             |             |             |             | from the    |
|             |             |             |             | base to the |
|             |             |             |             | current     |
|             |             |             |             | location of |
|             |             |             |             | a file      |
|             |             |             |             | part. When  |
|             |             |             |             | the client  |
|             |             |             |             | issues a    |
|             |             |             |             | command to  |
|             |             |             |             | reach the   |
|             |             |             |             | part, the   |
|             |             |             |             | servers     |
|             |             |             |             | follow the  |
|             |             |             |             | current     |
|             |             |             |             | track from  |
|             |             |             |             | the base to |
|             |             |             |             | the part’s  |
|             |             |             |             | current     |
|             |             |             |             | location.   |
|             |             |             |             | By default, |
|             |             |             |             | a new pulse |
|             |             |             |             | is          |
|             |             |             |             | generated   |
|             |             |             |             | every six   |
|             |             |             |             | hours.      |
|             |             |             |             |             |
|             |             |             |             | Setting     |
|             |             |             |             | GROUPNAME   |
|             |             |             |             | to the      |
|             |             |             |             | literal     |
|             |             |             |             | value       |
|             |             |             |             | ”NONE”      |
|             |             |             |             | eliminates  |
|             |             |             |             | default     |
|             |             |             |             | setting for |
|             |             |             |             | the         |
|             |             |             |             | GROUPNAME.  |
+-------------+-------------+-------------+-------------+-------------+
| REMOTENAME  | -r          | -           | -           | When the    |
|             |             |             |             | saved data  |
|             |             |             |             | is assigned |
|             |             |             |             | a remote    |
|             |             |             |             | name, that  |
|             |             |             |             | name        |
|             |             |             |             | becomes a   |
|             |             |             |             | part of the |
|             |             |             |             | CryptoMove  |
|             |             |             |             | namespace.  |
|             |             |             |             | Then it is  |
|             |             |             |             | possible to |
|             |             |             |             | restore     |
|             |             |             |             | that data   |
|             |             |             |             | from the    |
|             |             |             |             | CryptoMove  |
|             |             |             |             | data store  |
|             |             |             |             | onto any    |
|             |             |             |             | client      |
|             |             |             |             | computer,   |
|             |             |             |             | under any   |
|             |             |             |             | directory   |
|             |             |             |             | with any    |
|             |             |             |             | file name.  |
+-------------+-------------+-------------+-------------+-------------+
| SAVELOCAL   | -sa         | 0           | 0:1         | This value  |
|             |             |             |             | determines  |
|             |             |             |             | if the      |
|             |             |             |             | committed   |
|             |             |             |             | file or     |
|             |             |             |             | data must   |
|             |             |             |             | be saved    |
|             |             |             |             | locally     |
|             |             |             |             | after       |
|             |             |             |             | commit in   |
|             |             |             |             | the commit  |
|             |             |             |             | commands    |
|             |             |             |             | com and     |
|             |             |             |             | coml. If    |
|             |             |             |             | set to 1,   |
|             |             |             |             | the file is |
|             |             |             |             | saved in    |
|             |             |             |             | the local   |
|             |             |             |             | data store, |
|             |             |             |             | while the   |
|             |             |             |             | data is     |
|             |             |             |             | printed on  |
|             |             |             |             | the         |
|             |             |             |             | client’s    |
|             |             |             |             | standard    |
|             |             |             |             | output; if  |
|             |             |             |             | set to 0,   |
|             |             |             |             | then none   |
|             |             |             |             | of the      |
|             |             |             |             | above       |
|             |             |             |             | happens.    |
+-------------+-------------+-------------+-------------+-------------+
| SCATTERLENG | -b          | 0           | 0:64        | This number |
| TH          |             |             |             | determines  |
|             |             |             |             | the length  |
|             |             |             |             | of the      |
|             |             |             |             | scattering  |
|             |             |             |             | interval.   |
|             |             |             |             | When saving |
|             |             |             |             | a file,     |
|             |             |             |             | after the   |
|             |             |             |             | creation of |
|             |             |             |             | all         |
|             |             |             |             | encrypted   |
|             |             |             |             | permutes    |
|             |             |             |             | parts, the  |
|             |             |             |             | client      |
|             |             |             |             | concatenate |
|             |             |             |             | s           |
|             |             |             |             | the parts   |
|             |             |             |             | of each     |
|             |             |             |             | copy back   |
|             |             |             |             | into a      |
|             |             |             |             | consecutive |
|             |             |             |             | sequence of |
|             |             |             |             | bytes. Then |
|             |             |             |             | it divides  |
|             |             |             |             | the         |
|             |             |             |             | resulting   |
|             |             |             |             | sequence    |
|             |             |             |             | into equal  |
|             |             |             |             | intervals   |
|             |             |             |             | of the      |
|             |             |             |             | specified   |
|             |             |             |             | bit-length  |
|             |             |             |             | and         |
|             |             |             |             | scatters    |
|             |             |             |             | the bits    |
|             |             |             |             | from the    |
|             |             |             |             | intervals.  |
|             |             |             |             | First, it   |
|             |             |             |             | assembles   |
|             |             |             |             | the first   |
|             |             |             |             | bits from   |
|             |             |             |             | each        |
|             |             |             |             | interval,   |
|             |             |             |             | then the    |
|             |             |             |             | second      |
|             |             |             |             | bits, etc.  |
|             |             |             |             | up to the   |
|             |             |             |             | interval    |
|             |             |             |             | length. All |
|             |             |             |             | assembled   |
|             |             |             |             | bits are    |
|             |             |             |             | concatenate |
|             |             |             |             | d           |
|             |             |             |             | and split   |
|             |             |             |             | back into   |
|             |             |             |             | parts       |
|             |             |             |             | again.      |
|             |             |             |             |             |
|             |             |             |             | Upon        |
|             |             |             |             | retrieval,  |
|             |             |             |             | the file    |
|             |             |             |             | parts       |
|             |             |             |             | undergo a   |
|             |             |             |             | reverse     |
|             |             |             |             | process on  |
|             |             |             |             | the client  |
|             |             |             |             | host,       |
|             |             |             |             | before      |
|             |             |             |             | being       |
|             |             |             |             | decrypted   |
|             |             |             |             | and         |
|             |             |             |             | un-permuted |
|             |             |             |             | to be       |
|             |             |             |             | delivered   |
|             |             |             |             | into the    |
|             |             |             |             | user data   |
|             |             |             |             | store.      |
|             |             |             |             |             |
|             |             |             |             | When        |
|             |             |             |             | SCATTERLENG |
|             |             |             |             | TH          |
|             |             |             |             | is 0, then  |
|             |             |             |             | no          |
|             |             |             |             | scattering  |
|             |             |             |             | takes       |
|             |             |             |             | place.      |
+-------------+-------------+-------------+-------------+-------------+
| SIZE        | -si         | 8384        | 1:4194304   | The max     |
|             |             |             |             | size of     |
|             |             |             |             | data saved  |
|             |             |             |             | in commands |
|             |             |             |             | putl and    |
|             |             |             |             | modl.       |
+-------------+-------------+-------------+-------------+-------------+
| SPLITCOUNT  | -s          | 1           | 1:4096      | This number |
|             |             |             |             | determines  |
|             |             |             |             | into how    |
|             |             |             |             | many parts  |
|             |             |             |             | the client  |
|             |             |             |             | splits the  |
|             |             |             |             | file before |
|             |             |             |             | saving the  |
|             |             |             |             | parts in    |
|             |             |             |             | CryptoMove  |
|             |             |             |             | data store. |
+-------------+-------------+-------------+-------------+-------------+
| STRENGTH    | -co         | 0           | 0:9         | This number |
|             |             |             |             | determines  |
|             |             |             |             | compression |
|             |             |             |             | strength of |
|             |             |             |             | the saved   |
|             |             |             |             | data. Value |
|             |             |             |             | 0 means no  |
|             |             |             |             | compression |
|             |             |             |             | is          |
|             |             |             |             | performed.  |
|             |             |             |             | CryptoMove  |
|             |             |             |             | client uses |
|             |             |             |             | UNIX gzip   |
|             |             |             |             | utility to  |
|             |             |             |             | compress    |
|             |             |             |             | each saved  |
|             |             |             |             | file. If    |
|             |             |             |             | compression |
|             |             |             |             | does not    |
|             |             |             |             | decrease    |
|             |             |             |             | the file    |
|             |             |             |             | length,     |
|             |             |             |             | then the    |
|             |             |             |             | file is     |
|             |             |             |             | saved       |
|             |             |             |             | uncompresse |
|             |             |             |             | d.          |
|             |             |             |             |             |
|             |             |             |             | You may     |
|             |             |             |             | always      |
|             |             |             |             | choose      |
|             |             |             |             | utilities   |
|             |             |             |             | other than  |
|             |             |             |             | gzip to     |
|             |             |             |             | compress    |
|             |             |             |             | the file    |
|             |             |             |             | before      |
|             |             |             |             | saving it   |
|             |             |             |             | in          |
|             |             |             |             | CryptoMove  |
|             |             |             |             | data store. |
+-------------+-------------+-------------+-------------+-------------+
| TARGETSTORE | -to         | -           | -           | This value  |
|             |             |             |             | determines  |
|             |             |             |             | where the   |
|             |             |             |             | saved data  |
|             |             |             |             | must be     |
|             |             |             |             | stored. If  |
|             |             |             |             | TARGETSTORE |
|             |             |             |             | is not set, |
|             |             |             |             | then data   |
|             |             |             |             | is always   |
|             |             |             |             | saved in    |
|             |             |             |             | the data    |
|             |             |             |             | store under |
|             |             |             |             | directory   |
|             |             |             |             | specified   |
|             |             |             |             | by          |
|             |             |             |             | CRYPTOMOVE_ |
|             |             |             |             | PATH.       |
|             |             |             |             | If          |
|             |             |             |             | TARGETSTORE |
|             |             |             |             | is set, it  |
|             |             |             |             | shall       |
|             |             |             |             | conform to  |
|             |             |             |             | one of the  |
|             |             |             |             | following   |
|             |             |             |             | formats:    |
|             |             |             |             |             |
|             |             |             |             | -  host:    |
|             |             |             |             |    Specifie |
|             |             |             |             | s           |
|             |             |             |             |    the name |
|             |             |             |             |    of the   |
|             |             |             |             |    base     |
|             |             |             |             |    host     |
|             |             |             |             |    that     |
|             |             |             |             |    confines |
|             |             |             |             |    the      |
|             |             |             |             |    movement |
|             |             |             |             |    of the   |
|             |             |             |             |    saved    |
|             |             |             |             |    parts –  |
|             |             |             |             |    the      |
|             |             |             |             |    parts    |
|             |             |             |             |    move     |
|             |             |             |             |    only     |
|             |             |             |             |    within   |
|             |             |             |             |    that     |
|             |             |             |             |    host.    |
|             |             |             |             |             |
|             |             |             |             | -  [:]direc |
|             |             |             |             | tory:       |
|             |             |             |             |    Specifie |
|             |             |             |             | d           |
|             |             |             |             |    the name |
|             |             |             |             |    of the   |
|             |             |             |             |    director |
|             |             |             |             | y           |
|             |             |             |             |    that     |
|             |             |             |             |    confines |
|             |             |             |             |    the      |
|             |             |             |             |    movement |
|             |             |             |             |    of the   |
|             |             |             |             |    saved    |
|             |             |             |             |    data;    |
|             |             |             |             |    the      |
|             |             |             |             |    parts    |
|             |             |             |             |    move     |
|             |             |             |             |    only     |
|             |             |             |             |    within   |
|             |             |             |             |    that     |
|             |             |             |             |    director |
|             |             |             |             | y.          |
|             |             |             |             |             |
|             |             |             |             | -  host:dir |
|             |             |             |             | ectory:     |
|             |             |             |             |    Specifie |
|             |             |             |             | s           |
|             |             |             |             |    both the |
|             |             |             |             |    host and |
|             |             |             |             |    the      |
|             |             |             |             |    director |
|             |             |             |             | y           |
|             |             |             |             |    that     |
|             |             |             |             |    confine  |
|             |             |             |             |    data     |
|             |             |             |             |    movement |
|             |             |             |             | ;           |
|             |             |             |             |    the      |
|             |             |             |             |    parts    |
|             |             |             |             |    move     |
|             |             |             |             |    only     |
|             |             |             |             |    within   |
|             |             |             |             |    the      |
|             |             |             |             |    specifie |
|             |             |             |             | d           |
|             |             |             |             |    director |
|             |             |             |             | y           |
|             |             |             |             |    on the   |
|             |             |             |             |    specifie |
|             |             |             |             | d           |
|             |             |             |             |    host.    |
|             |             |             |             |             |
|             |             |             |             | -  NONE:    |
|             |             |             |             |    The      |
|             |             |             |             |    literal  |
|             |             |             |             |    value    |
|             |             |             |             |    ”NONE”   |
|             |             |             |             |    indicate |
|             |             |             |             | s           |
|             |             |             |             |    that the |
|             |             |             |             |    stored   |
|             |             |             |             |    data is  |
|             |             |             |             |    placed   |
|             |             |             |             |    under    |
|             |             |             |             |    the      |
|             |             |             |             |    default  |
|             |             |             |             |    target   |
|             |             |             |             |    store    |
|             |             |             |             |    set in   |
|             |             |             |             |    CRYPTOMO |
|             |             |             |             | VE_PATH     |
|             |             |             |             |             |
|             |             |             |             | If          |
|             |             |             |             | DIRECTORY   |
|             |             |             |             | is          |
|             |             |             |             | specified,  |
|             |             |             |             | then data   |
|             |             |             |             | parts will  |
|             |             |             |             | be moving   |
|             |             |             |             | only within |
|             |             |             |             | this        |
|             |             |             |             | directory   |
|             |             |             |             | inside any  |
|             |             |             |             | host it     |
|             |             |             |             | moves to.   |
+-------------+-------------+-------------+-------------+-------------+
| TESTCOUNT   | -u          | -           | -           | Determines  |
|             |             |             |             | how many    |
|             |             |             |             | times to    |
|             |             |             |             | repeat the  |
|             |             |             |             | command. If |
|             |             |             |             | not         |
|             |             |             |             | specified,  |
|             |             |             |             | command is  |
|             |             |             |             | executed    |
|             |             |             |             | only once.  |
+-------------+-------------+-------------+-------------+-------------+
| EXPIRATION  | -x          | 525600000   | 0:525600000 | Time period |
|             |             |             |             | in minutes  |
|             |             |             |             | after which |
|             |             |             |             | the saved   |
|             |             |             |             | data is     |
|             |             |             |             | automatical |
|             |             |             |             | ly          |
|             |             |             |             | deleted     |
|             |             |             |             | from the    |
|             |             |             |             | data store. |
|             |             |             |             | By default, |
|             |             |             |             | it expires  |
|             |             |             |             | in one      |
|             |             |             |             | thousand    |
|             |             |             |             | years.      |
+-------------+-------------+-------------+-------------+-------------+

Parameters in Commands to Restore or Delete Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following table explains CryptoMove client parameters and flags that
set those parameters in the commands to restore and delete data log,
rlog, get, mget, chk, mchk, del, mdel, clr, mclr, logl, chkl, dell, and
clrl. Default indicates the value set to the parameter upon client
startup. Interval shows allowed values.

+-------------+-------------+-------------+-------------+-------------+
| **Parameter | **Flag**    | **Default** | **Interval* | **Explanati |
| **          |             |             | *           | on**        |
+=============+=============+=============+=============+=============+
| NUMBER      | -n          | 1           | -4096:256   | Indicates   |
|             |             |             |             | how many    |
|             |             |             |             | file copies |
|             |             |             |             | to restore  |
|             |             |             |             | from the    |
|             |             |             |             | data store  |
|             |             |             |             | (useful in  |
|             |             |             |             | case some   |
|             |             |             |             | of the      |
|             |             |             |             | parts are   |
|             |             |             |             | missing or  |
|             |             |             |             | damaged).   |
|             |             |             |             |             |
|             |             |             |             | -  If > 0,  |
|             |             |             |             |    then     |
|             |             |             |             |    restore  |
|             |             |             |             |    only     |
|             |             |             |             |    first    |
|             |             |             |             |    specifie |
|             |             |             |             | d           |
|             |             |             |             |    number   |
|             |             |             |             |    of       |
|             |             |             |             |    copies   |
|             |             |             |             |    from all |
|             |             |             |             |    parts.   |
|             |             |             |             |    For      |
|             |             |             |             |    example, |
|             |             |             |             |    if       |
|             |             |             |             |    NUMBER   |
|             |             |             |             |    is 2,    |
|             |             |             |             |    then all |
|             |             |             |             |    parts    |
|             |             |             |             |    from the |
|             |             |             |             |    first    |
|             |             |             |             |    two      |
|             |             |             |             |    copies   |
|             |             |             |             |    of the   |
|             |             |             |             |    saved    |
|             |             |             |             |    file are |
|             |             |             |             |    restored |
|             |             |             |             | .           |
|             |             |             |             |             |
|             |             |             |             | -  If == 0, |
|             |             |             |             |    then     |
|             |             |             |             |    restore  |
|             |             |             |             |    all      |
|             |             |             |             |    copies   |
|             |             |             |             |    of all   |
|             |             |             |             |    parts.   |
|             |             |             |             |             |
|             |             |             |             | -  If < 0,  |
|             |             |             |             |    then     |
|             |             |             |             |    restore  |
|             |             |             |             |    only     |
|             |             |             |             |    first    |
|             |             |             |             |    -NUMBER  |
|             |             |             |             |    parts.   |
|             |             |             |             |    For      |
|             |             |             |             |    example, |
|             |             |             |             |    if       |
|             |             |             |             |    NUMBER   |
|             |             |             |             |    is -2,   |
|             |             |             |             |    then     |
|             |             |             |             |    restore  |
|             |             |             |             |    only two |
|             |             |             |             |    of all   |
|             |             |             |             |    saved    |
|             |             |             |             |    file     |
|             |             |             |             |    parts.   |
+-------------+-------------+-------------+-------------+-------------+
| RECORDCOUNT | -c          | 0           | 0:unlimited | Specifies   |
|             |             |             |             | how many    |
|             |             |             |             | records     |
|             |             |             |             | from the    |
|             |             |             |             | end of the  |
|             |             |             |             | log shall   |
|             |             |             |             | be          |
|             |             |             |             | delivered   |
|             |             |             |             | in the log  |
|             |             |             |             | commands.   |
|             |             |             |             | If not set  |
|             |             |             |             | or set to   |
|             |             |             |             | 0, then all |
|             |             |             |             | records are |
|             |             |             |             | delivered.  |
+-------------+-------------+-------------+-------------+-------------+
| REMOTENAME  | -r          | -           | -           | Specifies   |
|             |             |             |             | remote name |
|             |             |             |             | if          |
|             |             |             |             | previously  |
|             |             |             |             | assigned to |
|             |             |             |             | saved data. |
+-------------+-------------+-------------+-------------+-------------+
| TIMEOUT     | -t          | 30          | 1:unlimited | The number  |
|             |             |             |             | of seconds  |
|             |             |             |             | the client  |
|             |             |             |             | waits until |
|             |             |             |             | CryptoMove  |
|             |             |             |             | servers     |
|             |             |             |             | complete    |
|             |             |             |             | the         |
|             |             |             |             | operation.  |
+-------------+-------------+-------------+-------------+-------------+
| TESTCOUNT   | -u          | 0           | 0:16284     | This number |
|             |             |             |             | determines  |
|             |             |             |             | how many    |
|             |             |             |             | times to    |
|             |             |             |             | repeat the  |
|             |             |             |             | specified   |
|             |             |             |             | command. By |
|             |             |             |             | default,    |
|             |             |             |             | each        |
|             |             |             |             | command is  |
|             |             |             |             | executed    |
|             |             |             |             | only once.  |
|             |             |             |             | However, if |
|             |             |             |             | this number |
|             |             |             |             | is bigger   |
|             |             |             |             | than one,   |
|             |             |             |             | then client |
|             |             |             |             | repeats the |
|             |             |             |             | command     |
|             |             |             |             | several     |
|             |             |             |             | times.      |
|             |             |             |             |             |
|             |             |             |             | Also, if    |
|             |             |             |             | TESTCOUNT > |
|             |             |             |             | 0, then the |
|             |             |             |             | server      |
|             |             |             |             | synchronize |
|             |             |             |             | s           |
|             |             |             |             | its steps   |
|             |             |             |             | during the  |
|             |             |             |             | part        |
|             |             |             |             | movement.   |
|             |             |             |             | This test   |
|             |             |             |             | mode is     |
|             |             |             |             | useful for  |
|             |             |             |             | debugging;  |
|             |             |             |             | you should  |
|             |             |             |             | not use it  |
|             |             |             |             | on a        |
|             |             |             |             | regular     |
|             |             |             |             | basis.      |
+-------------+-------------+-------------+-------------+-------------+
| USERNAME    | -us         | root        | -           | The         |
|             |             |             |             | argument    |
|             |             |             |             | string      |
|             |             |             |             | determines  |
|             |             |             |             | the user    |
|             |             |             |             | name of a   |
|             |             |             |             | user that   |
|             |             |             |             | which will  |
|             |             |             |             | own the     |
|             |             |             |             | restored    |
|             |             |             |             | file. By    |
|             |             |             |             | default,    |
|             |             |             |             | that user   |
|             |             |             |             | is root.    |
|             |             |             |             | This flag   |
|             |             |             |             | is          |
|             |             |             |             | available   |
|             |             |             |             | only for    |
|             |             |             |             | commands    |
|             |             |             |             | get and     |
|             |             |             |             | mget.       |
+-------------+-------------+-------------+-------------+-------------+
| UUIDONLY    | -uo         | 0           | 0:1         | When set to |
|             |             |             |             | 1 and       |
|             |             |             |             | LOGSAVE is  |
|             |             |             |             | set to -1   |
|             |             |             |             | or -2, the  |
|             |             |             |             | log record  |
|             |             |             |             | produced    |
|             |             |             |             | has only    |
|             |             |             |             | uuid        |
|             |             |             |             | portion.    |
|             |             |             |             | When set to |
|             |             |             |             | 0 and       |
|             |             |             |             | LOGSAVE is  |
|             |             |             |             | set to -1   |
|             |             |             |             | or -2, the  |
|             |             |             |             | log record  |
|             |             |             |             | produced    |
|             |             |             |             | has both    |
|             |             |             |             | uuid and    |
|             |             |             |             | seed        |
|             |             |             |             | portions.   |
|             |             |             |             | The default |
|             |             |             |             | value is 0. |
+-------------+-------------+-------------+-------------+-------------+
| VERSION     | -v          | last        | 1:last      | When client |
|             |             |             |             | saves a     |
|             |             |             |             | file the    |
|             |             |             |             | first time, |
|             |             |             |             | it assigns  |
|             |             |             |             | version 1   |
|             |             |             |             | to the      |
|             |             |             |             | saved file. |
|             |             |             |             | Repeatedly  |
|             |             |             |             | saving the  |
|             |             |             |             | same file   |
|             |             |             |             | increments  |
|             |             |             |             | the saved   |
|             |             |             |             | version.    |
|             |             |             |             | When        |
|             |             |             |             | restoring   |
|             |             |             |             | the file,   |
|             |             |             |             | the client  |
|             |             |             |             | retrieves   |
|             |             |             |             | from the    |
|             |             |             |             | store its   |
|             |             |             |             | last saved  |
|             |             |             |             | version.    |
|             |             |             |             | You may     |
|             |             |             |             | retrieve a  |
|             |             |             |             | specific    |
|             |             |             |             | version of  |
|             |             |             |             | the saved   |
|             |             |             |             | file by     |
|             |             |             |             | setting the |
|             |             |             |             | version     |
|             |             |             |             | number on   |
|             |             |             |             | the         |
|             |             |             |             | retrieval   |
|             |             |             |             | command.    |
|             |             |             |             |             |
|             |             |             |             | When used   |
|             |             |             |             | in the      |
|             |             |             |             | commit      |
|             |             |             |             | command com |
|             |             |             |             | or coml,    |
|             |             |             |             | this        |
|             |             |             |             | parameter   |
|             |             |             |             | specifies   |
|             |             |             |             | from which  |
|             |             |             |             | last        |
|             |             |             |             | modificatio |
|             |             |             |             | n           |
|             |             |             |             | version to  |
|             |             |             |             | apply the   |
|             |             |             |             | commit      |
|             |             |             |             | operation.  |
|             |             |             |             |             |
|             |             |             |             | When used   |
|             |             |             |             | in commands |
|             |             |             |             | purge or    |
|             |             |             |             | purgel,     |
|             |             |             |             | this        |
|             |             |             |             | parameter   |
|             |             |             |             | indicates a |
|             |             |             |             | version or  |
|             |             |             |             | an interval |
|             |             |             |             | of versions |
|             |             |             |             | for files   |
|             |             |             |             | or data,    |
|             |             |             |             | which       |
|             |             |             |             | records     |
|             |             |             |             | shall be    |
|             |             |             |             | p[urged     |
|             |             |             |             | from the    |
|             |             |             |             | log after   |
|             |             |             |             | committing  |
|             |             |             |             | those       |
|             |             |             |             | versions.   |
+-------------+-------------+-------------+-------------+-------------+

Parameters in Command to Create Data Redundancy Group
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following table explains CryptoMove client parameters and flags that
set those parameters in the command putg that creates a redundancy
group. Default indicates the value set to the parameter upon client
startup. Interval shows allowed values.

+-------------+-------------+-------------+-------------+-------------+
| **Parameter | **Flag**    | **Default** | **Interval* | **Explanati |
| **          |             |             | *           | on**        |
+=============+=============+=============+=============+=============+
| PULSEBEAT   | -p          | 4           | 1:86400     | A group     |
|             |             |             |             | partition   |
|             |             |             |             | is          |
|             |             |             |             | maintained  |
|             |             |             |             | via a       |
|             |             |             |             | unique      |
|             |             |             |             | token that  |
|             |             |             |             | travels the |
|             |             |             |             | data store  |
|             |             |             |             | much like a |
|             |             |             |             | part of the |
|             |             |             |             | saved data  |
|             |             |             |             | does. When  |
|             |             |             |             | token       |
|             |             |             |             | reaches a   |
|             |             |             |             | host on its |
|             |             |             |             | forward     |
|             |             |             |             | movement,   |
|             |             |             |             | the host    |
|             |             |             |             | joins the   |
|             |             |             |             | partition.  |
|             |             |             |             | When token  |
|             |             |             |             | retracts on |
|             |             |             |             | its reverse |
|             |             |             |             | direction,  |
|             |             |             |             | the host    |
|             |             |             |             | leaves the  |
|             |             |             |             | partition   |
|             |             |             |             | when the    |
|             |             |             |             | token       |
|             |             |             |             | leaves the  |
|             |             |             |             | host. When  |
|             |             |             |             | a token     |
|             |             |             |             | persists on |
|             |             |             |             | a host, it  |
|             |             |             |             | sends a     |
|             |             |             |             | periodic    |
|             |             |             |             | pulse       |
|             |             |             |             | message     |
|             |             |             |             | back to the |
|             |             |             |             | base host.  |
|             |             |             |             |             |
|             |             |             |             | This        |
|             |             |             |             | parameter   |
|             |             |             |             | determines  |
|             |             |             |             | how many    |
|             |             |             |             | times a day |
|             |             |             |             | a pulse     |
|             |             |             |             | message is  |
|             |             |             |             | generated   |
|             |             |             |             | from the    |
|             |             |             |             | moment a    |
|             |             |             |             | token lands |
|             |             |             |             | on the host |
|             |             |             |             | till the    |
|             |             |             |             | time it     |
|             |             |             |             | departs     |
|             |             |             |             | from the    |
|             |             |             |             | host. The   |
|             |             |             |             | pulse       |
|             |             |             |             | message     |
|             |             |             |             | travels     |
|             |             |             |             | back to its |
|             |             |             |             | base host   |
|             |             |             |             | along the   |
|             |             |             |             | path        |
|             |             |             |             | traveled so |
|             |             |             |             | far. It     |
|             |             |             |             | leaves a    |
|             |             |             |             | randomly    |
|             |             |             |             | named       |
|             |             |             |             | encrypted   |
|             |             |             |             | track file  |
|             |             |             |             | on each     |
|             |             |             |             | host from   |
|             |             |             |             | the path.   |
|             |             |             |             | Each new    |
|             |             |             |             | pulse       |
|             |             |             |             | removes the |
|             |             |             |             | track left  |
|             |             |             |             | by the      |
|             |             |             |             | previous    |
|             |             |             |             | pulse       |
|             |             |             |             | before      |
|             |             |             |             | leaving its |
|             |             |             |             | track.      |
|             |             |             |             |             |
|             |             |             |             | This way,   |
|             |             |             |             | the hosts   |
|             |             |             |             | maintain an |
|             |             |             |             | encrypted   |
|             |             |             |             | ever-changi |
|             |             |             |             | ng          |
|             |             |             |             | sequence of |
|             |             |             |             | track files |
|             |             |             |             | that lead   |
|             |             |             |             | from the    |
|             |             |             |             | base to the |
|             |             |             |             | current     |
|             |             |             |             | location of |
|             |             |             |             | the token.  |
|             |             |             |             | When the    |
|             |             |             |             | client      |
|             |             |             |             | issues a    |
|             |             |             |             | command to  |
|             |             |             |             | reach the   |
|             |             |             |             | token, the  |
|             |             |             |             | hosts       |
|             |             |             |             | follow the  |
|             |             |             |             | current     |
|             |             |             |             | track from  |
|             |             |             |             | the base to |
|             |             |             |             | the part’s  |
|             |             |             |             | current     |
|             |             |             |             | location.   |
|             |             |             |             | By default, |
|             |             |             |             | a new pulse |
|             |             |             |             | is          |
|             |             |             |             | generated   |
|             |             |             |             | every six   |
|             |             |             |             | hours.      |
|             |             |             |             |             |
|             |             |             |             | By issuing  |
|             |             |             |             | command     |
|             |             |             |             | wakeup from |
|             |             |             |             | the prompt  |
|             |             |             |             | of a system |
|             |             |             |             | owner       |
|             |             |             |             | client      |
|             |             |             |             | (started as |
|             |             |             |             | crv os),    |
|             |             |             |             | all pulses  |
|             |             |             |             | for all     |
|             |             |             |             | saved data  |
|             |             |             |             | parts are   |
|             |             |             |             | renewed     |
|             |             |             |             | immediately |
|             |             |             |             | .           |
|             |             |             |             | This is     |
|             |             |             |             | useful if   |
|             |             |             |             | for some    |
|             |             |             |             | reason,     |
|             |             |             |             | e.g.        |
|             |             |             |             | network     |
|             |             |             |             | malfunction |
|             |             |             |             | ,           |
|             |             |             |             | there is a  |
|             |             |             |             | problem     |
|             |             |             |             | restoring   |
|             |             |             |             | the saved   |
|             |             |             |             | data        |
|             |             |             |             | because     |
|             |             |             |             | some pulses |
|             |             |             |             | have been   |
|             |             |             |             | lost.       |
+-------------+-------------+-------------+-------------+-------------+
| ARMSLENGTH  | -a          | 4           | 1:256       | This number |
|             |             |             |             | determines  |
|             |             |             |             | through how |
|             |             |             |             | many        |
|             |             |             |             | different   |
|             |             |             |             | hosts the   |
|             |             |             |             | group token |
|             |             |             |             | may travel. |
|             |             |             |             | When set to |
|             |             |             |             | 1, it stays |
|             |             |             |             | on the host |
|             |             |             |             | where the   |
|             |             |             |             | client      |
|             |             |             |             | runs, even  |
|             |             |             |             | if its      |
|             |             |             |             | local       |
|             |             |             |             | server is   |
|             |             |             |             | an edge     |
|             |             |             |             | server.     |
+-------------+-------------+-------------+-------------+-------------+
| COPYCOUNT   | -c          | 1           | 1:256       | The number  |
|             |             |             |             | of          |
|             |             |             |             | partitions  |
|             |             |             |             | in the      |
|             |             |             |             | group.      |
+-------------+-------------+-------------+-------------+-------------+
| DATADEPTH   | -d          | 4           | 1:128       | The maximum |
|             |             |             |             | depth of    |
|             |             |             |             | the path    |
|             |             |             |             | each token  |
|             |             |             |             | traverses   |
|             |             |             |             | the         |
|             |             |             |             | network.    |
|             |             |             |             | Each time a |
|             |             |             |             | token       |
|             |             |             |             | moves, it   |
|             |             |             |             | adds to the |
|             |             |             |             | current     |
|             |             |             |             | depth of    |
|             |             |             |             | its path.   |
|             |             |             |             | All token   |
|             |             |             |             | movements   |
|             |             |             |             | are random, |
|             |             |             |             | so it is    |
|             |             |             |             | not         |
|             |             |             |             | possible to |
|             |             |             |             | predict     |
|             |             |             |             | which hosts |
|             |             |             |             | will belong |
|             |             |             |             | to a        |
|             |             |             |             | partition.  |
|             |             |             |             |             |
|             |             |             |             | When the    |
|             |             |             |             | depth       |
|             |             |             |             | reaches the |
|             |             |             |             | maximum     |
|             |             |             |             | value, the  |
|             |             |             |             | token       |
|             |             |             |             | reverses    |
|             |             |             |             | its         |
|             |             |             |             | movement    |
|             |             |             |             | and         |
|             |             |             |             | backtracks  |
|             |             |             |             | to the base |
|             |             |             |             | host that   |
|             |             |             |             | originated  |
|             |             |             |             | the         |
|             |             |             |             | movement.   |
|             |             |             |             | When it     |
|             |             |             |             | reaches the |
|             |             |             |             | base host,  |
|             |             |             |             | it resumes  |
|             |             |             |             | its forward |
|             |             |             |             | movement,   |
|             |             |             |             | but with a  |
|             |             |             |             | different   |
|             |             |             |             | random      |
|             |             |             |             | path.       |
+-------------+-------------+-------------+-------------+-------------+
| MOVEPACE    | -m          | 2           | 1:86400     | This number |
|             |             |             |             | determines  |
|             |             |             |             | how many    |
|             |             |             |             | times per   |
|             |             |             |             | day a       |
|             |             |             |             | partition   |
|             |             |             |             | from a      |
|             |             |             |             | group       |
|             |             |             |             | changes its |
|             |             |             |             | content. By |
|             |             |             |             | default,    |
|             |             |             |             | the change  |
|             |             |             |             | happens at  |
|             |             |             |             | least once  |
|             |             |             |             | a day.      |
|             |             |             |             | Cryptomove  |
|             |             |             |             | calculates  |
|             |             |             |             | the time in |
|             |             |             |             | seconds     |
|             |             |             |             | until next  |
|             |             |             |             | move for    |
|             |             |             |             | each        |
|             |             |             |             | partition   |
|             |             |             |             | independent |
|             |             |             |             | ly,         |
|             |             |             |             | as a random |
|             |             |             |             | number      |
|             |             |             |             | equally     |
|             |             |             |             | distributed |
|             |             |             |             | between 1   |
|             |             |             |             | and 2*M,    |
|             |             |             |             | where M =   |
|             |             |             |             | 86400/MOVEP |
|             |             |             |             | ACE.        |
+-------------+-------------+-------------+-------------+-------------+
| REMOTENAME  | -r          | -           | -           | When the    |
|             |             |             |             | group is    |
|             |             |             |             | assigned a  |
|             |             |             |             | remote      |
|             |             |             |             | name, that  |
|             |             |             |             | name        |
|             |             |             |             | becomes a   |
|             |             |             |             | part of the |
|             |             |             |             | CryptoMove  |
|             |             |             |             | namespace.  |
|             |             |             |             | Then it is  |
|             |             |             |             | possible to |
|             |             |             |             | control     |
|             |             |             |             | that group  |
|             |             |             |             | from the    |
|             |             |             |             | CryptoMove  |
|             |             |             |             | data store  |
|             |             |             |             | from any    |
|             |             |             |             | client      |
|             |             |             |             | computer.   |
+-------------+-------------+-------------+-------------+-------------+
| SPLITCOUNT  | -s          | 1           | 1:16        | This number |
|             |             |             |             | determines  |
|             |             |             |             | how many    |
|             |             |             |             | traveling   |
|             |             |             |             | tokens      |
|             |             |             |             | constitute  |
|             |             |             |             | a group     |
|             |             |             |             | partition.  |
|             |             |             |             | Tokens from |
|             |             |             |             | the same    |
|             |             |             |             | partition   |
|             |             |             |             | tend to     |
|             |             |             |             | travel      |
|             |             |             |             | distinct    |
|             |             |             |             | hosts.      |
|             |             |             |             | Therefore,  |
|             |             |             |             | more tokens |
|             |             |             |             | cause the   |
|             |             |             |             | partitions  |
|             |             |             |             | to have     |
|             |             |             |             | more hosts. |
+-------------+-------------+-------------+-------------+-------------+
| EXPIRATION  | -x          | 525600000   | 0:525600000 | The period  |
|             |             |             |             | of time in  |
|             |             |             |             | minutes     |
|             |             |             |             | after which |
|             |             |             |             | the group   |
|             |             |             |             | is          |
|             |             |             |             | automatical |
|             |             |             |             | ly          |
|             |             |             |             | dissolved   |
|             |             |             |             | from the    |
|             |             |             |             | data store. |
|             |             |             |             | By default, |
|             |             |             |             | it expires  |
|             |             |             |             | in one      |
|             |             |             |             | thousand    |
|             |             |             |             | years.      |
+-------------+-------------+-------------+-------------+-------------+

Parameters in Commands to Control Data Redundancy Group
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following table explains CryptoMove client parameters and flags that
set those parameters in the commands to control redundancy group gchk,
gdel, glog. Default indicates the value set to the parameter upon client
startup. Interval shows allowed values.

+-------------+-------------+-------------+-------------+-------------+
| **Parameter | **Flag**    | **Default** | **Interval* | **Explanati |
| **          |             |             | *           | on**        |
+=============+=============+=============+=============+=============+
| REMOTENAME  | -r          | -           | -           | Specifies   |
|             |             |             |             | remote name |
|             |             |             |             | if          |
|             |             |             |             | previously  |
|             |             |             |             | assigned to |
|             |             |             |             | the group.  |
+-------------+-------------+-------------+-------------+-------------+
| TIMEOUT     | -t          | 30          | 1:unlimited | The number  |
|             |             |             |             | of seconds  |
|             |             |             |             | the client  |
|             |             |             |             | waits until |
|             |             |             |             | CryptoMove  |
|             |             |             |             | servers     |
|             |             |             |             | complete    |
|             |             |             |             | the         |
|             |             |             |             | operation.  |
+-------------+-------------+-------------+-------------+-------------+
| VERSION     | -v          | last        | 1:last      | When client |
|             |             |             |             | creates a   |
|             |             |             |             | group the   |
|             |             |             |             | first time, |
|             |             |             |             | it assigns  |
|             |             |             |             | version 1   |
|             |             |             |             | to the      |
|             |             |             |             | saved file. |
|             |             |             |             | Creation of |
|             |             |             |             | a group     |
|             |             |             |             | with the    |
|             |             |             |             | same name   |
|             |             |             |             | increments  |
|             |             |             |             | the group   |
|             |             |             |             | version.    |
|             |             |             |             | When        |
|             |             |             |             | controlling |
|             |             |             |             | the group,  |
|             |             |             |             | by default  |
|             |             |             |             | the client  |
|             |             |             |             | works with  |
|             |             |             |             | its last    |
|             |             |             |             | version.    |
|             |             |             |             | You may     |
|             |             |             |             | address a   |
|             |             |             |             | specific    |
|             |             |             |             | version of  |
|             |             |             |             | the group   |
|             |             |             |             | by setting  |
|             |             |             |             | the version |
|             |             |             |             | number with |
|             |             |             |             | this flag.  |
+-------------+-------------+-------------+-------------+-------------+

Client Options and Modes
~~~~~~~~~~~~~~~~~~~~~~~~

Client modes are viewed or changed each with a special command as
follows:

   auditname [AUDITNAME \| NONE]

   client [VERBOSE \| SILENT]

   ctmo [TIMEOUT]

   {e \| echo} [on \| off]

   {f \| force} [on \| off]

   {k \| key} [{on \| off}]

   {l \| link} [on \| off]

   meml [SIZE]

   {t \| trust} [on \| off]

Commands trust, link, force, and ctmo without arguments print the
current value of the respective mode. Otherwise, these commands with an
argument set the mode to the value of the argument.

The following table explains all client options and modes:

+-----------------+-----------------+-----------------+-----------------+
| **Command**     | **Default**     | **Values**      | **Explanation** |
+=================+=================+=================+=================+
| auditname       | NONE            | character       | When AUDITNAME  |
|                 |                 | string          | is specified,   |
|                 |                 |                 | then it is used |
|                 |                 |                 | as a tag for    |
|                 |                 |                 | every the       |
|                 |                 |                 | record logged   |
|                 |                 |                 | by the server   |
|                 |                 |                 | for every       |
|                 |                 |                 | clients’        |
|                 |                 |                 | command. When   |
|                 |                 |                 | set to NONE, no |
|                 |                 |                 | auditing record |
|                 |                 |                 | of that client  |
|                 |                 |                 | is tagged. When |
|                 |                 |                 | no parameter is |
|                 |                 |                 | set, then       |
|                 |                 |                 | command         |
|                 |                 |                 | auditname       |
|                 |                 |                 | displays        |
|                 |                 |                 | current         |
|                 |                 |                 | AUDITNAME.      |
+-----------------+-----------------+-----------------+-----------------+
| client          | VERBOSE         | VERBOSE         | By default, the |
|                 |                 |                 | client reports  |
|                 |                 | SILENT          | the phases of   |
|                 |                 |                 | its execution   |
|                 |                 |                 | for all         |
|                 |                 |                 | save/restore    |
|                 |                 |                 | and batch       |
|                 |                 |                 | commands. With  |
|                 |                 |                 | this option on, |
|                 |                 |                 | it executes     |
|                 |                 |                 | commands in     |
|                 |                 |                 | silence.        |
+-----------------+-----------------+-----------------+-----------------+
| ctmo            | TIMEOUT=360     | 1:unlimited     | The number of   |
|                 |                 |                 | seconds the     |
|                 |                 |                 | client waits    |
|                 |                 |                 | until local     |
|                 |                 |                 | CryptoMove      |
|                 |                 |                 | server          |
|                 |                 |                 | acknowledges it |
|                 |                 |                 | has received    |
|                 |                 |                 | the client      |
|                 |                 |                 | command.        |
+-----------------+-----------------+-----------------+-----------------+
| echo            | off             | off/on          | By default,     |
|                 |                 |                 | echo is off --  |
|                 |                 |                 | when typing a   |
|                 |                 |                 | key on a client |
|                 |                 |                 | prompt, the     |
|                 |                 |                 | client does not |
|                 |                 |                 | show the typed  |
|                 |                 |                 | key on the      |
|                 |                 |                 | screen. When    |
|                 |                 |                 | echo is on, the |
|                 |                 |                 | client prints   |
|                 |                 |                 | the typed key   |
|                 |                 |                 | on the screen.  |
|                 |                 |                 | command echo    |
|                 |                 |                 | without         |
|                 |                 |                 | argument shows  |
|                 |                 |                 | current echo    |
|                 |                 |                 | mode – on or    |
|                 |                 |                 | off.            |
+-----------------+-----------------+-----------------+-----------------+
| force           | off             | off/on          | By default,     |
|                 |                 |                 | when restoring  |
|                 |                 |                 | a file, the     |
|                 |                 |                 | client aborts   |
|                 |                 |                 | the operation   |
|                 |                 |                 | if the user     |
|                 |                 |                 | directory       |
|                 |                 |                 | contains a file |
|                 |                 |                 | with the same   |
|                 |                 |                 | name as the     |
|                 |                 |                 | restored file.  |
|                 |                 |                 | When this       |
|                 |                 |                 | option is on,   |
|                 |                 |                 | the client      |
|                 |                 |                 | replaces the    |
|                 |                 |                 | current file in |
|                 |                 |                 | the user        |
|                 |                 |                 | directory with  |
|                 |                 |                 | the data        |
|                 |                 |                 | restored from   |
|                 |                 |                 | CryptoMove data |
|                 |                 |                 | store.          |
+-----------------+-----------------+-----------------+-----------------+
| key             | off             | off/on          | By default,     |
|                 |                 |                 | this mode is    |
|                 |                 |                 | off, which      |
|                 |                 |                 | means that the  |
|                 |                 |                 | client uses     |
|                 |                 |                 | only *data key* |
|                 |                 |                 | when            |
|                 |                 |                 | saving/restorin |
|                 |                 |                 | g               |
|                 |                 |                 | user data. If   |
|                 |                 |                 | you switch this |
|                 |                 |                 | option to on,   |
|                 |                 |                 | the client will |
|                 |                 |                 | first ask you   |
|                 |                 |                 | to enter the    |
|                 |                 |                 | *user key*.     |
|                 |                 |                 | After that,     |
|                 |                 |                 | until you       |
|                 |                 |                 | switch the key  |
|                 |                 |                 | mode to off,    |
|                 |                 |                 | the client will |
|                 |                 |                 | always combine  |
|                 |                 |                 | the user key    |
|                 |                 |                 | and the data    |
|                 |                 |                 | key into a new  |
|                 |                 |                 | key, which it   |
|                 |                 |                 | uses to         |
|                 |                 |                 | generate a      |
|                 |                 |                 | symmetric key   |
|                 |                 |                 | for all         |
|                 |                 |                 | save/restore    |
|                 |                 |                 | operations.     |
|                 |                 |                 | Using secret    |
|                 |                 |                 | user keys       |
|                 |                 |                 | prevents system |
|                 |                 |                 | owners from     |
|                 |                 |                 | breaching the   |
|                 |                 |                 | data saved by   |
|                 |                 |                 | data users.     |
+-----------------+-----------------+-----------------+-----------------+
| link            | on              | off/on          | By default,     |
|                 |                 |                 | when saving     |
|                 |                 |                 | file X, the     |
|                 |                 |                 | client creates  |
|                 |                 |                 | a soft link     |
|                 |                 |                 | named .X.cln,   |
|                 |                 |                 | indicating that |
|                 |                 |                 | X has been      |
|                 |                 |                 | saved in the    |
|                 |                 |                 | data store. If  |
|                 |                 |                 | this option is  |
|                 |                 |                 | off, the link   |
|                 |                 |                 | is not created. |
+-----------------+-----------------+-----------------+-----------------+
| meml            | SIZE=4194304    | 1B:4GB          | The size in     |
|                 |                 |                 | bytes of locked |
|                 |                 |                 | client memory   |
|                 |                 |                 | to hold data    |
|                 |                 |                 | entered from    |
|                 |                 |                 | standard input  |
|                 |                 |                 | with putl       |
|                 |                 |                 | command.        |
+-----------------+-----------------+-----------------+-----------------+
| trust           | off             | off/on          | By default, the |
|                 |                 |                 | client asks the |
|                 |                 |                 | user to enter   |
|                 |                 |                 | the data key    |
|                 |                 |                 | for each        |
|                 |                 |                 | save/restore    |
|                 |                 |                 | operation. When |
|                 |                 |                 | this option is  |
|                 |                 |                 | on, the client  |
|                 |                 |                 | asks for the    |
|                 |                 |                 | key just once   |
|                 |                 |                 | and then keeps  |
|                 |                 |                 | using the       |
|                 |                 |                 | entered key for |
|                 |                 |                 | all subsequent  |
|                 |                 |                 | operations,     |
|                 |                 |                 | until the trust |
|                 |                 |                 | mode is         |
|                 |                 |                 | switched back   |
|                 |                 |                 | to off. The     |
|                 |                 |                 | client uses the |
|                 |                 |                 | key to generate |
|                 |                 |                 | a symmetric key |
|                 |                 |                 | to encrypt and  |
|                 |                 |                 | decrypt the     |
|                 |                 |                 | data.           |
+-----------------+-----------------+-----------------+-----------------+

Server Options and Commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~

When running CryptoMove client as a system owner, these commands show or
alter options on the local CryptoMove server:

audit [on \| off]

apicount

breach [on \| off]

count [PERIOD]

edge

getadm

kill [on \| off]

power [POWERVALUE]

progress [on \| off]

rshell [on \| off]

state

stmo [TIMEOUT]

timeshift [SHIFT]

trnc [PERIOD]

watch [PERIOD]

watchpids [pid ...]

All commands without argument dump the respective current option’s
value.

The following table explains each option:

+-----------------+-----------------+-----------------+-----------------+
| **Command**     | **Default**     | **Values**      | **Explanation** |
+=================+=================+=================+=================+
| audit           | on              | off/on          | Triggers server |
|                 |                 |                 | syslog auditing |
|                 |                 |                 | facility off or |
|                 |                 |                 | on.             |
+-----------------+-----------------+-----------------+-----------------+
| apicount        | -               | 0-100000        | Count of API    |
|                 |                 |                 | clients         |
|                 |                 |                 | currently       |
|                 |                 |                 | connected to    |
|                 |                 |                 | the server.     |
+-----------------+-----------------+-----------------+-----------------+
| breach          | off             | off/on          | If this option  |
|                 |                 |                 | is on, then the |
|                 |                 |                 | server declares |
|                 |                 |                 | its state as    |
|                 |                 |                 | BREACHED when   |
|                 |                 |                 | it detects an   |
|                 |                 |                 | unauthorized    |
|                 |                 |                 | process         |
|                 |                 |                 | accessing its   |
|                 |                 |                 | data store.     |
|                 |                 |                 | When off, the   |
|                 |                 |                 | server does not |
|                 |                 |                 | declare its     |
|                 |                 |                 | state BREACHED. |
+-----------------+-----------------+-----------------+-----------------+
| count           | none            | O:unlimited     | When issued     |
|                 |                 |                 | without         |
|                 |                 |                 | arguments, this |
|                 |                 |                 | command shows   |
|                 |                 |                 | the number of   |
|                 |                 |                 | parts the local |
|                 |                 |                 | server had      |
|                 |                 |                 | moved after its |
|                 |                 |                 | startup. When   |
|                 |                 |                 | PERIOD is a     |
|                 |                 |                 | positive        |
|                 |                 |                 | number, the     |
|                 |                 |                 | local server    |
|                 |                 |                 | starts dumping  |
|                 |                 |                 | on its standard |
|                 |                 |                 | output the      |
|                 |                 |                 | counts of       |
|                 |                 |                 | parts, with the |
|                 |                 |                 | specified       |
|                 |                 |                 | PERIOD in       |
|                 |                 |                 | seconds, using  |
|                 |                 |                 | format          |
|                 |                 |                 |                 |
|                 |                 |                 | count:PRESENT:I |
|                 |                 |                 | N:OUT:DELETED:H |
|                 |                 |                 | OSTNAME         |
|                 |                 |                 |                 |
|                 |                 |                 | where count is  |
|                 |                 |                 | a keyword,      |
|                 |                 |                 | PRESENT is the  |
|                 |                 |                 | number of parts |
|                 |                 |                 | on the server,  |
|                 |                 |                 | IN is the       |
|                 |                 |                 | number of parts |
|                 |                 |                 | moved onto this |
|                 |                 |                 | server, OUT is  |
|                 |                 |                 | the number of   |
|                 |                 |                 | parts moved     |
|                 |                 |                 | away from the   |
|                 |                 |                 | server, DELETED |
|                 |                 |                 | is the number   |
|                 |                 |                 | of parts        |
|                 |                 |                 | deleted from    |
|                 |                 |                 | the server, and |
|                 |                 |                 | HOSTNAME is the |
|                 |                 |                 | name of the     |
|                 |                 |                 | local host.     |
|                 |                 |                 |                 |
|                 |                 |                 | When PERIOD is  |
|                 |                 |                 | 0, then         |
|                 |                 |                 | periodic count  |
|                 |                 |                 | dump stops.     |
+-----------------+-----------------+-----------------+-----------------+
| edge            | none            | true/false      | When CryptoMove |
|                 |                 |                 | server starts   |
|                 |                 |                 | with command ss |
|                 |                 |                 | (server start), |
|                 |                 |                 | other servers   |
|                 |                 |                 | can see its     |
|                 |                 |                 | state: if the   |
|                 |                 |                 | state is        |
|                 |                 |                 | online, they    |
|                 |                 |                 | can transfer    |
|                 |                 |                 | data parts to   |
|                 |                 |                 | that server.    |
|                 |                 |                 |                 |
|                 |                 |                 | When CryptoMove |
|                 |                 |                 | server starts   |
|                 |                 |                 | with command se |
|                 |                 |                 | (server edge),  |
|                 |                 |                 | other servers   |
|                 |                 |                 | cannot see it   |
|                 |                 |                 | online. As a    |
|                 |                 |                 | result, they    |
|                 |                 |                 | never forward   |
|                 |                 |                 | to the edge     |
|                 |                 |                 | server data     |
|                 |                 |                 | parts.          |
|                 |                 |                 |                 |
|                 |                 |                 | The edge server |
|                 |                 |                 | can still       |
|                 |                 |                 | transfer the    |
|                 |                 |                 | saved parts to  |
|                 |                 |                 | other servers   |
|                 |                 |                 | because of the  |
|                 |                 |                 | saving data. In |
|                 |                 |                 | addition, it    |
|                 |                 |                 | can receive     |
|                 |                 |                 | data parts when |
|                 |                 |                 | restoring data  |
|                 |                 |                 | on the host     |
|                 |                 |                 | where it is     |
|                 |                 |                 | running. Edge   |
|                 |                 |                 | server shall    |
|                 |                 |                 | run on every    |
|                 |                 |                 | host which is   |
|                 |                 |                 | not a part of   |
|                 |                 |                 | the CryptoMove  |
|                 |                 |                 | data store, but |
|                 |                 |                 | from which      |
|                 |                 |                 | users want to   |
|                 |                 |                 | save/restore    |
|                 |                 |                 | their data.     |
|                 |                 |                 |                 |
|                 |                 |                 | When saving     |
|                 |                 |                 | data, the edge  |
|                 |                 |                 | server always   |
|                 |                 |                 | forwards saved  |
|                 |                 |                 | parts to the    |
|                 |                 |                 | bases it knows. |
|                 |                 |                 | When restoring  |
|                 |                 |                 | data, the edge  |
|                 |                 |                 | server receives |
|                 |                 |                 | data parts from |
|                 |                 |                 | the known       |
|                 |                 |                 | bases.          |
+-----------------+-----------------+-----------------+-----------------+
| getadm          | -               | -               | Dumps the list  |
|                 |                 |                 | of system keys’ |
|                 |                 |                 | ids.            |
+-----------------+-----------------+-----------------+-----------------+
| kill            | off             | off/on          | If this option  |
|                 |                 |                 | is on, then the |
|                 |                 |                 | server kills an |
|                 |                 |                 | unauthorized    |
|                 |                 |                 | process         |
|                 |                 |                 | accessing       |
|                 |                 |                 | CryptoMove data |
|                 |                 |                 | store, unless   |
|                 |                 |                 | that process’s  |
|                 |                 |                 | pid has been    |
|                 |                 |                 | specified with  |
|                 |                 |                 | watchpids. If   |
|                 |                 |                 | this option is  |
|                 |                 |                 | off, the        |
|                 |                 |                 | process is not  |
|                 |                 |                 | killed.         |
+-----------------+-----------------+-----------------+-----------------+
| power           | 20              | 1:100           | This value      |
|                 |                 |                 | controls the    |
|                 |                 |                 | portion of the  |
|                 |                 |                 | parts moving    |
|                 |                 |                 | between the     |
|                 |                 |                 | servers, as     |
|                 |                 |                 | well as the     |
|                 |                 |                 | pace of         |
|                 |                 |                 | movement.       |
|                 |                 |                 |                 |
|                 |                 |                 | -  When POWER   |
|                 |                 |                 |    is 1, then   |
|                 |                 |                 |    no parts     |
|                 |                 |                 |    from the     |
|                 |                 |                 |    data store   |
|                 |                 |                 |    on the given |
|                 |                 |                 |    server move  |
|                 |                 |                 |    – they       |
|                 |                 |                 |    remain       |
|                 |                 |                 |    intact in    |
|                 |                 |                 |    the store.   |
|                 |                 |                 |                 |
|                 |                 |                 | -  When POWER   |
|                 |                 |                 |    is 100, then |
|                 |                 |                 |    all parts    |
|                 |                 |                 |    from the     |
|                 |                 |                 |    CryptoMove   |
|                 |                 |                 |    data store   |
|                 |                 |                 |    move,        |
|                 |                 |                 |    according to |
|                 |                 |                 |    the movement |
|                 |                 |                 |    parameters   |
|                 |                 |                 |    set when the |
|                 |                 |                 |    parts where  |
|                 |                 |                 |    saved.       |
|                 |                 |                 |                 |
|                 |                 |                 | -  If POWER is  |
|                 |                 |                 |    between 1    |
|                 |                 |                 |    and 100,     |
|                 |                 |                 |    then some    |
|                 |                 |                 |    parts move   |
|                 |                 |                 |    and some     |
|                 |                 |                 |    stay intact. |
|                 |                 |                 |    The higher   |
|                 |                 |                 |    the number,  |
|                 |                 |                 |    the more     |
|                 |                 |                 |    parts move   |
|                 |                 |                 |    and more     |
|                 |                 |                 |    frequently.  |
|                 |                 |                 |                 |
|                 |                 |                 | -  The further  |
|                 |                 |                 |    the part is  |
|                 |                 |                 |    away from    |
|                 |                 |                 |    its base --  |
|                 |                 |                 |    the faster   |
|                 |                 |                 |    is its pace  |
|                 |                 |                 |    of movement. |
+-----------------+-----------------+-----------------+-----------------+
| progress        | off             | off/on          | In the mode     |
|                 |                 |                 | progress on the |
|                 |                 |                 | local server    |
|                 |                 |                 | reports on its  |
|                 |                 |                 | standard output |
|                 |                 |                 | the progress    |
|                 |                 |                 | during data     |
|                 |                 |                 | retrieval from  |
|                 |                 |                 | the data store. |
|                 |                 |                 | Each part       |
|                 |                 |                 | processed       |
|                 |                 |                 | during          |
|                 |                 |                 | execution of    |
|                 |                 |                 | the commands    |
|                 |                 |                 | \*GET*, \*DEL*, |
|                 |                 |                 | \*CHK*, issued  |
|                 |                 |                 | from any local  |
|                 |                 |                 | client, causes  |
|                 |                 |                 | the server to   |
|                 |                 |                 | output a line   |
|                 |                 |                 | in the          |
|                 |                 |                 | following       |
|                 |                 |                 | format:         |
|                 |                 |                 |                 |
|                 |                 |                 | filepart:PID:NA |
|                 |                 |                 | ME:STATUS:COUNT |
|                 |                 |                 | :INDEX:HOSTNAME |
|                 |                 |                 |                 |
|                 |                 |                 | where the       |
|                 |                 |                 | components have |
|                 |                 |                 | the following   |
|                 |                 |                 | meaning:        |
|                 |                 |                 |                 |
|                 |                 |                 | -  Filepart:    |
|                 |                 |                 |    keyword      |
|                 |                 |                 |                 |
|                 |                 |                 | -  PID: process |
|                 |                 |                 |    id of the    |
|                 |                 |                 |    local client |
|                 |                 |                 |    that issued  |
|                 |                 |                 |    the data     |
|                 |                 |                 |    access       |
|                 |                 |                 |    command      |
|                 |                 |                 |                 |
|                 |                 |                 | -  NAME: name   |
|                 |                 |                 |    of data or   |
|                 |                 |                 |    file to      |
|                 |                 |                 |    which this   |
|                 |                 |                 |    report       |
|                 |                 |                 |    relates      |
|                 |                 |                 |                 |
|                 |                 |                 | -  STATUS: one  |
|                 |                 |                 |    of found,    |
|                 |                 |                 |    missed or    |
|                 |                 |                 |    deleted,     |
|                 |                 |                 |    according to |
|                 |                 |                 |    the effect   |
|                 |                 |                 |    of the       |
|                 |                 |                 |    client       |
|                 |                 |                 |    command on   |
|                 |                 |                 |    this part:   |
|                 |                 |                 |                 |
|                 |                 |                 |    -  Found:    |
|                 |                 |                 |       when the  |
|                 |                 |                 |       part has  |
|                 |                 |                 |       been      |
|                 |                 |                 |       found     |
|                 |                 |                 |                 |
|                 |                 |                 |    -  Missed:   |
|                 |                 |                 |       when the  |
|                 |                 |                 |       part has  |
|                 |                 |                 |       been      |
|                 |                 |                 |       missed    |
|                 |                 |                 |                 |
|                 |                 |                 |    -  Deleted:  |
|                 |                 |                 |       when the  |
|                 |                 |                 |       part has  |
|                 |                 |                 |       been      |
|                 |                 |                 |       deleted   |
|                 |                 |                 |                 |
|                 |                 |                 |    -  COUNT:    |
|                 |                 |                 |       total     |
|                 |                 |                 |       number of |
|                 |                 |                 |       data      |
|                 |                 |                 |       parts     |
|                 |                 |                 |       under the |
|                 |                 |                 |       operation |
|                 |                 |                 |                 |
|                 |                 |                 |    -  INDEX:    |
|                 |                 |                 |       index of  |
|                 |                 |                 |       the part  |
|                 |                 |                 |       this      |
|                 |                 |                 |       record    |
|                 |                 |                 |       describes |
|                 |                 |                 |                 |
|                 |                 |                 |    -  HOSTNAME: |
|                 |                 |                 |       name of   |
|                 |                 |                 |       the local |
|                 |                 |                 |       host      |
|                 |                 |                 |                 |
|                 |                 |                 | When in mode    |
|                 |                 |                 | progress off ,  |
|                 |                 |                 | the server does |
|                 |                 |                 | not report      |
|                 |                 |                 | progress.       |
|                 |                 |                 |                 |
|                 |                 |                 | Command         |
|                 |                 |                 | progress        |
|                 |                 |                 | without         |
|                 |                 |                 | arguments       |
|                 |                 |                 | displays the    |
|                 |                 |                 | current         |
|                 |                 |                 | progress option |
|                 |                 |                 | on or off.      |
+-----------------+-----------------+-----------------+-----------------+
| rshell          | off             | off/on          | When rshell     |
|                 |                 |                 | mode is off,    |
|                 |                 |                 | which is        |
|                 |                 |                 | default, then   |
|                 |                 |                 | the server will |
|                 |                 |                 | refuse to       |
|                 |                 |                 | execute any     |
|                 |                 |                 | local or remote |
|                 |                 |                 | shell command   |
|                 |                 |                 | launched with   |
|                 |                 |                 | the operator    |
|                 |                 |                 | !!. When rshell |
|                 |                 |                 | is on, such     |
|                 |                 |                 | shell command   |
|                 |                 |                 | will be         |
|                 |                 |                 | executed.       |
+-----------------+-----------------+-----------------+-----------------+
| state           | none            | ONLINE OFFLINE  | The state       |
|                 |                 | FAULTED HOSTILE | reflects the    |
|                 |                 | UNKNOWN         | current         |
|                 |                 | UNREACHED       | operational     |
|                 |                 | BREACHED        | abilities of    |
|                 |                 | EXITING         | the CryptoMove  |
|                 |                 | ENTERING DOWN   | server. See     |
|                 |                 |                 | section         |
|                 |                 |                 | 4.3.12.1 for    |
|                 |                 |                 | the             |
|                 |                 |                 | explanations of |
|                 |                 |                 | each state.     |
+-----------------+-----------------+-----------------+-----------------+
| stmo            | 360             | 1:unlimited     | This value      |
|                 |                 |                 | places a limit  |
|                 |                 |                 | on the time the |
|                 |                 |                 | local server    |
|                 |                 |                 | waits for a     |
|                 |                 |                 | response after  |
|                 |                 |                 | sending a       |
|                 |                 |                 | request to      |
|                 |                 |                 | another server. |
+-----------------+-----------------+-----------------+-----------------+
| sync            | -               | -               | This is         |
|                 |                 |                 | actually a      |
|                 |                 |                 | command. It     |
|                 |                 |                 | writes all      |
|                 |                 |                 | buffered data   |
|                 |                 |                 | from the        |
|                 |                 |                 | current client  |
|                 |                 |                 | and from the    |
|                 |                 |                 | local server to |
|                 |                 |                 | disk.           |
+-----------------+-----------------+-----------------+-----------------+
| timeshift       | 120             | 0:unlimited     | This value      |
|                 |                 |                 | defines the     |
|                 |                 |                 | maximum allowed |
|                 |                 |                 | time            |
|                 |                 |                 | difference, in  |
|                 |                 |                 | seconds,        |
|                 |                 |                 | between the     |
|                 |                 |                 | CryptoMove      |
|                 |                 |                 | internal clock  |
|                 |                 |                 | and OS real     |
|                 |                 |                 | time clock.     |
|                 |                 |                 | When the        |
|                 |                 |                 | difference      |
|                 |                 |                 | exceeds the     |
|                 |                 |                 | specified       |
|                 |                 |                 | value, the      |
|                 |                 |                 | server enters   |
|                 |                 |                 | breached state. |
|                 |                 |                 | When TIMESHIFT  |
|                 |                 |                 | is 0, the       |
|                 |                 |                 | server          |
|                 |                 |                 | immediately     |
|                 |                 |                 | transitions to  |
|                 |                 |                 | the BREACHED    |
|                 |                 |                 | state,          |
|                 |                 |                 | regardless of   |
|                 |                 |                 | the actual      |
|                 |                 |                 | difference      |
|                 |                 |                 | between the     |
|                 |                 |                 | server and      |
|                 |                 |                 | system clocks.  |
+-----------------+-----------------+-----------------+-----------------+
| trnc            | none            | 0:1000 years    | This command    |
|                 |                 |                 | truncates       |
|                 |                 |                 | server trace    |
|                 |                 |                 | logs to zero    |
|                 |                 |                 | size. The       |
|                 |                 |                 | server          |
|                 |                 |                 | continues       |
|                 |                 |                 | filling trace   |
|                 |                 |                 | log files from  |
|                 |                 |                 | their           |
|                 |                 |                 | beginnings. If  |
|                 |                 |                 | PERIOD is 0,    |
|                 |                 |                 | then logs are   |
|                 |                 |                 | truncated       |
|                 |                 |                 | immediately. If |
|                 |                 |                 | PERIOD is a     |
|                 |                 |                 | positive        |
|                 |                 |                 | number, the     |
|                 |                 |                 | server will be  |
|                 |                 |                 | truncating the  |
|                 |                 |                 | log every       |
|                 |                 |                 | specified       |
|                 |                 |                 | number of       |
|                 |                 |                 | minutes. If     |
|                 |                 |                 | PERIOD is       |
|                 |                 |                 | absent, the     |
|                 |                 |                 | client displays |
|                 |                 |                 | how many        |
|                 |                 |                 | minutes are     |
|                 |                 |                 | left until the  |
|                 |                 |                 | next            |
|                 |                 |                 | truncation.     |
+-----------------+-----------------+-----------------+-----------------+
| watch           | 0               | 0:120           | When the watch  |
|                 |                 |                 | PERIOD is a     |
|                 |                 |                 | positive N,     |
|                 |                 |                 | then the server |
|                 |                 |                 | periodically    |
|                 |                 |                 | and randomly,   |
|                 |                 |                 | but at least    |
|                 |                 |                 | every N         |
|                 |                 |                 | seconds, polls  |
|                 |                 |                 | all open file   |
|                 |                 |                 | descriptors for |
|                 |                 |                 | all running     |
|                 |                 |                 | processes,      |
|                 |                 |                 | except its own. |
|                 |                 |                 | If it detects   |
|                 |                 |                 | that any        |
|                 |                 |                 | process, except |
|                 |                 |                 | those specified |
|                 |                 |                 | in the          |
|                 |                 |                 | watchpids list, |
|                 |                 |                 | opens a file in |
|                 |                 |                 | its data store, |
|                 |                 |                 | it considers    |
|                 |                 |                 | that process to |
|                 |                 |                 | be a threat to  |
|                 |                 |                 | the store, and  |
|                 |                 |                 | performs the    |
|                 |                 |                 | following       |
|                 |                 |                 | actions:        |
|                 |                 |                 |                 |
|                 |                 |                 | -  The server   |
|                 |                 |                 |    schedules    |
|                 |                 |                 |    immediate    |
|                 |                 |                 |    execution of |
|                 |                 |                 |    all          |
|                 |                 |                 |    outstanding  |
|                 |                 |                 |    server       |
|                 |                 |                 |    requests,    |
|                 |                 |                 |    including    |
|                 |                 |                 |    data         |
|                 |                 |                 |    movement and |
|                 |                 |                 |    mutation,    |
|                 |                 |                 |    pulse        |
|                 |                 |                 |    propagation, |
|                 |                 |                 |    data user    |
|                 |                 |                 |    requests,    |
|                 |                 |                 |    etc.         |
|                 |                 |                 |                 |
|                 |                 |                 | -  The server   |
|                 |                 |                 |    sets its     |
|                 |                 |                 |    state to     |
|                 |                 |                 |    BREACHED and |
|                 |                 |                 |    makes sure   |
|                 |                 |                 |    that all     |
|                 |                 |                 |    online hosts |
|                 |                 |                 |    immediately  |
|                 |                 |                 |    see its      |
|                 |                 |                 |    state as     |
|                 |                 |                 |    BREACHED     |
|                 |                 |                 |    too.         |
|                 |                 |                 |                 |
|                 |                 |                 | -  The server   |
|                 |                 |                 |    sends signal |
|                 |                 |                 |    SIGKILL to   |
|                 |                 |                 |    the process  |
|                 |                 |                 |    that had     |
|                 |                 |                 |    opened a     |
|                 |                 |                 |    file in the  |
|                 |                 |                 |    CryptoMove   |
|                 |                 |                 |    data store.  |
|                 |                 |                 |                 |
|                 |                 |                 | After the host  |
|                 |                 |                 | ends up in the  |
|                 |                 |                 | BREACHED state, |
|                 |                 |                 | the only way    |
|                 |                 |                 | out of that     |
|                 |                 |                 | state is via    |
|                 |                 |                 | the cluster     |
|                 |                 |                 | command issued  |
|                 |                 |                 | on that host:   |
|                 |                 |                 |                 |
|                 |                 |                 |    cluster      |
|                 |                 |                 |    cluster_name |
|                 |                 |                 |    --HOST       |
|                 |                 |                 |    host_name    |
|                 |                 |                 |    --START      |
|                 |                 |                 |                 |
|                 |                 |                 | When the watch  |
|                 |                 |                 | is 0, which is  |
|                 |                 |                 | the default,    |
|                 |                 |                 | CryptoMove      |
|                 |                 |                 | server allows   |
|                 |                 |                 | any process to  |
|                 |                 |                 | open any file   |
|                 |                 |                 | in its data     |
|                 |                 |                 | store.          |
|                 |                 |                 |                 |
|                 |                 |                 | Leaving watch   |
|                 |                 |                 | mode 0 by the   |
|                 |                 |                 | system owner    |
|                 |                 |                 | may be useful   |
|                 |                 |                 | when performing |
|                 |                 |                 | backup/restore  |
|                 |                 |                 | operations, or  |
|                 |                 |                 | any other       |
|                 |                 |                 | legitimate      |
|                 |                 |                 | service on the  |
|                 |                 |                 | data store.     |
+-----------------+-----------------+-----------------+-----------------+
| watchpids       | 0               | one or more     | When one or     |
|                 |                 | process         | more process    |
|                 |                 | identifiers     | identifiers,    |
|                 |                 |                 | separated by    |
|                 |                 |                 | spaces, are set |
|                 |                 |                 | in this         |
|                 |                 |                 | command, then   |
|                 |                 |                 | CryptoMove      |
|                 |                 |                 | server allows   |
|                 |                 |                 | for processes   |
|                 |                 |                 | with these      |
|                 |                 |                 | identifiers to  |
|                 |                 |                 | access its data |
|                 |                 |                 | store. When     |
|                 |                 |                 | displayed list  |
|                 |                 |                 | consists of a   |
|                 |                 |                 | single id equal |
|                 |                 |                 | to 0, then      |
|                 |                 |                 | access to       |
|                 |                 |                 | CryptoMove data |
|                 |                 |                 | store by        |
|                 |                 |                 | foreign         |
|                 |                 |                 | processes is    |
|                 |                 |                 | prohibited. To  |
|                 |                 |                 | prohibit any    |
|                 |                 |                 | process other   |
|                 |                 |                 | than CryptoMove |
|                 |                 |                 | server from     |
|                 |                 |                 | accessing its   |
|                 |                 |                 | data store, set |
|                 |                 |                 | the list to be  |
|                 |                 |                 | of a single     |
|                 |                 |                 | process         |
|                 |                 |                 | identifier 0.   |
+-----------------+-----------------+-----------------+-----------------+

Data Handling Commands
~~~~~~~~~~~~~~~~~~~~~~

Submitting Keys
^^^^^^^^^^^^^^^

   When saving multiple files, client uses data keys as follows:

-  When trust mode is on, it uses the data key entered previously for
   the trust on command.

-  When trust mode is off, it interactively prompts for the data key for
   each file.

Client Directory Management
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Cryptomove offers a number of commands that save and restore
(recursively) entire directories from the user space. For example,
command dput saves all files from a given directory, while command dget
restores all files into a given directory saved previously with command
dput. The difference between client directory commands like dput, dget
and commands like mput and mget is that the former maintain a listing of
all the files saved. CryptoMove server uses that listing in order to
restore exactly the same set of files that has been saved, by specifying
the directory name in the restoration command. However, the latter
maintains no such listing, so it is the user’s responsibility to
remember the names of the saved files.

The commands that manage client directories accept the same set of
parameters as individual file or data commands. CryptoMove server
applies the command’s arguments to each file from a client directory;
they are described in the following subsections, together with the
commands that handle individual files.

File Names in Client Commands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Most commands operate on files which names are specified like this:

[FILENAME... \|

@FILELIST \|

!SHELLCOMMAND]

The client determines the file or files to operate upon as follows:

-  If one or more FILENAME is set, then it saves all files with the
   given names. Each FILENAME can be either a file name, a directory
   name, or a regular expression expanding to any number of files.

-  If @FILELIST is specified, then CryptoMove client saves files listed
   in file FILELIST. Each line in that file must contain no more than
   one exact file name. Empty lines are OK.

-  If !SHELL_COMMAND is specified, then CryptoMove client executes the
   given shell command, which shall output a list of files on its
   standard output device one exact file name per line. The client saves
   all files from that list. The client must be a system owner to be
   able to execute a shell command.

-  The client always treats symbol ‘/’ in a file name as a directory
   separator, not a part of the file name.

-  In command putl, FILENAME is not related to any file in the file
   system but becomes the name of the data saved from standard input.
   That name shall be used later when restoring or deleting the data
   saved with this name. The end of input data is indicated by CTRL/D.

-  Files saved in the data store can be restored back into the files
   with the same FILENAMEs.

-  Data saved from the standard input acquires the fictitious name
   FILENAME in the data store; it can be restored back onto standard
   output with the same name FILENAME. You can delete or clear that data
   using the same name FILENAME.

Commands put[lr], [m]put,[m] mov, mod[l], com[l],dput[r], dmov[r] -- Saving Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commands put, mput, mov, and mmov save files from the user store into
CryptoMove store:

   {put \| mput \| mov \| mmov \| putl \|

   dput \| dputr \| dmov \| dmovr \|

   putr \| mod \| modl \| com \| coml}

   [{-a \| --armslength} ARMSLENGTH ]

   [{-b \| --bitscatter} SCATTERLENGTH]

   [{-co \| --compress} STRENGTH ]

   [{-cl \| --clean} CLEANCOMMIT ]

   [{-c \| --copy} COPYCOUNT ]

   [{-cu \| --cur} MODOFFSET ]

   [{-d \| --depth} DATADEPTH ]

   [{-e \| --entropy} ENTROPY ]

   [{-ec \| --encryptclient} CLIENTENCRYPT]

   [{-en \| --end} MODOFFSET ]

   [{-es \| --encryptserver} SERVERENCRYPT]

   [{-ex \| --expire} EXPIRATION ]

   [{-f \| --fill} FILLGROUP ]

   [{-g \| --group} GROUPNAME ]

   [{-in \| --in} MODNAME ]

   [{-m \| --move} MOVEPACE ]

   [{-l \| --log} LOGSAVE ]

   [{-n \| --number} NUMBER ]

   [{-p \| --pulse} PULSEBEAT ]

   [{-pe \| --permutation PERMUTATION ]

   [{-r \| --remote} REMOTENAME ]

   [{-s \| --split} SPLITCOUNT ]

   [{-sa \| --save} SAVELOCAL ]

   [{-se \| --set} MODOFFSET ]

   [{-to \| --to} TARGETSTORE ]

   [{-uo \| --uuidonly} UUIDONLY ]

[{-v \| --version} VERSION ] [FILENAME... \|

   @FILELIST \|

   !SHELLCOMMAND]

Commands work as follows:

-  put saves just one file,

-  mput command saves multiple files,

-  mov saves just one file; after saving, it deletes the original file
   from the user store,

-  mmov saves multiple files; after saving, it deletes all original
   files from the user store.

-  putl saves data typed from the standard input; typed data is not
   echoed on the standard output; it accepts only one FILENAME which
   becomes the name of the saved data. This command does not compress
   data from standard input.

-  dput saves all files from a directory FILENAME.

-  dputr saves recursively all files from a directory FILENAME and all
   its subdirectories.

-  dmov works like dput, but also deletes the originals after saving
   them.

-  dmovr works like dmov, but also deletes the originals after saving
   them.

-  mod modifies already saved file.

-  modl modifies already saved data typed from the standard input.

-  com commits to the file modifications from the previous mod commands.

-  coml commits to the saved data modifications from the previous modl
   commands.

-  putr command restores data preserved by putl command when that data
   still persist in $CRYPTOMOVE_API_PATH directory (say, due to a server
   or host crash).

-  When LOGSAVE is set to -1, no filename shall be present.

Some options can be applied only to specific commands:

+-----------------+----------+
| -v –sa          | com coml |
+=================+==========+
| -cu -se –en –in | mod modl |
+-----------------+----------+

Commands get, mget, dget, getl – Restoring Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commands get and mget restore data from CryptoMove store into the user
store:

{get \| mget \| getl \| dget \| ddir}

[{-lr \| --lr} LOGRECORD ]

[{-v \| --version} VERSION ]

[{-n \| --number} NUMBER ]

[{-nw \| --nowait} NOWAIT ]

[{-r \| --remote} REMOTENAME]

[{-t \| --timeout} TIMEOUT ]

[{-u \| --unittest} TESTCOUNT ]

[{-us \| --user} USERNAME ] [FILENAME... \|

@FILELIST \|

!SHELLCOMMAND]

Commands work as follows:

-  get restores just one file,

-  mget restores multiple files.

-  getl restores data on the standard output, without saving it in a
   file.

-  dget restores all files previously saved with commands dput, dputr,
   dmov, or dmovr.

-  ddir dumps a listing of all files previously saved with commands
   dput, dputr, dmov, or dmovr.

-  When REMOTENAME is present in command getl, FILENAME is ignored.

-  When LOGRECORD is in short format, no filename shall be present.

-  USERNAME can be set only for get or mget.

Example:

   (4483)crv>client silent

   (4483)crv>trust on

   enter data key:

   (4483)crv>!echo 1 > e; echo 2 >> e; echo 3 >> e

   (4483)crv>!cat e

   1

   2

   3

   (4483)crv> mput @e

   (4483)crv>

Commands chk, mchk, dchk, chkl – Checking Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commands chk and mchk check integrity of data in CryptoMove store:

{chk \| mchk \| chkl \| dchk}

[{-lr \| --lr} LOGRECORD ]

[{-v \| --version} VERSION ]

[{-n \| --number} NUMBER ]

[{-nw \| --nowait} NOWAIT ]

   [{-r \| --remote} REMOTENAME]

[{-t \| --timeout} TIMEOUT ] [FILENAME... \|

@FILELIST \|

!SHELLCOMMAND]

Commands work as follows:

-  chk checks the integrity of just one file,

-  mchk check the integrity of multiple files.

-  chkl checks data saved from standard input.

-  dchk checks all files previously saved with commands dput, dputr,
   dmov, or dmovr.

-  When REMOTENAME is set, all file names are ignored.

Example:

   (6561)crv>trust on

   enter data key:

   (6561)crv>client silent

   (6561)crv>chk 1

   (6561)crv>mchk !cat e

   (6561)crv>

Commands del, mdel, ddel, dell – Deleting Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commands del and mdel delete all parts of one or more files from
CryptoMove store:

{del \| mdel \| dell \| ddel}

[{-lr \| --lr} LOGRECORD ]

[{-nw \| --nowait} NOWAIT ]

[{-v \| --version} VERSION ]

   [{-r \| --remote} REMOTENAME]

[{-t \| --timeout} TIMEOUT ] [FILENAME... \|

@FILELIST \|

!SHELLCOMMAND]

Commands work as follows:

-  del deletes all parts of just one file,

-  mdel deletes all parts of multiple files.

-  dell deletes all parts of the data saved from standard input.

-  ddel deletes all files previously saved with commands dput, dputr,
   dmov, or dmovr.

-  When REMOTENAME is set, all file names are ignored.

Example:

   (11473)crv>trust on

   enter data key:

   (11473)crv>put 1

   \***\* saving commences (entropy 153)

   1. compressing...

   2. cooking 96 keys.....

   3. making 96 part names....

   4. making 96 hash names....

   5. making 96 trace names.....

   7. permuting....

   8. splitting...

   9. encrypting....

   10. padding & signing...

   11. confusing...

   12. scattering...

   13. flushing...

   14. encrypting traces...

   15. dumping traces...

   16. logging record <#18>...

   17. start moving 96 out of 96...

   \***\* saved file <1>

   (11473)crv> del 1

   \***\* deleting commences

   1. using log record <#19>...

   2. cooking 96 keys.....

   3. making 96 part names....

   4. making 96 hash names....

   5. deleting 96 parts (in <=20 seconds)....

   deleted 96 parts of <1>

   elapsed 0.041 seconds

   \***\* ?deleted? file <1>

   (11473)crv>

Commands clr, mclr, dclr, clrl – Clearing Data
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commands clr and mclr delete all parts of one or more files from the
client native store (in case they were left there due to aborted
operations):

{clr \| mclr \| clrl \| dclr}

[{-v \| --version} VERSION ]

[{-r \| --remote} REMOTENAME]

[{-t \| --timeout} TIMEOUT ] [FILENAME... \|

@FILELIST \|

!SHELLCOMMAND]

Commands work as follows:

-  clr clears all parts of just one file,

-  mclr clears all parts of multiple files.

-  clrl clears all parts of data entered from standard input.

-  dclr clears all files previously saved with commands dput, dputr,
   dmov, or dmovr.

-  When REMOTENAME is set, all file names are ignored.

Example:

   (11473)crv>clr 1

   \***\* clearing commences

   1. using log record <#19>...

   2. cooking 96 keys.....

   3. making 96 part names....

   4. making 96 hash names....

   5. clearing 96 parts ....

   \***\* cleared file <1>

   (11473)crv>

Commands log, dlog, logl – Dump Operational Log
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Command log dumps operational logs:

   {log \| logl \| dlog} [{-c \| --recordcount} RECORDCOUNT]

   [{-r \| --remote} REMOTENAME]

   FILENAME

Commands work as follows:

-  log dumps an operational log of given file,

-  logl dumps an operational log of data saved from standard input with
   the given file.

-  dlog restores operational log for all files previously saved with
   commands dput, dputr, dmov, or dmovr.

-  When REMOTENAME is set, the file name is ignored.

Example:

(11473)crv>log 2

1+Mon Aug 31 18:09:05 2015
+e527c96a2fea405893a30b3a06e8e454+12+4+a6443088d974f1d9aff547229766dbed+32+33204+0+8+1440+0+8
2+Mon Aug 31 18:11:44 2015
+8c64ff20ad21405e92caf2d013a19139+12+4+a709021169431683004b32613fe55786+32+33204+0+8+1440+4+8+DELETED+Mon
Aug 31 18:22:28 2015

3+Mon Aug 31 18:25:00 2015
+f7b57289ee1f48579f6222c7530ab495+12+4+d8623b2429a2ab5eb589f3da0d73e65f+32+33204+0+8+1440+0+8

(11473)crv>

Commands rlog, rlogl – Recover Operational Log
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These commands recover operational log records from the data store and
assemble them into a single log file located in the client directory. If
the file log is already present, and force mode is on, then the records
recovered from the data store update respective records in the already
existing log:

   {rlog \| rlogl} [{-v \| --version} { VERSION \|

   VERSION: \|

   :VERSION \|

   VERSION\ :sub:`low`:VERSION:sub:`high` }]

   [{-t \| --timeout} TIMEOUT] FILENAME

Flag –v defines one or more log records to recover from the data store:

-  VERSION recovers one log record with the specified VERSION.

-  VERSION: recovers all records starting from VERSION up to the last
   record found in the store.

-  :VERSION recovers log records starting from version 1 up to the
   specified VERSION.

-  VERSION\ :sub:`low`:VERSION:sub:`high` recovers log records starting
   from VERSION\ :sub:`low` and ending with VERSION\ :sub:`high`.

-  If flag –v is missing, then the first version is recovered.

-  After a version of the saved file is deleted, its log cannot be
   recovered.

-  When REMOTENAME is set, the file name is ignored.

Commands work as follows:

-  rlog restores operational log of the given file,

-  rlogl restores operational log of data saved from standard input with
   the given name.

Example:

   (11473)crv>rlog 2

   (11473)crv>

Commands purge, dpurge, purgel – Purge Operational Log
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These commands purge operational log records for data that has been
deleted from the data store:

   {purge \| purgel \| dpurge} {-v \| --version} { VERSION \|

   VERSION: \|

   :VERSION \|

   VERSION\ :sub:`low`:VERSION:sub:`high` }

   [{-f \| --force} FORCEPURGE]

[{-r \| --remote} REMOTENAME]

   [{-t \| --timeout} TIMEOUT ] FILENAME

Flag –v is mandatory – it defines one or more log records to purge from
the data store:

-  VERSION recovers one log record with the specified VERSION.

-  VERSION: recovers all records starting from VERSION up to the last
   record found in the store.

-  :VERSION recovers log records starting from version 1 up to the
   specified VERSION.

-  VERSION\ :sub:`low`:VERSION:sub:`high` recovers log records starting
   from VERSION\ :sub:`low` and ending with VERSION\ :sub:`high`.

-  When REMOTENAME is set, the file name is ignored.

Commands work as follows:

-  purge purges operational log of the given file,

-  purgel purges operational log of data saved from standard input with
   the given name.

-  dpurge purges operational log for all files previously saved with
   commands dput, dputr, dmov, or dmovr.

-  

Example:

   (11473)crv>purge –v 1:2 1

   \***\* purged 1 record(s) from log

   (11473)crv>

Data Size Limitations
^^^^^^^^^^^^^^^^^^^^^

The current CryptoMove version limits the overall number of parts per
file in one commands [m]put, [m]mov or per data in command putl to 4096.
In addition, the size limit on each saved part is 16MB. Note that if the
command invokes compression, then this number limits the size of the
result of the compression – the original file or data may be larger. The
overall size of the (compressed) data allowed to be saved in one command
is 64GB.

CryptoMove client checks the parts’ sizes before breaking the data into
parts. If any part size exceeds the maximum allowed part length, the
client issues an error message and aborts the operation. In this case,
you may increase the number of parts in the commands [m]put, [m]mov or
putl using option –s so that the length of each part becomes acceptable,
or increase compression ratio with option –co to achieve the same
effect.

Redundancy Group Management 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following sub-sections create and control CryptoMove redundancy
groups. A redundancy group is a set of data store hosts, which confine
the movement of different copies of the same data saved in the store.
Each group consists of partitions – separate subsets of hosts.
Partitions are organized so that there is enough of them that do not
have common hosts. This assures that when the hosts from one partition
fail and the parts on those hosts become inaccessible, the data can
still be restored from the copies that travel hosts from the surviving
partitions of the same group.

Command putg – Create Redundancy Group
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Command putg creates a redundancy group in CryptoMove data store:

   putg [{-a \| --armslength} ARMSLENGTH ]

   [{-c \| --copy} COPYCOUNT ]

   [{-d \| --depth} DATADEPTH ]

   [{-m \| --move} MOVEPACE ]

   [{-p \| --pulse} PULSEBEAT ]

   [{-r \| --remote} REMOTENAME ]

   [{-s \| --split} SPLITCOUNT ]

   [{-to \| --to} TARGETSTORE ]

   [{-x \| --expire} EXPIRATION ] GROUPNAME

   Command works as follows:

-  When REMOTENAME is set, the GROUPNAME is ignored.

Command chkg – Check Redundancy Group
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Command gchk checks integrity of a redundancy group in CryptoMove data
store:

chkg [{-v \| --version} VERSION ]

   [{-r \| --remote} REMOTENAME]

[{-t \| --timeout} TIMEOUT ] GROUPNAME

Command works as follows:

-  When REMOTENAME is set, the GROUPNAME is ignored.

Command delg – Delete Redundancy Group
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Command delg deletes a redundancy group in CryptoMove data store:

delg [{-v \| --version} VERSION ]

   [{-r \| --remote} REMOTENAME]

[{-t \| --timeout} TIMEOUT ] GROUPNAME

Command works as follows:

-  When REMOTENAME is set, the GROUPNAME is ignored.

Command logg—Dump Log of Redundancy Group
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Command logg dumps operational log of a redundancy group:

   logg [{-r \| --remote} REMOTENAME] GROUPNAME

Command works as follows:

-  When REMOTENAME is set, the GROUPNAME is ignored.

Commands rlogg – Recover Operational Log of Redundancy Group
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These commands recover operational log records from the data store and
assemble them into a single log file located in the client directory. If
the file log is already present, and force mode is on, then the records
recovered from the data store update respective records in the already
existing log:

   rlogg [{-v \| --version} { VERSION \|

   VERSION: \|

   :VERSION \|

   VERSION\ :sub:`low`:VERSION:sub:`high` }]

   [{-t \| --timeout} TIMEOUT] GROUPNAME

Flag –v defines one or more log records to recover from the data store:

-  VERSION recovers one log record with the specified VERSION.

-  VERSION: recovers all records starting from VERSION up to the last
   record found in the store.

-  :VERSION recovers log records starting from version 1 up to the
   specified VERSION.

-  VERSION\ :sub:`low`:VERSION:sub:`high` recovers log records starting
   from VERSION\ :sub:`low` and ending with VERSION\ :sub:`high`.

-  If flag –v is missing, then the first version is recovered.

-  After a version of the saved file is deleted, its log cannot be
   recovered.

-  When REMOTENAME is set, the file name is ignored.

Commands work as follows:

-  rlog restores operational log of the given file,

-  rlogl restores operational log of data saved from standard input with
   the given name.

Creating Data Decoys
^^^^^^^^^^^^^^^^^^^^

When data name in command putl is set to decoy.cryptomove, or any
variation of it with letters of either upper or lower case, the client
does not ask for data but instead generates a 10MB of random data, which
it then saves under the specified name. By setting a large number of
copies, user can generate larger decoy data in the data store options
-co, -b, -m and -p are ignored for data names of this kind: instead,
their fixed values are supplied respectively as 0, 0, 4, and 4.

CryptoMove Directory Management
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove directory is a list of elements that describe data and files
saved into the data store, as well as other sub-directories created in
the directory. CryptoMove directories are maintained and saved by the
CryptoMove client process automatically, in the process of its
operations. The following sub-sections describe all client commands that
create, change, display, and remove CryptoMove directories.

Command mkdir – Creating Directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Command mkdir creates a directory in CryptoMove data store:

   mkdir [{-a \| --armslength} ARMSLENGTH ]

   [{-b \| --bitscatter} SCATTERLENGTH]

   [{-co \| --compress} STRENGTH ]

   [{-c \| --copy} COPYCOUNT ]

   [{-d \| --depth} DATADEPTH ]

   [{-e \| --entropy} ENTROPY ]

   [{-ex \| --expire} EXPIRATION ]

   [{-f \| --fill} FILLGROUP ]

   [{-g \| --group} GROUPNAME ]

   [{-m \| --move} MOVEPACE ]

   [{-n \| --number} NUMBER ]

   [{-p \| --pulse} PULSEBEAT ]

   [{-pe \| --permutation PERMUTATION ]

   [{-r \| --remote} REMOTENAME ]

   [{-s \| --split} SPLITCOUNT ]

   [{-to \| --to} TARGETSTORE ] DIRECTORY

All parameters relate to the directory data itself, not to the directory
elements that may be added to the directory. The meaning of all
parameters is the same as in command put. The new directory name
DIRECTORY must be either the name of the top directory ‘/’, or a name of
a sub-directory of the current directory (set with command cd). As such,
sub-directory name cannot contain symbol ‘/’. If REMOTENAME is set, it
must be the same as DIRECTORY.

The command works as follows:

-  For mkdir to succeed, the client must operate in trust mode 'on'.

-  Only one version of directory with a given name can exist under a
   subdirectory.

-  Full directory name is the created name appended to the full path
   formed by the directory names from the successive 'cd' commands -- it
   is unique for a given password. It is also accessible from any client
   host where the data password has been distributed or replicated.

Command rmdir – Removing Directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

rmdir [{-t \| --timeout} TIMEOUT ] DIRECTORY

Command rmdir deletes the directory specified by DIRECTORY.

The command works as follows:

-  For mkdir to succeed, the client must operate in trust mode 'on'.

-  It only deletes directories under the current directory set
   previously by command ‘cd’, or the top of directory hierarchy ‘/’
   when current directory is not set.

-  For sub-directory to be removed, it shall contain no sub-directories
   and no saved data or files.

Command cd – Changing Current Directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

cd DIRECTORY

This command changes current directory to the one with the specified
DIRECTORY. The specified directory name can be set to ‘..’, which
denotes the parent of the current directory. All data and files saved or
sub-directories created while in the current sub-directory become
members of that sub-directory. As such, all sub-directory contents can
be displayed with the commands dir or dirr. When saved data is removed,
its membership in the sub-directory is revoked.

Command pwd – Displaying Current Directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pwd

This command dumps path to the current native directory.

Command uncd – Getting out of Current Directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

uncd

This command gets out of any current directory.

Command dir – Displaying Directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

dir [{-f \| --full} DIRFULL] [DIRECTORY]

This command displays the contents of the current directory – all its
sub-directories, as well as files and data saved under the current
directory. The display format is described on the following example:

(7039)crv>cd /

(7039)crv/>cd abc

(7039)crv/abc>dir

\***\* directory </abc>

Sep 5 12:08:11 2016 0 d "def/" ["/abc/def"]

Sep 5 12:08:28 2016 2 1 "z." []

Sep 5 12:08:38 2016 2 1 "z." ["/abc/bla"]

\***\* total 3 elements

(7039)crv/abc>

-  The header line shows full directory name, which includes all parent
   sub-directories.

-  Subsequent lines describe one directory element.

-  Each line has the following format:

   -  Date and time when the element has been created

   -  Size of data or file saved. For large data and files, the sizes is
      measured in K (kilobytes), M (megabytes), or G (gigabytes).

   -  Version number of the saved data or file, or letter ‘d’ for a
      sub-directory.

   -  Name of the saved data or file, or name of a sub-directory in
      quotes. Sub-directory names and data names are appended by a slash
      sign ‘/’.

   -  In square brackets, a quoted emote name of the saved data or file
      if they have been saved with the option ‘-r REMOTENAME’. For
      sub-directories, this field shows their full path.

   -  When DIRFULL is set to 1, the trailing information on each line
      show the data of the respective log record that describes the
      saved data or file, or sub-directory. When set to -1, no date of
      creation is displayed. By default, the value is 0 – short format
      with the date of creation.

-  The final line shows the total number of elements in the
   sub-directory.

Command dirr – Displaying Directory Hierarchy
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

dirr [{-f \| --full} DIRFULL] [DIRECTORY]

This command displays recursively all sub-directories of the current
directory similarly to command dir, as shown in the following example:

   (7039)crv/abc>cd ..

   (7039)crv/>dirr

   \***\* directory </>

   Sep 5 12:07:54 2016 0 d "abc/" ["/abc"]

   \***\* total 1 elements

   \***\* directory </abc>

   Sep 5 12:08:11 2016 0 d "def/" ["/abc/def"]

   Sep 5 12:08:28 2016 2 1 "z." []

   Sep 5 12:08:38 2016 2 1 "z." ["/abc/bla"]

   \***\* total 3 elements

   \***\* directory </abc/def>

   \***\* total 0 elements

   (7039)crv/>

CryptoMove Server Auditing
~~~~~~~~~~~~~~~~~~~~~~~~~~

CryptoMove auditing is controlled by the shell environment variable
CRYPTOMOVE_NOAUDIT. If it is not set, then both Cryptomove and Hello
runtime engine always start auditing operations at startup. Later, at
runtime, auditing can be switched off or on on demand. If it is set to
any value, then neither CryptoMove nor Hello runtime engine will
initiate auditing operations. There will be no way to switch auditing on
at runtime, until the server is restarted after CRYPTOMOVE_NOAUDIT is
unset.

The above functionality describes behavior for both CryptoMove client
and server processes. In particular:

-  if the client is a separate process launched via crv, then that
   process is subject to the above rules.

-  if the client has been launched via icrv or any application process
   linked with CryptoMove C language API, it executes within the server
   process. Thus, CRYPTOMOVE_NOAUDIT, being set or not set at the server
   startup time, controls that client's auditing.

Two client commands control CryptoMove auditing operations:

1. Command audit [on \| off] can be issued from a client launched in
   server owner mode:

-  audit on -- switches server auditing on

-  audit off -- switches server auditing off

2. When auditing, the server can include a client audit name in the
   auditing records:

-  Command auditname AUDITNAME sets client auditing name to AUDITNAME.

-  Command auditname displays the current client auditing name.

-  Command uditname NONE erases current client auditing name.

Auditing records are placed inside UNIX syslog file. They all contain
the following substrings, which can be used to extract CryptoMove
auditing records from syslog for any further analysis or archiving:

-  CRYPTOMOVE AUDIT TRAIL: NONE

..

   for clients that did not set their audit name.

-  CRYPTOMOVE AUDIT TRAIL: AUDITNAME

..

   for clients that set their audit name to AUDITNAME

All Cryptomove auditing records are done on behalf of the Hello runtime
engine hee, which name appears in the prefix of all such records. Hello
runtime engine auditing messages that are issued from the engine itself,
not from any Cryptomove Hello package, contain substring HELLO ENGINE.

High Availability Cluster Control
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Every CryptoMove server maintains a High Availability cluster of servers
with which it can exchange data parts. The server constantly monitors
the state of each server from its cluster via a periodic heartbeat
messages.

-  While heartbeats keep flowing, the monitored server remains in ONLINE
   state.

-  When heartbeats fail, the monitored server transitions to OFFLINE or
   FAULTED state.

-  After heartbeats resume, the monitored server returns to ONLINE
   state.

Each server can monitor several clusters, one of which is declared the
*current cluster*. **Data parts move only to online servers from the
current cluster.** You choose and display current cluster with the
following commands from CryptoMove client:

   { ccl \| current cluster } *Dumps the name of the current cluster.*

   { scl \| set cluster } CLUSTER_NAME *Sets current cluster by its
   name.*

You can see online help for all cluster commands by typing command

   cluster {-h \| --help}

Server States
^^^^^^^^^^^^^

The following table lists all possible CryptoMove server states [19]_:

+-----------------------------------+-----------------------------------+
| ONLINE                            | The server is monitored, the last |
|                                   | periodic heartbeat to the host    |
|                                   | has succeeded.                    |
|                                   |                                   |
|                                   |    *This state indicates that the |
|                                   |    server participates in the     |
|                                   |    normal course of operations of |
|                                   |    saving, restoring, moving and  |
|                                   |    other data operations.*        |
+===================================+===================================+
| OFFLINE                           | The server is not monitored; the  |
|                                   | last monitored state was not      |
|                                   | FAULTED.                          |
|                                   |                                   |
|                                   |    *The server transitions into   |
|                                   |    this state after its           |
|                                   |    monitoring has been switched   |
|                                   |    off by a command from the      |
|                                   |    client.*                       |
+-----------------------------------+-----------------------------------+
| FAULTED                           | The server is monitored, the last |
|                                   | periodic heartbeat to the host    |
|                                   | has failed.                       |
|                                   |                                   |
|                                   |    *The server unexpectedly       |
|                                   |    stopped responding to          |
|                                   |    heartbeats.*                   |
+-----------------------------------+-----------------------------------+
| HOSTILE                           | The server is not monitored       |
|                                   | because it has produced corrupt   |
|                                   | data.                             |
|                                   |                                   |
|                                   |    *Other servers detected that   |
|                                   |    the server produced a          |
|                                   |    corrupted data part.*          |
+-----------------------------------+-----------------------------------+
| UNREACHED                         | The server is monitored; the last |
|                                   | connect to the host has failed.   |
|                                   |                                   |
|                                   |    *The server cannot be reached  |
|                                   |    over the network in order to   |
|                                   |    establish a TCP/IP             |
|                                   |    connection.*                   |
+-----------------------------------+-----------------------------------+
| BREACHED                          | The server is monitored; the      |
|                                   | server has detected a breach of   |
|                                   | its own data store.               |
|                                   |                                   |
|                                   |    *One of three conditions       |
|                                   |    occurred:*                     |
|                                   |                                   |
|                                   | -  *The server’s internal clock   |
|                                   |    and OS clock got               |
|                                   |    dissyncrhonized.*              |
|                                   |                                   |
|                                   | -  *The server noticed            |
|                                   |    unauthorized process accessing |
|                                   |    its data store.*               |
|                                   |                                   |
|                                   | -  *The server notice that it has |
|                                   |    produced corrupted data part.* |
+-----------------------------------+-----------------------------------+
| UNKNOWN                           | The server is not monitored or    |
|                                   | has been just created.            |
|                                   |                                   |
|                                   |    *No connection has been        |
|                                   |    established to the server, and |
|                                   |    no heartbeats are flowing to   |
|                                   |    this server yet.*              |
+-----------------------------------+-----------------------------------+
| ENTERING                          | The server is monitored; the      |
|                                   | server is starting up.            |
|                                   |                                   |
|                                   |    *The server has been started   |
|                                   |    up, but has not completed the  |
|                                   |    startup procedure yet.*        |
+-----------------------------------+-----------------------------------+
| EXITING                           | The server is monitored; the      |
|                                   | server is going down.             |
|                                   |                                   |
|                                   |    *The server has been shut      |
|                                   |    down, but has not completed    |
|                                   |    the shut down procedure yet.*  |
+-----------------------------------+-----------------------------------+
| DOWN                              | The server is monitored; the      |
|                                   | server has been shut down.        |
|                                   |                                   |
|                                   |    *The server has been shut down |
|                                   |    by the server shut down        |
|                                   |    command crv st.*               |
+-----------------------------------+-----------------------------------+

Every host exchanges data only with hosts which states are ONLINE or
BREACHED. Servers in BREACHED state propagate arriving data immediately,
without regard to the client defined data movement or pulse frequencies.

Default Cluster Membership
^^^^^^^^^^^^^^^^^^^^^^^^^^

At startup, CryptoMove server creates the default cluster named
DIRECT_CRYPTOMOVE_CLUSTER. CryptoMove server running on a given computer
maintains its cluster membership as follows:

1. At startup, it creates a default cluster named
   DIRECT_CRYPTOMOVE_CLUSTER:

   a. If there is an empty file $HELLO_PACKSRC_PATH/.hello_hosts, then
      the server becomes the only member of the default cluster.

   b. If $HELLO_PACKSRC_PATH/.hello_hosts is not empty, then its content
      shall be similar to the content of the system file /etc/hosts.
      Hosts from $HELLO_PACKSRC_PATH/.hello_hosts all become members of
      the default cluster.

   c. If $HELLO_PACKSRC_PATH/.hello_hosts does not exist, then
      CryptoMove reads file /etc/hosts and adds all hosts listed there
      into the default cluster.

2. At runtime, system owner can add or remove other hosts using an
   explicit cluster command submitted via the CryptoMove client. The
   next sub-section 4.3.12.3 lists all cluster commands.

3. The system owner may control the membership of its server in other
   server’s clusters via the content of the file
   $HELLO_PACKSRC_PATH/.hello_in as follows:

   d. If $HELLO_PACKSRC_PATH/.hello_in is empty, then it cannot be a
      cluster member of any other host.

   e. If $HELLO_PACKSRC_PATH/.hello_in is not empty, it shall contain
      information similar to /etc/hosts. Only hosts listed in
      $HELLO_PACKSRC_PATH/.hello_in can list this host in their
      clusters.

   f. If $HELLO_PACKSRC_PATH/.hello_in does not exist, then this server
      can become a member of any cluster of any CryptoMove server.

Standalone CryptoMove
^^^^^^^^^^^^^^^^^^^^^

You may set up a standalone CryptoMove server – the one that never
communicates with other CryptoMove servers – by following the rules from
the previous sub-section 4.2.3. For a standalone CryptoMove server,
create empty files $HELLO_PACKSRC_PATH/hello_hosts and
$HELLO_PACKSRC_PATH/.hello_in.

Cluster Commands
^^^^^^^^^^^^^^^^

You control CryptoMove cluster via CryptoMove client after starting it
as a system owner as ‘crv os’. Each cluster control command begins with
the keyword cluster followed by specific *cluster command* and its
parameters. The general form of all cluster commands is

   cluster [--SERVER server_name] cluster_args

The optional keyword --SERVER followed by the name of the server
server_name indicates that the subsequent cluster arguments cluster_args
shall be applied to a server with the specified name. If ‑‑SERVER is
missing, then the arguments are applied to the current CryptoMove
server.

The following table lists all possible cluster command arguments:

+-----------------------------------+-----------------------------------+
| **Arguments**                     | **Explanation**                   |
+===================================+===================================+
| --DUMP                            | Dumps all clusters monitored by   |
|                                   | this server.                      |
+-----------------------------------+-----------------------------------+
| cluster_name {--LOCAL\|           | Creates cluster named             |
|                                   | cluster_name from:                |
| --DIRECT\|                        |                                   |
|                                   | -  Single local host (--LOCAL),   |
| --REVERSE\|                       |                                   |
|                                   | -  Direct neighborhood [22]_      |
| --FULL\|                          |    (--DIRECT)                     |
|                                   |                                   |
| --DEEP}                           | -  Reverse neighborhood           |
|                                   |    (--REVERSE),                   |
| [count [max [depth]]]             |                                   |
|                                   | -  Both direct and reverse        |
|                                   |    neighborhoods (--FULL)         |
|                                   |                                   |
|                                   | -  From hosts located deep inside |
|                                   |    the network.                   |
|                                   |                                   |
|                                   | Count: Choose exactly count       |
|                                   | hosts; if missing choose all      |
|                                   | hosts.                            |
|                                   |                                   |
|                                   | Max: Get up to max candidate      |
|                                   | hosts randomly chosen from count  |
|                                   | hosts; if missing then max =      |
|                                   | count.                            |
|                                   |                                   |
|                                   | Depth: Only for DEEP choice:      |
|                                   | traverse down to depth in the     |
|                                   | group of directly connected       |
|                                   | hosts; if missing then depth =    |
|                                   | max. [23]_                        |
+-----------------------------------+-----------------------------------+
| cluster_name --DUMP               | Dumps parameters of all hosts     |
|                                   | from the specified cluster on the |
|                                   | client stdout.                    |
+-----------------------------------+-----------------------------------+
| cluster_name --DELETE             | Deletes specified cluster.        |
+-----------------------------------+-----------------------------------+
| cluster_name --START              | Starts monitoring specified       |
|                                   | cluster.                          |
+-----------------------------------+-----------------------------------+
| cluster_name --STOP               | Stops monitoring specified        |
|                                   | cluster.                          |
+-----------------------------------+-----------------------------------+
| cluster_name --TRACE              | Traces monitoring specified       |
|                                   | cluster on the server stdout.     |
+-----------------------------------+-----------------------------------+
| cluster_name --NOTRACE            | Stops tracing specified cluster.  |
+-----------------------------------+-----------------------------------+
| cluster_name --TIMEOUT N          | Sets N Seconds for heartbeat      |
|                                   | timeout to cluster hosts. The     |
|                                   | default timeout is 1 second.      |
+-----------------------------------+-----------------------------------+
| cluster_name --COUNTDOWN N        | Sets N seconds for the interval   |
|                                   | between heartbeats attempts. The  |
|                                   | default interval is 1 second.     |
+-----------------------------------+-----------------------------------+
| cluster_name –-MIN min            | Sets minimum desired count of     |
|                                   | online hosts to min. If there are |
|                                   | less than min online hosts, the   |
|                                   | cluster monitor procures hosts    |
|                                   | from the cluster. By default, min |
|                                   | is one-third of hosts in the      |
|                                   | cluster plus 1.                   |
+-----------------------------------+-----------------------------------+
| cluster_name –-MAX max            | Sets maximum desired count of     |
|                                   | online hosts to max. By default,  |
|                                   | max is min times two.             |
+-----------------------------------+-----------------------------------+
| cluster_name --HOST host_name     | Adds a host with its specified    |
|                                   | host_address and optional         |
| [--ADDRESS host_address]          | host_path to the specified        |
|                                   | cluster and sets its cluster name |
| [host_path]                       | as host_name; if host_address is  |
|                                   | missing, then uses host_name as   |
|                                   | the host_address.                 |
+-----------------------------------+-----------------------------------+
| cluster_name --HOST host_name     | Dumps parameters of the specified |
| --DUMP                            | host.                             |
+-----------------------------------+-----------------------------------+
| cluster_name --HOST host_name     | Deletes specified host from the   |
| --DELETE                          | specified cluster.                |
+-----------------------------------+-----------------------------------+
| cluster_name --HOST host_name     | Starts monitoring specified host  |
| --START                           | from the specified cluster.       |
+-----------------------------------+-----------------------------------+
| cluster_name --HOST host_name     | Stops monitoring specified host   |
| --STOP                            | from the specified cluster.       |
+-----------------------------------+-----------------------------------+

CCAPI Reference
===============

*CryptoMove C Language Application Programming Interface* (CCAPI) offers
a number of C function calls to save and restore data in the CryptoMove
data store. With CCAPI, you can efficiently integrate CryptoMove
operations inside your application’s logic.

All declarations of CCAPI C functions with the accompanied data types
and constants are concentrated in file cryptomove_api.h, which you shall
include into the C program when calling CCAPI. CCAPI code is located
within cryptomove_api.cpp -- you shall compile the code of your
application together with that source file.

CCAPI Operations
----------------

Before using CCAPI, you shall set environmental variable CRYPTOMOVE_API
to any value. When using CCAPI, you should follow a variation of the
following programming pattern, skipping or repeating some steps
according to your application’s logic:

+-----------------------------------+-----------------------------------+
| **CCAPI Operation Step**          | **Comment**                       |
+===================================+===================================+
| 1. Startup CryptoMove server      | CryptoMove server process must be |
|                                   | running on the local host in      |
|                                   | order for any program to save and |
|                                   | restore data. However, some       |
|                                   | operations, like system key       |
|                                   | generation, require no server.    |
|                                   |                                   |
|                                   | There may be no more than one     |
|                                   | server running on the host. You   |
|                                   | can skip the server startup from  |
|                                   | your application if some other    |
|                                   | process has already started it.   |
+-----------------------------------+-----------------------------------+
| 2. Startup one or more CryptoMove | You shall start up a CryptoMove   |
|    clients                        | client process from your          |
|                                   | application in order to save and  |
|                                   | restore data. The CryptoMove      |
|                                   | client immediately connects your  |
|                                   | program with a dedicated thread   |
|                                   | inside the local CryptoMove       |
|                                   | server and then quits. From then  |
|                                   | on, your program communicates     |
|                                   | through a UNIX pipe with the API  |
|                                   | thread directly in order to       |
|                                   | perform CryptoMove commands. Note |
|                                   | that the speed of this            |
|                                   | communication is up to ten times  |
|                                   | faster than the speed of          |
|                                   | communication between regular crv |
|                                   | client and CryptoMove server via  |
|                                   | TCP/IP.                           |
|                                   |                                   |
|                                   | Every CCAPI call, which accesses  |
|                                   | data, causes that API thread to   |
|                                   | perform a specific CryptoMove     |
|                                   | command as if that command is     |
|                                   | executed by a CryptoMove client   |
|                                   | crv.                              |
|                                   |                                   |
|                                   | One application process can start |
|                                   | up one or more clients, which     |
|                                   | connect the program to the        |
|                                   | separate API threads inside the   |
|                                   | local CryptoMove server.          |
|                                   |                                   |
|                                   | After successful startup, each    |
|                                   | client is designated in your      |
|                                   | application by a separate         |
|                                   | instance of the data structure    |
|                                   | crv_daemon.                       |
+-----------------------------------+-----------------------------------+
| 3. Perform one or more of the     | After an application process      |
|    CryptoMove client operations   | starts a CryptoMove client and    |
|                                   | connects to an API thread, that   |
|                                   | thread performs requests from     |
|                                   | that user process exclusively –   |
|                                   | no other process is able to       |
|                                   | perform CryptoMove requests with  |
|                                   | that API thread.                  |
|                                   |                                   |
|                                   | From an application process,      |
|                                   | CCAPI always directs data calls   |
|                                   | to one or more CryptoMove API     |
|                                   | threads, depending on how many    |
|                                   | clients your application has      |
|                                   | started. In the calls, you        |
|                                   | designate that thread by a        |
|                                   | pointer to an instance of the     |
|                                   | structure crv_daemon.             |
+-----------------------------------+-----------------------------------+
| 4. Stop CryptoMove client(s)      | Using CCAPI, you can stop any API |
|                                   | thread previously started from a  |
|                                   | user process.                     |
+-----------------------------------+-----------------------------------+
| 5. Stop CryptoMove server         | CCAPI provides a call to stop the |
|                                   | running CryptoMove server.        |
+-----------------------------------+-----------------------------------+

CCAPI Source Code
-----------------

The source code for CCAPI is located in files cryptomove_api.h and
cryptomove_api.cpp. Both files contain extensive comments explaining
parameters from the signatures of the API methods, and the processing of
the methods themselves.

Simple Client icrv
------------------

The source of a simple interactive client icrv is available in the file
icrv.cpp. Following the comments and the code from this source file, one
can learn how to use CCAPI in order to create their own programs that
use CCAPI to manipulate data with the CryptoMove Server.

You may build the icrv client (and, similarly, any client of your own)
using C++ compiler g++ as follows:

   g++ -g -pthread icrv.cpp cryptomove_api.cpp –ledit\\

   -ltermcap\\

   -lnettle\\

   -o icrv

icrv Commands
~~~~~~~~~~~~~

The simple client icrv can execute almost all of the data management
commands from crv client. In addition, it can execute a number of server
management and key management operations. For a list of specific icrv
commands, see its help information by typing icrv help command h.

APPENDIX – Math
===============

Data Split Effect
-----------------

In order to realize the prohibitive amount of efforts to start any
cryptographic attack on the CryptoMove distributed data store, one can
think of an encrypted file broken into :math:`N` parts, each part
multiplied into :math:`C` copies. Together, they constitute
:math:`F = N \times C` file parts. It is impossible to distinguish
between an original and a copy; their content, names, pattern of use and
access times do not reveal the source file. Moreover, in order to
decrypt the original, all :math:`N` parts must be available in a
specific order [24]_.

.. image:: /images/data_split_effect.png

There are
:math:`A = F \times \left( F - 1 \right)\ldots\left( F - N + 1 \right) = \frac{F!}{\left( F - N \right)!}`
ordered combinations of parts. Thus, one has to perform :math:`O(A)`
brute force attempts in order to recover the complete encrypted piece
from :math:`\text{\ N}` parts. For a large amount of
parts\ :math:`\text{\ \ F}`, this number is quite large. For example, a
file broken into :math:`N = 12` parts [25]_ each duplicated into
:math:`C = 4` copies yields number of file parts
:math:`F = 12 \times 4 = 48` and number of
combinations\ :math:`\ A = \frac{F!}{\left( F - N \right)!} = \frac{48!}{36!} \approx 2^{64.85}`.
However, the following explains that in reality the number of
combinations is substantially larger.

In order to gather the file parts, one has to know their names. Those
names are unknown to the attacker, and the parts of one file are
intermixed in the native directories with the parts of other files.
Suppose that all native directories of all CryptoMove servers contain
parts of :math:`M` files each broken into the same number
:math:`F = N \times C` of :math:`N` parts and :math:`C` duplicates. The
locations of the parts of interest are unknown – they can reside in any
directory at any server. Therefore, to find the proper combination of
the parts of a single file
requires\ :math:`\ O\left( \frac{\left( M \times F \right)!}{\left( M \times F - N \right)!} \right)`
attempts. If :math:`M` is sufficiently large, then this number becomes
very large. For example, if the store has :math:`M = 1000` files,
then\ :math:`\ A = \frac{\left( M \times F \right)!}{\left( M \times F - N \right)!} = \frac{48000!}{47988!} \approx 2^{186.60}`.

Permutation Effect
------------------

However, even the number of attempts
:math:`O\left( \frac{\left( M \times F \right)!}{\left( M \times F - N \right)!} \right)`
is a gross underestimate. It assumes that the encrypted file is broken
into consecutive parts. Yet in reality, it is broken into
non-consecutive parts using a random permutation. Therefore, unless one
uncovers the permutation, it is practically impossible to rebuild the
file from its parts. Just to find all the permutation’s parts again
requires going
through\ :math:`\text{\ O}\left( \frac{\left( M \times F \right)!}{\left( M \times F - N \right)!} \right)`
combinations because for each data part CryptoMove creates its
corresponding permutation part and it is impossible to distinguish
between the parts and permutations.

.. image:: /images/permutation_effect.png

Permutation files increase the previous estimate from
:math:`\ O\left( \frac{\left( M \times F \right)!}{\left( M \times F - N \right)!} \right)\text{\ \ }`\ to\ :math:`\ O\left( \frac{\left( M \times 2 \times F \right)!}{\left( M \times 2 \times F - 2 \times N \right)!} \right)`.
For example, in case of :math:`12` parts and :math:`4` copies the number
of combinations for parts and permutations (without taking into account
the total amount of files\ :math:`\ M`, i.e.
setting\ :math:`\text{\ \ }M = 1`) increases
to\ :math:`\ A = \frac{96!}{72!} \approx 2^{153.50}`. When :math:`M`
reaches :math:`\ 10000\ `\ files, then
:math:`\text{A\ }`\ becomes\ :math:`\ \frac{\left( M \times 2 \times F \right)!}{\left( M \times 2 \times F - N \right)!} = \frac{960000!}{959976!} \approx 2^{476.94}`.
Note how much higher is this number compared to the number of atoms in
the Universe :math:`{\approx 2}^{272.4}` ( [26]_).

Movement Effect
---------------

In CryptoMove, the file parts travel randomly and independently between
multiple computers -- at each moment in time, the parts reside at
different places across the network. Each part name changes when it
moves from one location to another: the changing part names are always
unique and never repeat. Also, the name keeps changing while the part
persists in one place in between the moves.

The name changes follow a random pattern. The access time is always the
same. The CryptoMove server encrypts with its system key the file parts
sometimes after they arrive on that computer and always before moving
them to another computer. It also uses the system key to generate new
part names for the same part.

.. image:: /images/movement_effect.png

The movement further decreases the odds of a successful attack because
there is no way for the attacker to identify the parts she had already
processed in the previously failed decryption attempts. Therefore, the
subsequent attempts might fail since the attacker could unknowingly use
the same parts in the same order that had failed previously. Thus, the
attacker’s chances may not increase with each next deciphering attempt.
The following analysis quantifies the advantages of the part movement.

Quantization of the Movement Effect
-----------------------------------

Without the movement, after failing to decrypt a given combination out
of the total :math:`A` combination, the attacker can exclude the failed
combination from subsequent attempts. Because the parts are uniquely
identified by their names and locations in different native stores, the
attacker can choose the next combination from the total of :math:`A - 1`
combinations, the following one from the total of :math:`A - 2`
combinations, and so on,
where\ :math:`\ A = \frac{\left( M \times 2 \times F \right)!}{\left( M \times 2 \times F - 2 \times N \right)!}`.
For this reason, the probabilities :math:`\ S_{i}\ `\ of success exactly
in the :math:`i`-th trial can be proven to be the same value
:math:`A^{- 1}` as follows:

:math:`{S_{1} = A}^{- 1}`,

:math:`S_{2} = \left( 1 - S_{1} \right){\times \left( A - 1 \right)}^{- 1} = \left( 1 - \frac{1}{A} \right){\times \left( A - 1 \right)}^{- 1} = A^{- 1}`
,

:math:`S_{3} = \left( 1 - S_{1} - S_{2} \right){\times \left( A - 2 \right)}^{- 1} = \left( 1 - \frac{1}{A} - \frac{1}{A} \right){\times \left( A - 2 \right)}^{- 1} = A^{- 1}`
,

. . .

:math:`S_{i + 1} = (1 - \sum_{j = 1}^{i}{S_{j})}{\times (A - i)}^{- 1} = {\left( 1 - \frac{i}{A} \right){\times \left( A - i \right)}^{- 1} = A}^{- 1}`.

The above scenario is similar to a simple case of a hypergeometric
discrete distribution that describes the probability of one success in
:math:`\text{i\ }`\ draws without replacement from the population of
size :math:`A` containing one success  [27]_. The attacker succeeds with
probability\ :math:`\ 1\ `\ after no more than :math:`A` attempts trying
at most :math:`A` distinct combinations:

:math:`P_{A} = \sum_{i = 1}^{A}S_{i} = \sum_{i = 1}^{A}A^{- 1} = A \times A^{- 1} = 1`.

However, the part movement decreases the odds of a successful attack
without a guarantee of success. Indeed, assume that the attacker only
reads the file parts to decipher user data, and never deletes the parts
from the store (deleting the parts would limit the chance of success
even further). After failing to decrypt a given combination, the
attacker cannot reliably exclude that combination from subsequent
attempts because the parts continuously change their names and content
while retaining their access times. Any next combination might include
the parts that have been chosen in the previous trials. Therefore, one
can model attacker’s activity as repetitive attempts with replacement
from :math:`A` combinations. Thus, the odds of success :math:`T_{i}` in
each next attempt keep decreasing:

:math:`{T_{1} = A}^{- 1}`,

:math:`T_{2} = \left( 1 - T_{1} \right){\times A}^{- 1} = (1 - {A^{- 1}) \times A}^{- 1}`,

:math:`T_{3} = \left( 1 - T_{1} - T_{2} \right){\times A}^{- 1} = (1 - {A^{- 1} - \left( 1 - {A^{- 1}) \times A}^{- 1} \right) \times A}^{- 1} = {(1 - A^{- 1})}^{2}{\times A}^{- 1}`,

. . .

:math:`T_{i + 1} = (1 - \sum_{j = 1}^{i}{T_{j})}{\times A}^{- 1} = {(1 - A^{- 1})}^{i}{\times A}^{- 1}`.

The above scenario is similar to a simple case of a binomial discrete
distribution that describes the probability of one success in
:math:`\text{i\ }`\ draws with replacement from the population of size
:math:`A` containing one success [28]_. From the above formula, it is
clear that as the number of failed attempts :math:`\text{i\ }`\ keeps
increasing, the probability of success at each next step decreases.
Moreover, there is no guarantee of success at any time: after :math:`K`
attempts the probability of success in at least one of them is always
less than\ :math:`\ 1` (although it approaches :math:`1` as
:math:`K \rightarrow \infty)`:

:math:`P_{K}(A) = \sum_{i = 1}^{K}T_{i} = \sum_{i = 1}^{K}{{(1 - A^{- 1})}^{i} \times A}^{- 1} = 1 - \left( 1 - A^{- 1} \right)^{K} < 1`.

In actuality, the attacker’s chances of success are even slimmer than
has been estimated so far. Indeed, the previous analysis assumes that
the attacker knows that all parts in the chosen combination are
different. However, without freezing all CryptoMove servers, the parts
may change names and locations in the process of the attacker choosing a
single combination of parts. It can happen that several parts in the
same combination are re-encrypted and renamed versions of the same
original data part. Therefore, in the worst-case scenario (from the
attacker’s point of view), one should assume the total amount of
combinations to be :math:`A^{'} = {(M \times 2 \times F)}^{2 \times N}`
which is higher (although not significantly for large\ :math:`\ M`) than
the previously considered
value\ :math:`\text{\ \ }A = \frac{\left( M \times 2 \times F \right)!}{\left( M \times 2 \times F - 2 \times N \right)!}`.
This estimate decreases the probability of success even further.

Distribution Effect
-------------------

To recover parts from N computers as described in the previous
sub-sections, an attacker must be in possession of the keys to login
into each of the computers and to access their persistent stores. If
:math:`N` different computers have independent access control policies
resulting in different keys, then the attacker also has to discover at
least :math:`N` keys. Suppose that the probability of discovering a key
on :math:`i`-th computer is\ :math:`\text{\ p}_{i}`. Then the
probability of independently discovering all keys of :math:`N` computers
is no more than\ :math:`\ \prod_{i = 1}^{N}p_{i}`. For large :math:`N`
and small\ :math:`\text{\ p}_{i}`, say :math:`N > 1000`
and\ :math:`\ p_{i} < 1/2` (which is reasonable to assume when computers
are located on the Internet, in the ownership of different users),
achieving this feat in real time is infeasible.

Combined Movement & Distribution Effects
----------------------------------------

The combined effect of data movement and distribution is similar to a
shell game where the pea is constantly moving between the shells.
It forbids simultaneous access to all parts at any given time \ *unless
the attacker freezes all computers*. Without freezing, she cannot
perform a successful parallel attack as some parts may be missing. By
the time she finishes scanning a limited number of combinations, the
stored data changes due to the movement of parts. Therefore, the
attacker must try an unimaginable amount of sequential scanning attempts
-- that would take longer than the current lifetime of the Universe.

Since each read takes a finite amount of time\ :math:`\ T`, to read
:math:`K` times takes time\ :math:`\ K \times T`. This value is very big
for sufficiently large\ :math:`\ K`: unless the attacker can peek into
the future, it is physically impossible to complete the attack in the
attacker’s lifetime.

Worst-Case Scenarios
--------------------

Suppose that an attacker takes a physical possession of all computers,
obtains all root keys and stops all CryptoMove servers. Still, without
knowing the part names, she would face an impossible task of sifting
through\ :math:`\text{\ O}\left( \frac{\left( M \times 2 \times F \right)!}{\left( M \times 2 \times F - 2 \times N \right)!} \right)\ `\ combinations
of all file parts, where :math:`M` is the total amount of parts across
all the stalled computers. Finally, suppose that despite all odds,
someone gets the encrypted parts of the whole file. In this case, the
attacker still faces the task of deciphering the strongly encrypted data
without knowing the data key.

Endnotes
========

.. [1]
   CryptoMove will be accessible in future from other Linux
   distributions, Windows, OS X, mobile devices.

.. [2]
   http://en.wikipedia.org/wiki/Ubuntu_(operating_system)

.. [3]
   http://en.wikipedia.org/wiki/Linux_OS

.. [4]
   http://en.wikipedia.org/wiki/X86_64

.. [5]
   Note how much higher is this number compared to the estimated number
   of atoms in the Universe\ \ :math:`\ {\sim 2}^{272.4}`.

.. [6]
   This is a free public domain utility. You may need to install iotop
   on your system in case it is not there yet.

.. [7]
   This is a free public domain utility. You may need to install iotop
   on your system in case it is not there yet.

.. [8]
   I.e. it establishes a TCP/IP connection to the dedicated server port
   12357.

.. [9]
   Operating different single instances of Cryptomove servers from
   multiple virtual machines on the same physical host is still OK.

.. [10]
   Make sure to install Cryptomove on the second host as explained in
   the `Preface <#Cryptomove Installation>`__ , set HELLO_PACKSRC_PATH,
   and create a system key on that host before running this example.

.. [11]
   The last step is optional

.. [12]
   Because track files keep changing their contents, names and
   existence, they afford the same level of protection as the data parts
   in the Cryptomove store.

.. [13]
   Make sure the entry in /etc/sudoers is correct, or otherwise command
   sudo may stop functioning. You may use special editor visudo in order
   to edit /etc/sudoer – that editor checks correctness of the changes
   made.

.. [14]
   Execution of this command is not allowed in this version of
   CryptoMove software.

.. [15]
   You can always shut down all Hello engines and their running
   packages, while cleaning all system resources, by issuing command
   ‘sudo hee –Q’ – see Hello Programming Guide for troubleshooting
   procedures.

.. [16]
   Cryptomove uses Linux editline package for both history and editing.

.. [17]
   For parameter NUMBER the value ALL indicates saving or restoring all
   copies. For parameter VERSION the value LAST indicates working with
   the last saved version.

.. [18]
   You cannot change the server edge mode – it is set at startup to true
   for command se or false for command ss.

.. [19]
   Because each computer host can run only one Cryptomove server, the
   computer host states and server states are equivalent.

.. [20]
   By definition, direct neighborhood consists of all hosts to which
   current Hello host has connected. Reverse neighborhood consists of
   all hosts that connected to this host. See `Hello User
   Guide <http://www.amsdec.com/wp-content/uploads/2015/08/helloguide.pdf>`__
   for more details about Hello runtime neighborhoods.

.. [21]
   See Hello Programming guide for details about Hello network
   neighborhoods and group traversals.

.. [22]
   By definition, direct neighborhood consists of all hosts to which
   current Hello host has connected. Reverse neighborhood consists of
   all hosts that connected to this host. See `Hello User
   Guide <http://www.amsdec.com/wp-content/uploads/2015/08/helloguide.pdf>`__
   for more details about Hello runtime neighborhoods.

.. [23]
   See Hello Programming guide for details about Hello network
   neighborhoods and group traversals.

.. [24]
   The order is important for the following reasons:

   The original data is split into consecutive parts, so they must be
   restored in the same sequence.

   If bit-scatter has been applied at the save time, to restore the
   scattered bits properly, the parts must be in the correct order.

   Finally, If data is pair-wise XOR-ed, one need to arrange the pairs
   in a proper order to un-XOR the data.

.. [25]
   The number of parts 12 and the number of copies 4 are used just for
   the sake of an illustration; users may vary these and other options
   when they save data in the data store.

.. [26]
   `http://www.universetoday.com/36302/atoms-in-the-universe/
   2015 <http://www.universetoday.com/36302/atoms-in-the-universe/>`__

.. [27]
   `http://en.wikipedia.org/wiki/Hypergeometric_distribution
   2015 <http://en.wikipedia.org/wiki/Hypergeometric_distribution%202015>`__

.. [28]
   `http://en.wikipedia.org/wiki/Binomial_distribution
   2015 <http://en.wikipedia.org/wiki/Binomial_distribution>`__

.. |image0| image:: media/image1.png
.. |C:\Users\boris997\AppData\Local\Microsoft\Windows\INetCache\Content.Word\CryptoMove background white.png| image:: media/image2.png
   :width: 0.875in
   :height: 0.875in
