Cryptomove Documentation Portal!
=================================

.. _helloguide:

.. toctree::
   :maxdepth: 2
   :caption: Hello: Programming Guide for Orchestrating Location and Remote Computation
   
   helloguide

.. _apiguide:

.. toctree::
   :maxdepth: 2
   :caption: Developer API Documentation

   This developer guide describes about various APIs that cryptomove provides to use.
     
   api/upload_key_API
   api/restore_key_API
   api/list_keys_API
   api/delete_key_API
   api/uploade_file_API
   api/list_no_dup_API
   api/version_list_API
   api/version_expose_API
   api/version_delete_API

.. api/payment_API

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`