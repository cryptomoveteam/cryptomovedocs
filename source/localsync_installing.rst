Installing Localsync
=====================

#. Download the installation file: **cryptomove_installer**
#. Put the file into a folder. For example, */a/b/c/*
#. Go to the folder having the installation file, and run the installation file as follows:
    
    ``cd /a/b/c/``
    
    ``chmod +x cryptomove_installer``
    
    ``./cryptomove_installer``

    .. note:: During the installation, the following folders will be created under HOME directory:
    
        - cryptomove_secret
        - cryptomove_program
    
        Cryptomove will be installed in the **cryptomove_program** folder, and **cryptomove_secret** is the default folder that localsync will protect.
    
#. Follow these instructions:

    #. Answer ‘y’:
        
        **Do you agree with the Cryptomove Software license [yn]?**

    #. Answer ‘y’:
        
        **Warning:If you have a previous Cryptomove installation, it will be overwritten, do you still want to continue [yn]?**

    #. Input your passphrase and press enter to configure cryptomove server:
    
        **To configure cryptomove server, enter a passphrase you want to use:**

    #. Answer ‘y’:
    
        **Do you want to start localsync now [yn]?**

    #. Input your passphrase and hit enter to start localsync:
    
        **To start localsync, enter your passphrase:**

    #. Finally, you will see the following message:
    
        **Localsync is ready...**
        
        The message indicates that localsync is ready and is protecting your folder.