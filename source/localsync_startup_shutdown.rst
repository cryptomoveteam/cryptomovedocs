Stopping Localsync
===================

To stop cryptomove server, run the following command:
    
    ``sudo hee -Q``

Starting Localsync
===================

To start localsync:

#. Ensure that cryptomove is not running.
#. Go to **cryptomove_program** folder.
#. Run the following commands: 

    ``"./startlocalsync.sh":``
    
    ``cd cryptomove_program``
    
    ``./startlocalsync.sh``

Running Localsync
===================

To run localsync:
	
``Localsync <directory or folder you’re trying to secure>``